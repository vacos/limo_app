webpackJsonp([0],{

/***/ 10:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__rest_api_rest_api__ = __webpack_require__(38);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AccountProvider = /** @class */ (function () {
    function AccountProvider(http) {
        this.http = http;
    }
    AccountProvider.prototype.getUserId = function () {
        return localStorage.userId;
    };
    AccountProvider.prototype.UserLogOut = function () {
        var obj = {
            "id": this.getUserId()
        };
        return this.http.PostRequest("/user/logout", obj);
    };
    AccountProvider.prototype.UserGet = function () {
        var obj = {
            "id": this.getUserId()
        };
        return this.http.PostRequest("/user/get", obj);
    };
    AccountProvider.prototype.UserCheck = function (phone) {
        var obj = {
            "phone": phone,
            "_check_only": "1"
        };
        return this.http.PostRequest("/user/check", obj);
    };
    AccountProvider.prototype.UserCheckUpdatePhone = function (phone) {
        var obj = {
            "phone": phone,
            "check_only": "1"
        };
        return this.http.PostRequest("/user/check", obj);
    };
    AccountProvider.prototype.UserVerify = function (phone, code) {
        var obj = {
            "phone": phone,
            "code": code
        };
        return this.http.PostRequest("/user/verify", obj);
    };
    AccountProvider.prototype.UserResetPassword = function (phone) {
        var obj = {
            "phone": phone
        };
        return this.http.PostRequest("/user/reset_password", obj);
    };
    AccountProvider.prototype.UserLogin = function (phone, password) {
        var lat = localStorage.currentLat;
        var lng = localStorage.currentLng;
        if (lat == null) {
            lat = "13.7014544";
        }
        if (lng == null) {
            lng = "100.3879633";
        }
        var obj = {
            "phone": phone,
            "password": password,
            "lat": lat,
            "long": lng
        };
        return this.http.PostRequest("/user/login", obj);
    };
    AccountProvider.prototype.UserLoginApp = function () {
        var lat = localStorage.currentLat;
        var lng = localStorage.currentLng;
        if (lat == null) {
            lat = "13.750443";
        }
        if (lng == null) {
            lng = "100.539913";
        }
        var obj = {
            "id": localStorage.userId,
            "lat": lat,
            "long": lng
        };
        return this.http.PostRequest("/user/login_app", obj);
    };
    AccountProvider.prototype.UserUpdate = function (data) {
        var obj = {
            "id": data.id,
            "_phone": data.phone,
            "_facebook_id": data._facebook_id,
            "name": data.name,
            "email": data.email,
            "image": data.image,
            "_uuid": data._uuid,
            "_udid": data._udid,
            "_pushbadge": data._pushbadge,
            "_pushsound": data._pushsound,
            "_pushalert": data._pushalert,
            "device": data.device,
            "password": data.password,
        };
        return this.http.PostRequest("/user/update", obj);
    };
    AccountProvider.prototype.UserUpdatePhone = function (data) {
        var obj = {
            "id": data.id,
            "phone": data.phone
        };
        return this.http.PostRequest("/user/update", obj);
    };
    AccountProvider.prototype.UserPaymentAdd = function (str_token) {
        var obj = {
            "id": this.getUserId(),
            "token": str_token,
        };
        return this.http.PostRequest("/user/payment_add", obj);
    };
    AccountProvider.prototype.UserPaymentAddCash = function () {
        var obj = {
            "id": this.getUserId(),
        };
        return this.http.PostRequest("/user/payment_add_cash", obj);
    };
    AccountProvider.prototype.UserPaymentDelete = function (pid) {
        var obj = {
            "id": this.getUserId(),
            "id_payment": pid
        };
        return this.http.PostRequest("/user/payment_delete", obj);
    };
    AccountProvider.prototype.UserPaymentListing = function () {
        var obj = {
            "id": this.getUserId(),
        };
        return this.http.PostRequest("/user/payment_listing", obj);
    };
    AccountProvider.prototype.UserAddPlace = function (data) {
        var obj = {
            "id": this.getUserId(),
            "lat": data.lat,
            "long": data.long,
            "type": data.type,
            "name": data.name
        };
        return this.http.PostRequest("/user/add_place", obj);
    };
    AccountProvider.prototype.UserListingPlace = function () {
        var obj = {
            "id": this.getUserId()
        };
        return this.http.PostRequest("/user/listing_place", obj);
    };
    AccountProvider.prototype.UserDelPlace = function (pid) {
        var obj = {
            "id": this.getUserId(),
            "place_id": pid
        };
        return this.http.PostRequest("/user/del_place", obj);
    };
    AccountProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__rest_api_rest_api__["a" /* RestApiProvider */]])
    ], AccountProvider);
    return AccountProvider;
}());

//# sourceMappingURL=account.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DriverProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__rest_api_rest_api__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DriverProvider = /** @class */ (function () {
    function DriverProvider(http) {
        this.http = http;
    }
    DriverProvider.prototype.DriverFind = function () {
        return this.http.PostRequest("/driver/find", {});
    };
    DriverProvider.prototype.DriverDriving = function () {
        return this.http.PostRequest("/driver/driving", {});
    };
    DriverProvider.prototype.DriverConfirm = function (dId, oId) {
        var obj = {
            "driver_id": dId,
            "order_id": oId
        };
        return this.http.PostRequest("/driver/confirm", obj);
    };
    DriverProvider.prototype.DriverSuccess = function (dId, oId) {
        var obj = {
            "driver_id": dId,
            "order_id": oId
        };
        return this.http.PostRequest("/driver/success", obj);
    };
    DriverProvider.prototype.DriverVote = function (oId, rate, comment) {
        var obj = {
            "order_id": oId,
            "rate": rate,
            "comment": comment
        };
        return this.http.PostRequest("/driver/vote", obj);
    };
    DriverProvider.prototype.DriverPickup = function (dId, oId) {
        var obj = {
            "driver_id": dId,
            "order_id": oId
        };
        return this.http.PostRequest("/driver/pickup", obj);
    };
    // https://maps.googleapis.com/maps/api/geocode/json?latlng=13.70149792102319,100.38790051145523&result_type=route&key=AIzaSyDgxVA7U6pyie189pCEKCd-C1QxpRE9ibI
    DriverProvider.prototype.DriverLoc = function (lat, long) {
        var urls = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + long + "&result_type=route&key=AIzaSyDgxVA7U6pyie189pCEKCd-C1QxpRE9ibI";
        return this.http.GetRequestUrl(urls);
    };
    DriverProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__rest_api_rest_api__["a" /* RestApiProvider */]])
    ], DriverProvider);
    return DriverProvider;
}());

//# sourceMappingURL=driver.js.map

/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__payment_add_payment_add__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_account_account__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__modal_popup_modal_popup__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__add_card_add_card__ = __webpack_require__(224);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PaymentPage = /** @class */ (function () {
    function PaymentPage(navCtrl, navParams, modalCtrl, pAccount, alertCtrl, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.pAccount = pAccount;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.isAdd = false;
        this.isCash = false;
        this.paymentList = [];
        this.initPaymentLists();
    }
    PaymentPage.prototype.initPaymentLists = function () {
        var _this = this;
        this.isAdd = false;
        this.paymentList = [];
        this.pAccount.UserPaymentListing().then(function (r) {
            if (r.status == 200 && r.message == "Success") {
                _this.paymentList = r.data;
            }
        });
    };
    PaymentPage.prototype.clickAdd = function () {
        this.isAdd = !this.isAdd;
        if (this.isAdd) {
            this.isCash = true;
            for (var i = 0; i < this.paymentList.length; i++) {
                if (this.paymentList[i].card_number == 'Cash') {
                    this.isCash = false;
                    break;
                }
            }
        }
        // alert(this.isCash);
    };
    PaymentPage.prototype.OpenAddCash = function () {
        var _this = this;
        this.pAccount.UserPaymentAddCash().then(function (r) {
            if (r.data) {
                _this.initPaymentLists();
            }
        });
    };
    PaymentPage.prototype.OpenAddCredit = function () {
        var _this = this;
        var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__payment_add_payment_add__["a" /* PaymentAddPage */], { userId: 8675309 });
        profileModal.onDidDismiss(function (data) {
            _this.isAdd = false;
            _this.initPaymentLists();
        });
        profileModal.present();
    };
    PaymentPage.prototype.OpenAddCreditII = function () {
        var _this = this;
        var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__add_card_add_card__["a" /* AddCardPage */], { userId: 8675309 });
        profileModal.onDidDismiss(function (data) {
            _this.isAdd = false;
            _this.initPaymentLists();
        });
        profileModal.present();
    };
    PaymentPage.prototype.removeItem_B = function (p) {
        var _this = this;
        var set_Popup = {
            title: "Confirm delete",
            text_message: "Do you want to delete this card? ",
            btn_cancel: "Cancel",
            btn_confirm: "Delete",
            isCancel: true
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            // console.log(popup);  
            if (popup) {
                _this.pAccount.UserPaymentDelete(p.id).then(function (r) {
                    _this.initPaymentLists();
                });
            }
        });
        popModal.present();
    };
    PaymentPage.prototype.removeItem = function (p) {
        var _this = this;
        this.pAccount.UserPaymentDelete(p.id).then(function (r) {
            _this.initPaymentLists();
        });
    };
    PaymentPage.prototype.dismiss = function () {
        if (this.isAdd) {
            this.isAdd = !this.isAdd;
        }
        else {
            this.viewCtrl.dismiss();
        }
    };
    PaymentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-payment',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/payment/payment.html"*/'<ion-header>\n  <ion-navbar>\n      <ion-buttons left>\n          <button ion-button (click)="dismiss()" color="light">\n              <ion-icon name="ios-arrow-round-back-outline"></ion-icon>\n          </button>\n      </ion-buttons>\n      <ion-title full text-center>Payment Method</ion-title>\n      <ion-buttons end>\n        <button ion-button>\n            <ion-icon name="ios-arrow-round-back-outline" color="primary"></ion-icon>\n        </button>\n    </ion-buttons>\n  </ion-navbar> \n</ion-header>\n  \n<ion-content>\n\n  <ion-list *ngIf="!isAdd"> \n    <ion-item-sliding *ngFor="let p of paymentList">\n      <ion-item>\n        <img *ngIf="p.card_number!=\'Cash\'" src="assets/images/credit-card.png" /> \n        <img *ngIf="p.card_number ==\'Cash\'" src="assets/images/cash.png" />\n        {{p.card_number}}\n      </ion-item> \n      <ion-item-options icon-start>\n        <button ion-button (click)="removeItem(p)">\n            <ion-icon name="ios-trash-outline"></ion-icon>\n            Delete\n        </button>\n      </ion-item-options>\n    </ion-item-sliding> \n\n  <ion-item color="light" (click)="clickAdd()">\n    <img src="assets/images/credit-card.png"/>\n    Credit/Debit\n    <ion-icon name="md-add" item-end style="margin-right: 25px"></ion-icon>  \n  </ion-item>\n \n    <ion-item no-lines> \n    </ion-item>\n  </ion-list>  \n\n  <ion-list *ngIf="isAdd">\n    <ion-item  color="light" (click)="OpenAddCredit()">\n      <img src="assets/images/credit-card.png"/>\n      Credit/Debit \n     </ion-item>\n     <ion-item *ngIf="isCash" color="light" (click)="OpenAddCash()">\n      <img src="assets/images/cash.png"/>\n      Cash   \n     </ion-item> \n    <ion-item no-lines> \n    </ion-item>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/payment/payment.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"], __WEBPACK_IMPORTED_MODULE_3__providers_account_account__["a" /* AccountProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], PaymentPage);
    return PaymentPage;
}());

//# sourceMappingURL=payment.js.map

/***/ }),

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ValidatePasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ValidatePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ValidatePasswordPage = /** @class */ (function () {
    function ValidatePasswordPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.inputPassword = "";
    }
    ValidatePasswordPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad ValidatePasswordPage');
    };
    ValidatePasswordPage.prototype.OnClick_Login = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    ValidatePasswordPage.prototype.OnClick_ForgotPassword = function () {
        alert("Unavailable now.");
    };
    ValidatePasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-validate-password',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/validate-password/validate-password.html"*/'<!--\n  Generated template for the ValidatePasswordPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Verify Password</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n    <ion-item>\n        <ion-label color="primary" stacked>Password</ion-label> \n        <ion-input type="text" [(ngModel)]="inputPassword" name="password"></ion-input>\n    </ion-item>\n\n\n    <button ion-button (click)="OnClick_Login()">Login</button>\n    <button ion-button (click)="OnClick_ForgotPassword()">forgot password</button>\n\n</ion-content>\n'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/validate-password/validate-password.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ValidatePasswordPage);
    return ValidatePasswordPage;
}());

//# sourceMappingURL=validate-password.js.map

/***/ }),

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateNewuserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__term_condition_term_condition__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_account_account__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CreateNewuserPage = /** @class */ (function () {
    function CreateNewuserPage(navCtrl, platform, navParams, pAccount) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.navParams = navParams;
        this.pAccount = pAccount;
        this.stateValidate = false;
        this.message_validate = "";
        this.questionList = [
            {
                question: "What is your email address?",
                value: [{
                        placeholder: "Name@example",
                        type: "email",
                        value: ""
                    }]
            },
            {
                question: "Create your password?",
                value: [{
                        placeholder: "Minimum 5 charactor",
                        type: "password",
                        value: ""
                    },
                    {
                        placeholder: "Confirm password",
                        type: "password",
                        value: ""
                    }]
            },
            {
                question: "Enter your name?",
                value: [{
                        placeholder: "Firstname",
                        type: "text",
                        value: ""
                    },
                    {
                        placeholder: "Lastname",
                        type: "text",
                        value: ""
                    }]
            }
        ];
        this.currentIndex = 0;
        this.phoneNumber = "";
        this.userid = "";
        this.bBack = true;
        this.text_color = "sp_color_b";
        this.ip_color = "ip_color_b";
        this.phoneNumber = this.navParams.get("phoneNumber");
        this.userid = this.navParams.get("userid");
    }
    CreateNewuserPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.navBar.backButtonClick = function (e) {
            // todo something 
            if (_this.currentIndex > 0) {
                _this.currentIndex--;
            }
            if (_this.currentIndex == 0) {
                _this.bBack = true;
            }
        };
        this.platform.registerBackButtonAction(function () {
            if (_this.currentIndex > 0) {
                _this.currentIndex--;
            }
            if (_this.currentIndex == 0) {
                _this.bBack = true;
            }
        });
    };
    CreateNewuserPage.prototype.dismiss = function () {
        if (this.currentIndex > 0) {
            this.currentIndex--;
        }
    };
    CreateNewuserPage.prototype.nextState = function () {
        var _this = this;
        if (this.currentIndex < this.questionList.length - 1) {
            if (this.currentIndex == 0) {
                //console.log("0000000000");
                if (!this.emailValidator(this.questionList[0].value[0].value)) {
                    this.text_color = "sp_color_a";
                    this.ip_color = "ip_color_a";
                    this.stateValidate = true;
                    this.message_validate = "Check your email address";
                }
                else {
                    this.text_color = "sp_color_b";
                    this.ip_color = "ip_color_b";
                    this.stateValidate = false;
                    this.message_validate = "";
                    this.bBack = false;
                    this.currentIndex++;
                }
            }
            else if (this.currentIndex == 1) {
                //console.log("11111111111");
                if (!this.passwordValidator(this.questionList[1].value[0].value)) {
                    this.text_color = "sp_color_a";
                    this.ip_color = "ip_color_a";
                    this.stateValidate = true;
                    this.message_validate = "Enter your password, Minimum 5 charactor, Password not match";
                }
                else {
                    if (this.questionList[1].value[0].value != this.questionList[1].value[1].value) {
                        this.text_color = "sp_color_a";
                        this.ip_color = "ip_color_a";
                        this.stateValidate = true;
                        this.message_validate = "Enter your password";
                    }
                    else {
                        this.text_color = "sp_color_b";
                        this.ip_color = "ip_color_b";
                        this.stateValidate = false;
                        this.message_validate = "";
                        this.currentIndex++;
                    }
                }
            }
        }
        else {
            //  console.log("222222222"); 
            if ((this.questionList[2].value[0].value + "" != "") && (this.questionList[2].value[1].value + "" != "")) {
                this.text_color = "sp_color_b";
                this.ip_color = "ip_color_b";
                this.stateValidate = false;
                this.message_validate = "";
                localStorage.userName = this.questionList[2].value[0].value + " " + this.questionList[2].value[1].value;
                this.pAccount.UserUpdate({
                    id: this.userid,
                    _phone: this.phoneNumber,
                    _facebook_id: "???",
                    name: this.questionList[2].value[0].value + " " + this.questionList[2].value[1].value,
                    email: this.questionList[0].value[0].value,
                    _image: "",
                    _uuid: "?",
                    _udid: "?",
                    _pushbadge: "?",
                    _pushsound: "?",
                    _pushalert: "?",
                    device: "?",
                    password: this.questionList[1].value[0].value,
                }).then(function (r) {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__term_condition_term_condition__["a" /* TermConditionPage */], { phoneNumber: r.data.phone, userid: r.data.id });
                });
            }
            else {
                this.text_color = "sp_color_a";
                this.stateValidate = true;
                this.message_validate = "Enter your name";
            }
        }
    };
    CreateNewuserPage.prototype.nextStateff = function () {
        var _this = this;
        if (this.currentIndex < this.questionList.length - 1) {
            if (this.currentIndex == 1 && (this.questionList[1].value[0].value != this.questionList[1].value[1].value)) {
                // alert("Password miss match.");
            }
            else {
                this.currentIndex++;
            }
        }
        else {
            localStorage.userName = this.questionList[2].value[0].value + " " + this.questionList[2].value[1].value;
            this.pAccount.UserUpdate({
                id: this.userid,
                _phone: this.phoneNumber,
                _facebook_id: "???",
                name: this.questionList[2].value[0].value + " " + this.questionList[2].value[1].value,
                email: this.questionList[0].value[0].value,
                _image: "",
                _uuid: "?",
                _udid: "?",
                _pushbadge: "?",
                _pushsound: "?",
                _pushalert: "?",
                device: "?",
                password: this.questionList[1].value[0].value,
            }).then(function (r) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__term_condition_term_condition__["a" /* TermConditionPage */], { phoneNumber: r.data.phone, userid: r.data.id });
            });
        }
    };
    CreateNewuserPage.prototype.OnClick_CreateNewUser = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    CreateNewuserPage.prototype.emailValidator = function (str) {
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return emailPattern.test(str);
    };
    CreateNewuserPage.prototype.passwordValidator = function (str) {
        var regExp = /^[a-zA-Z0-9]{5,20}/;
        return regExp.test(str);
    };
    CreateNewuserPage.prototype.fnameValidator = function (str) {
        var regExp = /^[a-zA-Z ]{1,30}$/;
        return regExp.test(str);
    };
    CreateNewuserPage.prototype.lnameValidator = function (str) {
        var regExp = /^[a-zA-Z ]{1,30}$/;
        return regExp.test(str);
    };
    CreateNewuserPage.prototype.phoneValidator = function (str) {
        var regExp = /^([0]{1})([0-9]{2})([0-9]{3})([0-9]{4})$/;
        //let regExp = /^[0-9]{10}$/;
        return regExp.test(str);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"])
    ], CreateNewuserPage.prototype, "navBar", void 0);
    CreateNewuserPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-create-newuser',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/create-newuser/create-newuser.html"*/'<ion-header> \n    <ion-navbar hideBackButton="{{bBack}}"> \n         <ion-title full text-center></ion-title> \n    </ion-navbar>  \n</ion-header>\n\n\n<ion-content>\n    <div class="panel-top"> \n        <h4 class="{{text_color}}">{{questionList[currentIndex].question}}</h4>\n\n        <div class="panel-input-user">\n            <ion-input class="{{ip_color}}" *ngFor="let a of questionList[currentIndex].value" \n            type="{{a.type}}" (keyup.enter)="nextState()"\n            placeholder="{{a.placeholder}}" [(ngModel)]="a.value"\n            ></ion-input>\n            <h6 *ngIf="stateValidate">{{message_validate}}</h6>\n        </div>\n \n    </div>\n     \n    <div class="panel-action">\n            <ion-icon name="ios-arrow-round-forward-outline" color="light" \n            (click)="nextState()"></ion-icon>\n        </div> \n</ion-content>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/create-newuser/create-newuser.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_4__providers_account_account__["a" /* AccountProvider */]])
    ], CreateNewuserPage);
    return CreateNewuserPage;
}());

//# sourceMappingURL=create-newuser.js.map

/***/ }),

/***/ 116:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrivacyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_page_api_page_api__ = __webpack_require__(39);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the YourTripPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PrivacyPage = /** @class */ (function () {
    function PrivacyPage(navCtrl, navParams, pget, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pget = pget;
        this.viewCtrl = viewCtrl;
    }
    PrivacyPage.prototype.ionViewDidLoad = function () {
        // legal, term, refund
        var _this = this;
        var ttype = "privacy";
        if (localStorage.memberType == "2") {
            ttype = "privacy_driver";
        }
        this.pget.PageApiGet(ttype).then(function (r) {
            if (r.status == 200 && r.message == "Success") {
                _this.privacy_html = r.data.content;
            }
        });
    };
    PrivacyPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    PrivacyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-privacy',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/privacy/privacy.html"*/'<!--\n  Generated template for the YourTripPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header> \n        <!-- <ion-navbar>\n                <ion-title>PRIVACY & POLICY\n                </ion-title> \n        </ion-navbar> -->\n</ion-header>\n\n\n<ion-content>\n\n        <div class="panel-top">\n                <h2>PRIVACY & POLICY</h2>\n             </div>\n             <div class="horizontalline"></div> \n\n    <!-- <ion-fab right top>\n        <ion-icon name="close" color="primary" style="font-size: 4em;margin-right: 20px;"\n        (click)="dismiss()"></ion-icon>\n    </ion-fab> -->\n \n\n    <ion-scroll style="height:80%; " scrollY="true" >\n            <div class="panel-detail-b" [innerHTML]="privacy_html"> \n                </div> \n            </ion-scroll>\n\n  \n\n   <!-- <ion-fab right bottom>\n        <ion-icon name="ios-arrow-round-back-outline" color="cGray" style="font-size: 4em;margin-right: 20px;"\n        (click)="dismiss()"></ion-icon>\n    </ion-fab>  -->\n   \n</ion-content>  \n\n<ion-footer no-border>\n        <ion-toolbar color="light" >\n            <ion-buttons end>\n                <button ion-button (click)="dismiss()">\n                    <ion-icon name="ios-arrow-round-back-outline" color="cGray" style="font-size: 4em;margin-right: 20px;"></ion-icon>\n                </button>\n            </ion-buttons> \n        </ion-toolbar>\n      </ion-footer>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/privacy/privacy.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_page_api_page_api__["a" /* PageApiProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], PrivacyPage);
    return PrivacyPage;
}());

//# sourceMappingURL=privacy.js.map

/***/ }),

/***/ 117:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyLocationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_driver_home_driver__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__modal_popup_modal_popup__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_account_account__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyLocationPage = /** @class */ (function () {
    function MyLocationPage(navCtrl, pAccount, modalCtrl) {
        this.navCtrl = navCtrl;
        this.pAccount = pAccount;
        this.modalCtrl = modalCtrl;
        this.isGPS = false;
    }
    MyLocationPage.prototype.ionViewDidLoad = function () {
        this.map = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["a" /* GoogleMaps */].create('map_canvas1', {});
        this.GetCurrentLocation();
    };
    MyLocationPage.prototype.GetCurrentLocation = function () {
        //this.map.clear();
        var _this = this;
        // Get the location of you
        this.map.getMyLocation()
            .then(function (location) {
            // console.log(JSON.stringify(location, null ,2));
            localStorage.currentLat = location.latLng.lat;
            localStorage.currentLng = location.latLng.lng;
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({ 'location': location.latLng }, function (results, status) {
                // console.log(results); 
                localStorage.currentPlaceA = results[0].address_components[0].short_name + ' ' + results[0].address_components[1].short_name;
                localStorage.currentPlaceB = results[0].formatted_address;
                _this.pAccount.UserLoginApp().then(function (r) {
                    // console.log(r);  
                });
                //this.isGPS =true; 
                _this.GotoHomePage();
            });
        }).catch(function (error) {
            //alert('Error getting location');
            _this.isGPS = true;
            console.log(error);
        });
    };
    MyLocationPage.prototype.GotoHomePage = function () {
        if (localStorage.memberType == "0") {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
        }
        else if (localStorage.memberType == "1") {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
        }
        else if (localStorage.memberType == "2") {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_driver_home_driver__["a" /* HomeDriverPage */]);
        }
    };
    MyLocationPage.prototype.openPopAlertMessage = function (msg) {
        var _this = this;
        var set_Popup = {
            title: "Pop-up Request",
            text_message: msg,
            btn_cancel: "",
            btn_confirm: "Done",
            isCancel: false
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            _this.GetCurrentLocation();
        });
        popModal.present();
    };
    MyLocationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-my-location',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/my-location/my-location.html"*/' \n<ion-content class="ion-content">\n\n\n  <div class="logo-panel">\n    <img src="assets/images/Logo.png"/>\n    <div>\n      Welcome to Premium<br/>Travel Application\n    </div>\n    \n  </div>\n \n\n  <div class="btn-panel"> \n\n      <br>\n\n      <div *ngIf="!isGPS">\n          Loading ...\n      </div>\n     \n      <br>\n\n      <div *ngIf="isGPS"> \n        Please help to checked Open GPS<br/>for use Limo Application\n      </div>\n       \n  </div>\n\n  \n  <div id="map_canvas1" style="width: 100px; height: 100px;display: none;"></div> \n  \n</ion-content>\n'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/my-location/my-location.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_5__providers_account_account__["a" /* AccountProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"]])
    ], MyLocationPage);
    return MyLocationPage;
}());

//# sourceMappingURL=my-location.js.map

/***/ }),

/***/ 119:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PhoneUpdatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_account_account__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__modal_popup_modal_popup__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__otp_update_phone_otp_update_phone__ = __webpack_require__(237);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PhoneUpdatePage = /** @class */ (function () {
    function PhoneUpdatePage(navCtrl, viewCtrl, modalCtrl, navParams, pAccount) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.pAccount = pAccount;
        this.ipPhoneNumber = "";
        this.ipCountryCode = "TH";
        this.cCountrys = [];
        this.phoneValidate = false;
        this.message_phone_validate = "Check Your Phone Number";
        this.selectedCountry = {};
        this.countries = [{
                name: "United States",
                dial_code: "+1",
                code: "US"
            }, {
                name: "Canada",
                dial_code: "+1",
                code: "CA"
            }, {
                name: "China",
                dial_code: "+86",
                code: "CN"
            }, {
                name: "France",
                dial_code: "+33",
                code: "FR"
            }, {
                name: "Germany",
                dial_code: "+49",
                code: "DE"
            }, {
                name: "Greece",
                dial_code: "+30",
                code: "GR"
            }, {
                name: "Indonesia",
                dial_code: "+62",
                code: "ID"
            }, {
                name: "Iraq",
                dial_code: "+964",
                code: "IQ"
            }, {
                name: "Italy",
                dial_code: "+39",
                code: "IT"
            }, {
                name: "Japan",
                dial_code: "+81",
                code: "JP"
            }, {
                name: "Malaysia",
                dial_code: "+60",
                code: "MY"
            }, {
                name: "Myanmar",
                dial_code: "+95",
                code: "MM"
            }, {
                name: "Netherlands",
                dial_code: "+31",
                code: "NL"
            }, {
                name: "Panama",
                dial_code: "+507",
                code: "PA"
            }, {
                name: "Peru",
                dial_code: "+51",
                code: "PE"
            }, {
                name: "Philippines",
                dial_code: "+63",
                code: "PH"
            }, {
                name: "Qatar",
                dial_code: "+974",
                code: "QA"
            }, {
                name: "Saudi Arabia",
                dial_code: "+966",
                code: "SA"
            }, {
                name: "Singapore",
                dial_code: "+65",
                code: "SG"
            }, {
                name: "South Africa",
                dial_code: "+27",
                code: "ZA"
            }, {
                name: "Spain",
                dial_code: "+34",
                code: "ES"
            }, {
                name: "Sri Lanka",
                dial_code: "+94",
                code: "LK"
            }, {
                name: "Switzerland",
                dial_code: "+41",
                code: "CH"
            }, {
                name: "Thailand",
                dial_code: "+66",
                code: "TH"
            }, {
                name: "United Kingdom",
                dial_code: "+44",
                code: "GB"
            }, {
                name: "Brunei Darussalam",
                dial_code: "+673",
                code: "BN"
            }, {
                name: "Hong Kong",
                dial_code: "+852",
                code: "HK"
            }, {
                name: "Korea, Republic of",
                dial_code: "+82",
                code: "KR"
            }, {
                name: "Russia",
                dial_code: "+7",
                code: "RU"
            }, {
                name: "Taiwan, Province of China",
                dial_code: "+886",
                code: "TW"
            }, {
                name: "Viet Nam",
                dial_code: "+84",
                code: "VN"
            }];
    }
    PhoneUpdatePage.prototype.sChanged = function (event) {
        // User was selected
    };
    PhoneUpdatePage.prototype.onClose = function () {
        // let toast = this.toastCtrl.create({
        //   message: 'Thanks for your selection',
        //   duration: 2000
        // });
        // toast.present();
    };
    PhoneUpdatePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    PhoneUpdatePage.prototype.openFromCode = function () {
        this.selectComponent.open();
    };
    PhoneUpdatePage.prototype.ionViewDidLoad = function () {
    };
    PhoneUpdatePage.prototype.openPopAlertMessage = function (msg) {
        var set_Popup = {
            title: "Pop-up Request",
            text_message: msg,
            btn_cancel: "",
            btn_confirm: "Done",
            isCancel: false
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            // console.log(popup);    
        });
        popModal.present();
    };
    PhoneUpdatePage.prototype.nextState = function () {
        var _this = this;
        if (this.ipPhoneNumber != "") {
            var isNumb = this.phoneValidator(this.ipPhoneNumber);
            if (isNumb) {
                this.pAccount.UserCheckUpdatePhone(this.ipPhoneNumber).then(function (r) {
                    if (r.status == 500) {
                        localStorage.phoneNumber = r.data.phone;
                        //console.log("CODE : ",r.data.code);
                        //alert("OTP CODE : " + r.data.code);
                        //localStorage.phoneNumber = this.ipPhoneNumber;
                        //this.navCtrl.push(ValidateOtpPage, r.data);
                        _this.openVerifyOtp(r.data);
                    }
                    else {
                        _this.openPopAlertMessage(r.message);
                    }
                });
            }
            else {
                this.phoneValidate = true;
                //this.openPopAlertMessage("Check Your Phone Number");
            }
        }
        else {
            this.phoneValidate = true;
            //this.openPopAlertMessage("Check Your Phone Number");
        }
    };
    PhoneUpdatePage.prototype.phoneValidator = function (str) {
        var regExp = /^([0]{1})([0-9]{2})([0-9]{3})([0-9]{4})$/;
        //let regExp = /^[0-9]{10}$/;
        return regExp.test(str);
    };
    PhoneUpdatePage.prototype.openVerifyOtp = function (dat) {
        var _this = this;
        var pModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__otp_update_phone_otp_update_phone__["a" /* OtpUpdatePhonePage */], dat);
        pModal.onDidDismiss(function (data) {
            if (data != null) {
                var datas = {
                    phone: data.phone
                };
                _this.viewCtrl.dismiss(datas);
            }
        });
        pModal.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('myselect'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableComponent"])
    ], PhoneUpdatePage.prototype, "selectComponent", void 0);
    PhoneUpdatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-phone-update',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/phone-update/phone-update.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button (click)="dismiss()" color="light">\n        <ion-icon name="ios-arrow-round-back-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n    <!-- <ion-title full text-center>EDIT ACCOUNT</ion-title> -->\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n \n  <div class="panel-top">\n      <h4>Enter your phone number</h4>  \n  \n      <div class="panel-content">  \n          <ion-grid>\n              <ion-row>\n                  <ion-col col-2>\n                      <img item-left src="https://limoapp.me/assets/di/flags/th.png" style="height: 32px;" />\n                  </ion-col>\n                <ion-col col-3>     \n                  <ion-select [(ngModel)]="ipCountryCode"> \n                        <ion-option value="TH">+66\n                        </ion-option>  \n                  </ion-select> \n \n                </ion-col>\n                <ion-col col-7>\n                  <input type="tel" [(ngModel)]="ipPhoneNumber"  maxlength="10" (keyup.enter)="nextState()"/>\n                </ion-col>\n              </ion-row> \n              <ion-row>\n                  <ion-col col-2>\n                  </ion-col>\n                <ion-col col-10>\n                    <p style="color: red;font-size: 0.8em;" *ngIf="phoneValidate">* {{message_phone_validate}}</p>\n                  </ion-col>\n              </ion-row>\n            </ion-grid>\n      </div> \n  </div>\n\n\n  <div class="panel-action">\n      <ion-icon name="ios-arrow-round-forward-outline" color="light" \n      (click)="nextState()"></ion-icon>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/phone-update/phone-update.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_account_account__["a" /* AccountProvider */]])
    ], PhoneUpdatePage);
    return PhoneUpdatePage;
}());

//# sourceMappingURL=phone-update.js.map

/***/ }),

/***/ 131:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 131;

/***/ }),

/***/ 14:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PopupPage = /** @class */ (function () {
    function PopupPage(viewCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.setPopup = {
            title: "Pop-up Request",
            text_message: "",
            btn_cancel: "Cancel",
            btn_confirm: "Confirm",
            isCancel: true
        };
        //console.log( navParams.get('setPopup'));
        this.setPopup = navParams.get('setPopup');
    }
    PopupPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    PopupPage.prototype.chooseItem = function (val) {
        // console.log('choose btn > ', val);
        this.viewCtrl.dismiss(val);
    };
    PopupPage.prototype.SaveOption = function () {
        this.viewCtrl.dismiss();
    };
    PopupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-popup',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/modal-popup/modal-popup.html"*/'<ion-content class="main-view">\n    <div class="modal_content">   \n            <ion-item style="height: 68px;" text-wrap text-center no-lines>\n                <ion-label item-end style="font-size: 1.2em;color:#000;">\n                    {{setPopup.title}}\n                </ion-label>  \n            </ion-item> \n             \n            <div class="horizontalline"></div> \n             \n            <ion-item no-lines style="height: auto; vertical-align: middle;" text-center text-wrap>\n                <ion-label item-end style="font-size: 1.1em;color:#000;">\n                    {{setPopup.text_message}}\n                </ion-label>   \n            </ion-item>   \n\n            <div class="horline"></div> \n             \n            <ion-item no-lines  text-center style="height: 60px;vertical-align: middle;">\n                <ion-label *ngIf="setPopup.isCancel" style="font-size: 1.2em;" (click)="chooseItem(false)">\n                        <p style="font-size: 1.2em;">{{setPopup.btn_cancel}}</p>\n                </ion-label> \n                <ion-label item-end style="font-size: 1.2em;" (click)="chooseItem(true)">\n                        <p style="font-size: 1.2em;">{{setPopup.btn_confirm}}</p>\n                </ion-label> \n          </ion-item>   \n    </div>\n</ion-content>\n'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/modal-popup/modal-popup.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], PopupPage);
    return PopupPage;
}());

//# sourceMappingURL=modal-popup.js.map

/***/ }),

/***/ 172:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 172;

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeSearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_place_add_place__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_account_account__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__modal_popup_modal_popup__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// declare var maps;
var HomeSearchPage = /** @class */ (function () {
    function HomeSearchPage(navCtrl, pAccount, navParams, zone, modalCtrl, viewCtrl) {
        this.navCtrl = navCtrl;
        this.pAccount = pAccount;
        this.navParams = navParams;
        this.zone = zone;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.ipModel = {
            origin_a: "",
            origin_b: "",
            destination_a: "",
            destination_b: ""
        };
        this.setOrigin = {
            lat: "",
            lng: ""
        };
        this.setDestination = {
            lat: "",
            lng: ""
        };
        this.isDestinationScr = true;
        this.label_myLocation_a = "";
        this.label_myLocation_b = "";
        this.placeList = [];
        this.service = new google.maps.places.AutocompleteService();
        this.isMyPlace = false;
        console.log(navParams.get('location_a'));
        this.label_myLocation_a = navParams.get('location_a');
        this.label_myLocation_b = navParams.get('location_b');
        this.setOrigin = navParams.get('userlocation');
        this.ipModel.origin_a = this.label_myLocation_a;
        this.ipModel.origin_b = this.label_myLocation_b;
        this.originItems = [];
        this.originQuery = {
            query: ''
        };
        if (this.label_myLocation_a != "") {
            this.originQuery.query = this.label_myLocation_a;
        }
        // this.originQuery.query = this.label_myLocation_a;
        this.destinationItems = [];
        this.destinationQuery = {
            query: ''
        };
        this.loadPlaceLists();
    }
    HomeSearchPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        setTimeout(function () {
            _this.idDestination.setFocus();
        }, 150);
    };
    HomeSearchPage.prototype.loadPlaceLists = function () {
        var _this = this;
        this.placeList = [];
        this.pAccount.UserListingPlace().then(function (r) {
            if (r.status == 200 && r.message == "Success") {
                //console.log(r.data);
                _this.placeList = r.data;
                if (r.data.length > 0) {
                    _this.isMyPlace = true;
                }
            }
        });
    };
    // ngAfterViewInit() {
    //   setTimeout(() => {
    //      this.idDestination.setFocus();
    //   }, 150);
    // }
    HomeSearchPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    HomeSearchPage.prototype.clickDestination = function () {
        this.destinationItems = [];
        this.originItems = [];
        this.isDestinationScr = true;
        this.loadPlaceLists();
    };
    HomeSearchPage.prototype.clickOrigin = function () {
        this.destinationItems = [];
        this.originItems = [];
        this.isDestinationScr = false;
        this.loadPlaceLists();
    };
    HomeSearchPage.prototype.DestinationSearch = function () {
        this.isMyPlace = false;
        this.placeList = [];
        this.isDestinationScr = true;
        if (this.destinationQuery.query == '') {
            this.destinationItems = [];
            return;
        }
        // other types available in the API: 'geocode' , 'locality' , 'political', 'establishment', 'regions', and 'cities'
        var me = this;
        this.service.getPlacePredictions({
            input: this.destinationQuery.query,
            componentRestrictions: {
                country: 'TH'
            }, types: ['establishment'], language: 'th-TH'
        }, function (predictions, status) {
            me.destinationItems = [];
            me.zone.run(function () {
                if (predictions != null) {
                    predictions.forEach(function (prediction) {
                        me.destinationItems.push({
                            label_a: prediction.structured_formatting.main_text,
                            label_b: prediction.description
                        });
                    });
                }
            });
        });
    };
    HomeSearchPage.prototype.OriginSearch = function () {
        this.isMyPlace = false;
        this.placeList = [];
        this.isDestinationScr = false;
        if (this.originQuery.query == '') {
            this.originItems = [];
            return;
        }
        // other types available in the API: 'establishment', 'regions', and 'cities'
        var me = this;
        this.service.getPlacePredictions({
            input: this.originQuery.query,
            componentRestrictions: {
                country: 'TH'
            },
            types: ['establishment'],
            language: 'th-TH'
        }, function (predictions, status) {
            me.originItems = [];
            me.zone.run(function () {
                if (predictions != null) {
                    predictions.forEach(function (prediction) {
                        me.originItems.push({
                            label_a: prediction.structured_formatting.main_text,
                            label_b: prediction.description
                        });
                    });
                }
            });
        });
    };
    HomeSearchPage.prototype.chooseCurrentOriginItem = function () {
        var _this = this;
        this.originQuery.query = this.label_myLocation_a;
        this.ipModel.origin_a = this.label_myLocation_a;
        this.ipModel.origin_b = this.label_myLocation_b;
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': this.label_myLocation_b }, function (results, status) {
            _this.setOrigin = {
                lat: results[0].geometry.location.lat(),
                lng: results[0].geometry.location.lng()
            };
            //alert("lat: " + this.setOrigin.lat + ", long: " + this.setOrigin.lng);
            //alert(this.setDestination.lat);
            if (typeof _this.setDestination.lat !== "undefined" && _this.setDestination.lat != "") {
                _this.goBackToMap({
                    origin: _this.setOrigin,
                    desti: _this.setDestination
                });
            }
            else {
                _this.idDestination.setFocus();
                _this.destinationItems = [];
                _this.originItems = [];
                _this.isDestinationScr = true;
            }
        });
    };
    HomeSearchPage.prototype.choosePlaceOriginItem = function (item) {
        var _this = this;
        this.originQuery.query = item.name;
        this.ipModel.origin_a = item.name;
        this.setOrigin = {
            lat: item.lat,
            lng: item.long
        };
        var geocoder = new google.maps.Geocoder;
        geocoder.geocode({ 'location': this.setOrigin }, function (results, status) {
            //console.log(status);
            //console.log(results); 
            //this.ipModel.origin_a = results[0].address_components[0].short_name;
            _this.ipModel.origin_b = results[0].formatted_address;
        });
        if (typeof this.setDestination.lat !== "undefined" && this.setDestination.lat != "") {
            if (this.setOrigin.lat == this.setDestination.lat &&
                this.setOrigin.lng == this.setDestination.lng) {
                this.openPopAlertMessage("Please not choose same location");
            }
            else {
                this.goBackToMap({
                    origin: this.setOrigin,
                    desti: this.setDestination
                });
            }
        }
        else {
            this.idDestination.setFocus();
            this.destinationItems = [];
            this.originItems = [];
            this.isDestinationScr = true;
        }
    };
    HomeSearchPage.prototype.choosePlaceDestinationItem = function (item) {
        var _this = this;
        this.destinationQuery.query = item.name;
        this.ipModel.destination_a = item.name;
        this.setDestination = {
            lat: item.lat,
            lng: item.long
        };
        var geocoder = new google.maps.Geocoder;
        geocoder.geocode({ 'location': this.setDestination }, function (results, status) {
            //console.log(status);
            //console.log(results); 
            //this.ipModel.destination_a = results[0].address_components[0].short_name;
            _this.ipModel.destination_b = results[0].formatted_address;
        });
        if (typeof this.setOrigin.lat !== "undefined") {
            if (this.setOrigin.lat == this.setDestination.lat &&
                this.setOrigin.lng == this.setDestination.lng) {
                this.openPopAlertMessage("Please not choose same location");
            }
            else {
                this.goBackToMap({
                    origin: this.setOrigin,
                    desti: this.setDestination
                });
            }
        }
        else {
            this.idOrigin.setFocus();
            this.destinationItems = [];
            this.originItems = [];
            this.isDestinationScr = false;
        }
    };
    HomeSearchPage.prototype.chooseDestinationItem = function (item, set) {
        this.geo = item;
        this.destinationQuery.query = item.label_a;
        this.ipModel.destination_a = item.label_a;
        this.ipModel.destination_b = item.label_b;
        if (set == '1') {
            this.geoCodeDestination(this.geo); //convert Address to lat and long
        }
        else if (set == '2') {
            this.AddPlaces(this.geo);
        }
        else if (set == '3') {
            this.DelPlaces(this.geo);
        }
    };
    HomeSearchPage.prototype.DelPlaces = function (item) {
        var _this = this;
        this.pAccount.UserDelPlace(item.place_id).then(function (r) {
            // console.log(r);
            if (r.status == 200 && r.message == "Success") {
                _this.dismiss();
            }
            else {
                _this.openPopAlertMessage(r.message);
            }
        });
    };
    HomeSearchPage.prototype.geoCodeDestination = function (address) {
        var _this = this;
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address.label_b }, function (results, status) {
            _this.setDestination = {
                lat: results[0].geometry.location.lat(),
                lng: results[0].geometry.location.lng()
            };
            //alert("lat: " + this.setDestination.lat + ", long: " + this.setDestination.lng);
            //alert(this.setOrigin.lat); 
            //console.log(this.setOrigin);
            if (typeof _this.setOrigin.lat !== "undefined") {
                _this.goBackToMap({
                    origin: _this.setOrigin,
                    desti: _this.setDestination
                });
            }
            else {
                _this.idOrigin.setFocus();
                _this.destinationItems = [];
                _this.originItems = [];
                _this.isDestinationScr = false;
            }
        });
    };
    HomeSearchPage.prototype.chooseOriginItem = function (item, set) {
        this.geo = item;
        this.originQuery.query = item.label_a;
        this.ipModel.origin_a = item.label_a;
        this.ipModel.origin_b = item.label_b;
        if (set == '1') {
            this.geoCodeOrigin(this.geo); //convert Address to lat and long
        }
        else if (set == '2') {
            this.AddPlaces(this.geo);
        }
        else if (set == '3') {
            this.DelPlaces(this.geo);
        }
    };
    HomeSearchPage.prototype.geoCodeOrigin = function (address) {
        var _this = this;
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address.label_b }, function (results, status) {
            _this.setOrigin = {
                lat: results[0].geometry.location.lat(),
                lng: results[0].geometry.location.lng()
            };
            //alert("lat: " + this.setOrigin.lat + ", long: " + this.setOrigin.lng);
            //alert(this.setDestination.lat);
            if (typeof _this.setDestination.lat !== "undefined" && _this.setDestination.lat != "") {
                _this.goBackToMap({
                    origin: _this.setOrigin,
                    desti: _this.setDestination
                });
            }
            else {
                _this.idDestination.setFocus();
                _this.destinationItems = [];
                _this.originItems = [];
                _this.isDestinationScr = true;
            }
        });
    };
    HomeSearchPage.prototype.AddPlaces = function (item) {
        var _this = this;
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': item.label_b }, function (results, status) {
            var place = {
                lat: results[0].geometry.location.lat(),
                lng: results[0].geometry.location.lng(),
                name: item.label_a,
                address: item.label_b
            };
            var addPlacesModal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__add_place_add_place__["a" /* AddPlacePage */], {
                place: place
            });
            addPlacesModal.onDidDismiss(function (data) {
                _this.navCtrl.pop();
            });
            addPlacesModal.present();
        });
    };
    HomeSearchPage.prototype.goBackToMap = function (data) {
        var datas = {
            select: data,
            label: this.ipModel
        };
        this.viewCtrl.dismiss(datas);
    };
    HomeSearchPage.prototype.openPopAlertMessage = function (msg) {
        var set_Popup = {
            title: "Pop-up Request",
            text_message: msg,
            btn_cancel: "",
            btn_confirm: "Done",
            isCancel: false
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            console.log(popup);
        });
        popModal.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('idOrigin'),
        __metadata("design:type", Object)
    ], HomeSearchPage.prototype, "idOrigin", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('idDestination'),
        __metadata("design:type", Object)
    ], HomeSearchPage.prototype, "idDestination", void 0);
    HomeSearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home-search',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/home-search/home-search.html"*/'<!--\n  Generated template for the HomeSearchPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header> \n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button (click)="dismiss()" color="light">\n        <ion-icon name="ios-arrow-round-back-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title></ion-title>\n  </ion-navbar> \n</ion-header>\n\n\n<ion-content>\n\n\n  <div class="search-panel" padding>\n    <div class="image-panel">\n      <div class="icon-line"></div>\n      <img src="assets/images/icon-origin.png" class="icon-origin" />\n      <img src="assets/images/icon-destination.png" class="icon-destination"/>\n    </div>\n    <div class="input-data">\n        <ion-searchbar name="ipOrigin" #idOrigin [(ngModel)]="originQuery.query" placeholder="Origin" [showCancelButton]="false" (ionInput)="OriginSearch()" (click)="clickOrigin()"></ion-searchbar> \n        <ion-searchbar name="ipDestination" #idDestination [(ngModel)]="destinationQuery.query" placeholder="Destination" [showCancelButton]="false" (ionInput)="DestinationSearch()" (click)="clickDestination()"></ion-searchbar> \n    </div>\n \n  </div>\n  <div> \n      <ion-list *ngIf="!isDestinationScr">\n          <ion-item-divider color="light">My Location</ion-item-divider>\n          <ion-item-sliding>\n              <ion-item text-wrap (click)="chooseCurrentOriginItem()">\n                  <img src="assets/images/placeholder.png" width="20"/>\n                  {{label_myLocation_a}}\n                  <p>{{label_myLocation_b}}</p>\n              </ion-item>  \n          </ion-item-sliding>  \n\n          <ion-item-divider *ngIf="isMyPlace" color="light">My Places</ion-item-divider>\n          <ion-item-sliding *ngFor="let item of placeList">\n            <ion-item text-wrap (click)="choosePlaceOriginItem(item)">\n                    <img *ngIf="item.type==\'home\'" src="assets/images/home.png" width="20"/>\n                    <img *ngIf="item.type==\'work\'" src="assets/images/work.png" width="20"/>\n                    <img *ngIf="item.type==\'other\'" src="assets/images/placeholder.png" width="20"/>\n                    <span *ngIf="item.type==\'home\'">Home</span>\n                    <span *ngIf="item.type==\'work\'">Work</span>\n                    <span *ngIf="item.type==\'other\'">Other</span> \n                    <p>{{item.name}}</p>\n            </ion-item> \n            <ion-item-options icon-start>\n              <button ion-button  (click)="chooseOriginItem(item,\'3\')">\n                  <ion-icon name="ios-trash-outline"></ion-icon>\n                  Delete\n              </button>\n            </ion-item-options>\n         </ion-item-sliding>  \n\n          <ion-item-sliding *ngFor="let item of originItems">\n            <ion-item text-wrap (click)="chooseOriginItem(item,\'1\')">\n                <img src="assets/images/placeholder.png" width="20"/>\n                {{item.label_a}}\n                <p>{{item.label_b}}</p>\n            </ion-item> \n            <ion-item-options icon-start>\n                <button ion-button  (click)="chooseOriginItem(item,\'2\')">\n                    <ion-icon name="ios-add-circle-outline"></ion-icon>\n                    Place\n                </button>\n              </ion-item-options>\n          </ion-item-sliding>  \n        </ion-list>\n \n        <ion-list *ngIf="isDestinationScr">\n            <ion-item-divider *ngIf="isMyPlace" color="light">My Places</ion-item-divider> \n          <ion-item-sliding *ngFor="let item of placeList">\n            <ion-item text-wrap (click)="choosePlaceDestinationItem(item)">\n                <img *ngIf="item.type==\'home\'" src="assets/images/home.png" width="20"/>\n                <img *ngIf="item.type==\'work\'" src="assets/images/work.png" width="20"/>\n                <img *ngIf="item.type==\'other\'" src="assets/images/placeholder.png" width="20"/>\n                <span *ngIf="item.type==\'home\'">Home</span>\n                <span *ngIf="item.type==\'work\'">Work</span>\n                <span *ngIf="item.type==\'other\'">Other</span> \n                <p>{{item.name}}</p>\n            </ion-item> \n            <ion-item-options icon-start>\n              <button ion-button  (click)="chooseDestinationItem(item,\'3\')">\n                  <ion-icon name="ios-trash-outline"></ion-icon>\n                  Delete\n              </button>\n            </ion-item-options> \n         </ion-item-sliding>  \n\n            <ion-item-sliding *ngFor="let item of destinationItems" >\n              <ion-item text-wrap (click)="chooseDestinationItem(item,\'1\')">\n                  <img src="assets/images/placeholder.png" width="20"/>\n                  {{item.label_a}}\n                  <p>{{item.label_b}}</p>\n              </ion-item> \n              <ion-item-options icon-start>\n                  <button ion-button  (click)="chooseDestinationItem(item,\'2\')">\n                      <ion-icon name="ios-add-circle-outline"></ion-icon>\n                      Place\n                  </button>\n                </ion-item-options>\n            </ion-item-sliding>  \n        </ion-list> \n  </div>\n</ion-content>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/home-search/home-search.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_account_account__["a" /* AccountProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], HomeSearchPage);
    return HomeSearchPage;
}());

//# sourceMappingURL=home-search.js.map

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddPlacePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_account_account__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modal_popup_modal_popup__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddPlacePage = /** @class */ (function () {
    function AddPlacePage(pAccount, actionSheetCtrl, loadingCtrl, toastCtrl, navParams, platform, modalCtrl, viewCtrl) {
        this.pAccount = pAccount;
        this.actionSheetCtrl = actionSheetCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.platform = platform;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.ipName = "";
        this.Address = "";
        this.lat = "";
        this.lng = "";
        this.type = "1";
        //console.log(navParams.get('place'));
        var data = navParams.get('place');
        this.ipName = data.name;
        this.Address = data.address;
        this.lat = data.lat;
        this.lng = data.lng;
    }
    AddPlacePage.prototype.Save = function () {
        var _this = this;
        if (this.ipName != "") {
            this.pAccount.UserAddPlace({
                lat: this.lat,
                long: this.lng,
                type: this.type,
                name: this.ipName
            }).then(function (r) {
                // console.log(r);
                if (r.status == 200 && r.message == "Success") {
                    _this.dismiss();
                }
                else {
                    _this.openPopAlertMessage(r.message);
                }
            });
        }
        else {
            this.openPopAlertMessage("Enter your place");
        }
    };
    AddPlacePage.prototype.openPopAlertMessage = function (msg) {
        var set_Popup = {
            title: "Pop-up Request",
            text_message: msg,
            btn_cancel: "",
            btn_confirm: "Done",
            isCancel: false
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            // console.log(popup);    
        });
        popModal.present();
    };
    AddPlacePage.prototype.ionViewDidLoad = function () {
    };
    AddPlacePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddPlacePage.prototype.segmentChanged = function () {
        // console.log('Segment selected', this.type);  
    };
    AddPlacePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-place',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/add-place/add-place.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button (click)="dismiss()" color="light">\n        <ion-icon name="ios-arrow-round-back-outline"></ion-icon>\n      </button>\n    </ion-buttons> \n  </ion-navbar>\n</ion-header>\n\n<ion-content> \n  <div class="panel-top">\n    <h2>ADD PLACE</h2> \n  </div> \n  <br>\n  <ion-segment [(ngModel)]="type" color="primary" (ionChange)="segmentChanged()">\n      <ion-segment-button value="1" >\n        <h4>Home</h4>\n      </ion-segment-button>\n      <ion-segment-button value="2" >\n        <h4>Work</h4>\n      </ion-segment-button>\n      <ion-segment-button value="3" >\n        <h4>Other</h4>\n      </ion-segment-button>\n  </ion-segment>\n  <br> \n  <ion-item class="i-item">\n    <p>Name</p>\n    <input type="text" [(ngModel)]="ipName" (keyup.enter)="Save()" />\n  </ion-item> \n  <ion-item class="i-item">\n    <p>Address</p> \n    <ion-label text-wrap style="margin-left: 55px;">{{Address}}</ion-label> \n  </ion-item> \n  <ion-item class="i-item" no-lines></ion-item>\n  <ion-item class="i-item" no-lines></ion-item> \n  <div padding class="btn-action">\n    <button ion-button full color="primary" (click)="Save()">CONFIRM</button>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/add-place/add-place.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_account_account__["a" /* AccountProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], AddPlacePage);
    return AddPlacePage;
}());

//# sourceMappingURL=add-place.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalDeclaration; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var GlobalDeclaration = /** @class */ (function () {
    function GlobalDeclaration() {
        this.BaseUrl = "https://limoapp.me/api";
    }
    GlobalDeclaration = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], GlobalDeclaration);
    return GlobalDeclaration;
}());

//# sourceMappingURL=Declaration.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectPayment; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_account_account__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SelectPayment = /** @class */ (function () {
    function SelectPayment(viewCtrl, navParams, pAccount) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.pAccount = pAccount;
        this.paymentList = [];
        this.id_payment = "";
        this.id_payment = navParams.get('id_payment');
        this.initPaymentLists();
    }
    SelectPayment.prototype.initPaymentLists = function () {
        var _this = this;
        this.pAccount.UserPaymentListing().then(function (r) {
            if (r.status == 200 && r.message == "Success") {
                _this.paymentList = r.data;
            }
        });
    };
    SelectPayment.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    SelectPayment.prototype.chooseItem = function (item) {
        //console.log('modal > chooseItem > item > ', item);
        this.viewCtrl.dismiss(item);
    };
    SelectPayment = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-select-payment',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/modal-set-card/modal-select-payment.html"*/'<!-- \n    <ion-header>\n  <ion-navbar color="secondary">\n    <ion-title></ion-title>\n  </ion-navbar>\n</ion-header>\n -->\n\n <ion-content class="main-view">\n        <div class="modal_content"> \n            <ion-scroll style="height: 340px" scrollY="true">\n            <ion-list scroll="true">\n                <ion-item-divider no-lines  color="light">Payment Card\n                    <ion-icon name="ios-close" item-end (click)="dismiss()"></ion-icon>\n                </ion-item-divider>\n                <div class="horline"></div> \n                <ion-item  no-lines *ngFor="let item of paymentList" (click)="chooseItem(item)">\n                    <img item-start src="assets/images/credit-card.png" width="38" />  \n                     <h5 style="padding: 6px;">{{item.card_number}}</h5>\n                     <ion-icon *ngIf="item.id== id_payment" item-right color="primary" name="ios-checkmark-circle"></ion-icon>\n                    <!-- <p item-end>THB {{item.fare}}</p>  -->\n                </ion-item>\n                <ion-item no-lines></ion-item> \n                <ion-item no-lines></ion-item> \n            </ion-list>\n            </ion-scroll>\n        </div>\n    </ion-content>\n'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/modal-set-card/modal-select-payment.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_account_account__["a" /* AccountProvider */]])
    ], SelectPayment);
    return SelectPayment;
}());

//# sourceMappingURL=modal-select-payment.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectOption; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SelectOption = /** @class */ (function () {
    function SelectOption(viewCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.textNote = "";
        this.textNote = navParams.get('text');
    }
    SelectOption.prototype.dismiss = function () {
        var text = this.textNote;
        this.viewCtrl.dismiss(text);
    };
    SelectOption.prototype.chooseItem = function (item) {
        //console.log('modal > chooseItem > item > ', item);
        this.viewCtrl.dismiss(item);
    };
    SelectOption.prototype.SaveOption = function () {
        var text = this.textNote;
        this.viewCtrl.dismiss(text);
    };
    SelectOption = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-select-option',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/modal-option/modal-option.html"*/'<!-- \n    <ion-header>\n  <ion-navbar color="secondary">\n    <ion-title></ion-title>\n  </ion-navbar>\n</ion-header>\n -->\n\n <ion-content class="main-view">\n        <div class="modal_content"> \n            <ion-scroll no-padding style="height: 100%" scrollY="true">\n            <ion-list scroll="true">\n                <ion-item-divider no-lines  color="light">Option\n                    <ion-icon name="ios-close" item-end (click)="dismiss()"></ion-icon>\n                </ion-item-divider> \n\n                <div class="horline"></div> \n\n                <!-- <ion-item style="height: 60px" no-lines> \n                    <ion-label item-start>Tag</ion-label>\n                    <ion-icon item-end name="ios-person-outline"><p>Personal</p></ion-icon> \n                </ion-item> -->\n                <ion-item no-lines>  \n                </ion-item>\n                <ion-item no-lines>  \n                    <!-- <input type="text" [(ngModel)]="textNote" /> -->\n                    <ion-textarea class="trounded" style="border-radius: 6%;" no-margin text-wrap rows="5"  [(ngModel)]="textNote"  placeholder="Note To Drive" full ></ion-textarea>\n                </ion-item> \n                <div class="horline"></div>  \n                <ion-item style="height: 80px;" no-lines>\n                    <button ion-button full color="primary" style="height: 50px;font-size: 1em;" (click)="SaveOption()">Confirm</button>\n                </ion-item>\n            </ion-list>\n            </ion-scroll>\n        </div>\n    </ion-content>\n'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/modal-option/modal-option.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], SelectOption);
    return SelectOption;
}());

//# sourceMappingURL=modal-option.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectCars; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_order_order__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SelectCars = /** @class */ (function () {
    function SelectCars(viewCtrl, navParams, pOrder) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.pOrder = pOrder;
        this.list_cars = [];
        var durat = navParams.get('Duration');
        var dist = navParams.get('Distance');
        this.loadList(durat, dist);
    }
    SelectCars.prototype.loadList = function (Duration, Distance) {
        var _this = this;
        this.pOrder.OrderCalculate(Duration, Distance).then(function (r) {
            //console.log(r);
            _this.list_cars = r.data;
        });
    };
    SelectCars.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    SelectCars.prototype.chooseItem = function (item) {
        // console.log('modal > chooseItem > item > ', item);
        this.viewCtrl.dismiss(item);
    };
    SelectCars = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-select-cars',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/modal-cars/modal-cars.html"*/'<!-- \n    <ion-header>\n  <ion-navbar color="secondary">\n    <ion-title></ion-title>\n  </ion-navbar>\n</ion-header>\n -->\n<ion-content class="main-view">\n    <div class="modal_content"> \n        <ion-scroll style="height: 340px" scrollY="true">\n            <ion-list scroll="true">\n                <ion-item-divider no-lines  color="light">Booking\n                    <ion-icon name="ios-close" item-end (click)="dismiss()"></ion-icon>\n                </ion-item-divider>\n                <div class="horline"></div> \n                <ion-item no-lines *ngFor="let item of list_cars" (click)="chooseItem(item)">\n                    <img item-start [src]="item.icon" width="42" />  \n                    <h5 style="padding: 6px;">{{item.name}} </h5>\n                    <p item-end>THB {{item.fare}}</p>  \n                </ion-item>\n                <ion-item no-lines></ion-item> \n                <ion-item no-lines></ion-item> \n                <ion-item no-lines></ion-item> \n            </ion-list>\n        </ion-scroll>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/modal-cars/modal-cars.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_order_order__["a" /* OrderProvider */]])
    ], SelectCars);
    return SelectCars;
}());

//# sourceMappingURL=modal-cars.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectPlace; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_account_account__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SelectPlace = /** @class */ (function () {
    function SelectPlace(viewCtrl, pAccount) {
        this.viewCtrl = viewCtrl;
        this.pAccount = pAccount;
        this.placeList = [];
        this.initPlaceLists();
    }
    SelectPlace.prototype.initPlaceLists = function () {
        var _this = this;
        this.pAccount.UserListingPlace().then(function (r) {
            if (r.status == 200 && r.message == "Success") {
                _this.placeList = r.data;
            }
        });
    };
    SelectPlace.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    SelectPlace.prototype.chooseItem = function (item) {
        // console.log('modal > chooseItem > item > ', item);
        this.viewCtrl.dismiss(item);
    };
    SelectPlace = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-place',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/modal-place/modal-place.html"*/'<!-- \n    <ion-header>\n  <ion-navbar color="secondary">\n    <ion-title></ion-title>\n  </ion-navbar>\n</ion-header>\n -->\n\n <ion-content class="main-view">\n        <div class="modal_content"> \n            <ion-scroll style="height: 340px" scrollY="true">\n            <ion-list scroll="true">\n                <ion-item-divider no-lines  color="light">Place\n                    <ion-icon name="ios-close" item-end (click)="dismiss()"></ion-icon>\n                </ion-item-divider>\n                <div class="horline"></div> \n                <ion-item-sliding *ngFor="let item of placeList" (click)="chooseItem(item)">\n                    <ion-item text-wrap>\n                        <img *ngIf="item.type==1" src="assets/images/home.png" width="20"/>\n                        <img *ngIf="item.type==2" src="assets/images/work.png" width="20"/>\n                        <img *ngIf="item.type==3" src="assets/images/placeholder.png" width="20"/>\n                        <span *ngIf="item.type==1">Home</span>\n                        <span *ngIf="item.type==2">Work</span>\n                        <span *ngIf="item.type==3">Other</span> \n                        <p>{{item.name}}</p>\n                    </ion-item>  \n                </ion-item-sliding>  \n                <ion-item></ion-item> \n            </ion-list>\n            </ion-scroll>\n        </div>\n    </ion-content>\n'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/modal-place/modal-place.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"], __WEBPACK_IMPORTED_MODULE_2__providers_account_account__["a" /* AccountProvider */]])
    ], SelectPlace);
    return SelectPlace;
}());

//# sourceMappingURL=modal-place.js.map

/***/ }),

/***/ 223:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentAddPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PaymentAddPage = /** @class */ (function () {
    function PaymentAddPage(domSanitizer, navCtrl, viewCtrl, loadingCtrl) {
        this.domSanitizer = domSanitizer;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.loadingCtrl = loadingCtrl;
        this.loadurl();
    }
    PaymentAddPage.prototype.ionViewWillLeave = function () {
        this.hiddenInput.setFocus();
        this.hiddenInput.setBlur();
    };
    PaymentAddPage.prototype.loadurl = function () {
        var loader = this.loadingCtrl.create({
            spinner: 'dots',
            content: ""
        });
        loader.present();
        setTimeout(function (x) {
            loader.dismiss();
        }, 1500);
        this.url = this.domSanitizer.bypassSecurityTrustResourceUrl("https://limoapp.me/add_card?user_id=" + localStorage.userId);
    };
    PaymentAddPage.prototype.dismiss = function () {
        // let iframe1 = document.getElementById('iframe1').baseURI; 
        this.viewCtrl.dismiss();
    };
    PaymentAddPage.prototype.popKeyb = function () {
        this.hiddenInput.setFocus();
        this.hiddenInput.setBlur();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('hiddenInput'),
        __metadata("design:type", Object)
    ], PaymentAddPage.prototype, "hiddenInput", void 0);
    PaymentAddPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-payment-add',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/payment-add/payment-add.html"*/'<ion-header (click)="popKeyb()" >\n  <ion-navbar>\n      <ion-buttons left>\n          <button ion-button (click)="dismiss()" color="light">\n              <ion-icon name="ios-arrow-round-back-outline"></ion-icon>\n          </button>\n      </ion-buttons>\n      <ion-title center>Add Card</ion-title>\n  </ion-navbar> \n</ion-header>\n\n<ion-content padding>   \n      <iframe (click)="popKeyb()"  name="iframe1" #iframe1 class="webPage"  [src]="url" allowfullscreen></iframe>  \n\n      <ion-input #hiddenInput type="text" style="width:0;height:0;padding:0;margin:0;opacity:0;"></ion-input>  \n \n </ion-content>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/payment-add/payment-add.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["LoadingController"]])
    ], PaymentAddPage);
    return PaymentAddPage;
}());

//# sourceMappingURL=payment-add.js.map

/***/ }),

/***/ 224:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddCardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AddCardPage = /** @class */ (function () {
    function AddCardPage(domSanitizer, viewCtrl) {
        this.domSanitizer = domSanitizer;
        this.viewCtrl = viewCtrl;
    }
    AddCardPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddCardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-card',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/add-card/add-card.html"*/'\n<ion-header>\n  <ion-navbar>\n      <ion-buttons left>\n          <button ion-button (click)="dismiss()" color="light">\n              <ion-icon name="ios-arrow-round-back-outline"></ion-icon>\n          </button>\n      </ion-buttons>\n      <ion-title full text-center>Add Card</ion-title>\n      <ion-buttons end>\n        <button ion-button>\n            <ion-icon name="ios-arrow-round-back-outline" color="primary"></ion-icon>\n        </button>\n    </ion-buttons>\n  </ion-navbar> \n</ion-header>\n\n<ion-content padding>   \n \n            <ion-item no-lines style="height: 100px; margin-left: 10px;">\n                <img  src="assets/images/credit-card.png" style="width: 80px;" />\n            </ion-item>\n            <ion-item no-lines>\n                <ion-input type="text" placeholder="Card name"></ion-input>\n            </ion-item>\n            <ion-item no-lines>\n                <ion-input type="tel" placeholder="Card number" ></ion-input>\n            </ion-item>\n            <ion-item no-lines>\n                <ion-input type="tel" style="width: 50px;" placeholder="MM/YY"></ion-input><ion-input type="tel" style="width: 50px;" placeholder="CVC"></ion-input>\n            </ion-item> \n\n        <div class="panel-action">\n            <ion-icon name="ios-arrow-round-forward-outline" color="light"></ion-icon>\n        </div>\n  \n</ion-content>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/add-card/add-card.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ViewController"]])
    ], AddCardPage);
    return AddCardPage;
}());

//# sourceMappingURL=add-card.js.map

/***/ }),

/***/ 225:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentOmisePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PaymentOmisePage = /** @class */ (function () {
    function PaymentOmisePage(domSanitizer, loadingCtrl, navParams, viewCtrl) {
        this.domSanitizer = domSanitizer;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.str_url = '';
        this.str_url = navParams.get('url');
        this.loadurl(this.str_url);
    }
    PaymentOmisePage.prototype.ionViewWillLeave = function () {
        this.hiddenInput.setFocus();
        this.hiddenInput.setBlur();
    };
    PaymentOmisePage.prototype.loadurl = function (url_) {
        var loader = this.loadingCtrl.create({
            spinner: 'dots',
            content: ""
        });
        loader.present();
        setTimeout(function (x) {
            loader.dismiss();
        }, 1500);
        this.url = this.domSanitizer.bypassSecurityTrustResourceUrl(url_);
    };
    PaymentOmisePage.prototype.dismiss = function () {
        // let iframe1 = document.getElementById('iframe1').baseURI; 
        this.viewCtrl.dismiss();
    };
    PaymentOmisePage.prototype.popKeyb = function () {
        this.hiddenInput.setFocus();
        this.hiddenInput.setBlur();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('hiddenInput'),
        __metadata("design:type", Object)
    ], PaymentOmisePage.prototype, "hiddenInput", void 0);
    PaymentOmisePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-payment-omise',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/payment-omise/payment-omise.html"*/'\n<ion-header>\n  <ion-navbar>\n      <ion-buttons left>\n          <button ion-button (click)="dismiss()" color="light">\n              <ion-icon name="ios-arrow-round-back-outline"></ion-icon>\n          </button>\n      </ion-buttons>\n      <ion-title center>Omise Payment</ion-title>\n  </ion-navbar> \n</ion-header>\n\n<ion-content padding>  \n  <iframe (click)="popKeyb()"  name="iframe1" #iframe1 class="webPage"  [src]="url" allowfullscreen></iframe>\n\n  <ion-input #hiddenInput type="text" style="width:0;height:0;padding:0;margin:0;opacity:0;"></ion-input>  \n  \n</ion-content>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/payment-omise/payment-omise.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ViewController"]])
    ], PaymentOmisePage);
    return PaymentOmisePage;
}());

//# sourceMappingURL=payment-omise.js.map

/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PhoneInputPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__validate_otp_validate_otp__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_account_account__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__password_input_password_input__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_select_searchable__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modal_popup_modal_popup__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PhoneInputPage = /** @class */ (function () {
    function PhoneInputPage(navCtrl, modalCtrl, navParams, pAccount) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.pAccount = pAccount;
        this.ipPhoneNumber = "";
        this.ipCountryCode = "TH";
        this.cCountrys = [];
        this.phoneValidate = false;
        this.message_phone_validate = "Check Your Phone Number";
        this.selectedCountry = {};
        this.countries = [{
                name: "United States",
                dial_code: "+1",
                code: "US"
            }, {
                name: "Canada",
                dial_code: "+1",
                code: "CA"
            }, {
                name: "China",
                dial_code: "+86",
                code: "CN"
            }, {
                name: "France",
                dial_code: "+33",
                code: "FR"
            }, {
                name: "Germany",
                dial_code: "+49",
                code: "DE"
            }, {
                name: "Greece",
                dial_code: "+30",
                code: "GR"
            }, {
                name: "Indonesia",
                dial_code: "+62",
                code: "ID"
            }, {
                name: "Iraq",
                dial_code: "+964",
                code: "IQ"
            }, {
                name: "Italy",
                dial_code: "+39",
                code: "IT"
            }, {
                name: "Japan",
                dial_code: "+81",
                code: "JP"
            }, {
                name: "Malaysia",
                dial_code: "+60",
                code: "MY"
            }, {
                name: "Myanmar",
                dial_code: "+95",
                code: "MM"
            }, {
                name: "Netherlands",
                dial_code: "+31",
                code: "NL"
            }, {
                name: "Panama",
                dial_code: "+507",
                code: "PA"
            }, {
                name: "Peru",
                dial_code: "+51",
                code: "PE"
            }, {
                name: "Philippines",
                dial_code: "+63",
                code: "PH"
            }, {
                name: "Qatar",
                dial_code: "+974",
                code: "QA"
            }, {
                name: "Saudi Arabia",
                dial_code: "+966",
                code: "SA"
            }, {
                name: "Singapore",
                dial_code: "+65",
                code: "SG"
            }, {
                name: "South Africa",
                dial_code: "+27",
                code: "ZA"
            }, {
                name: "Spain",
                dial_code: "+34",
                code: "ES"
            }, {
                name: "Sri Lanka",
                dial_code: "+94",
                code: "LK"
            }, {
                name: "Switzerland",
                dial_code: "+41",
                code: "CH"
            }, {
                name: "Thailand",
                dial_code: "+66",
                code: "TH"
            }, {
                name: "United Kingdom",
                dial_code: "+44",
                code: "GB"
            }, {
                name: "Brunei Darussalam",
                dial_code: "+673",
                code: "BN"
            }, {
                name: "Hong Kong",
                dial_code: "+852",
                code: "HK"
            }, {
                name: "Korea, Republic of",
                dial_code: "+82",
                code: "KR"
            }, {
                name: "Russia",
                dial_code: "+7",
                code: "RU"
            }, {
                name: "Taiwan, Province of China",
                dial_code: "+886",
                code: "TW"
            }, {
                name: "Viet Nam",
                dial_code: "+84",
                code: "VN"
            }];
    }
    PhoneInputPage.prototype.sChanged = function (event) {
        // User was selected
    };
    PhoneInputPage.prototype.onClose = function () {
        // let toast = this.toastCtrl.create({
        //   message: 'Thanks for your selection',
        //   duration: 2000
        // });
        // toast.present();
    };
    PhoneInputPage.prototype.phoneValidator = function (str) {
        var regExp = /^([0]{1})([0-9]{2})([0-9]{3})([0-9]{4})$/;
        //let regExp = /^[0-9]{10}$/;
        return regExp.test(str);
    };
    PhoneInputPage.prototype.openFromCode = function () {
        this.selectComponent.open();
    };
    PhoneInputPage.prototype.ionViewDidLoad = function () {
    };
    PhoneInputPage.prototype.nextState = function () {
        var _this = this;
        if (this.ipPhoneNumber != "") {
            var isNumb = this.phoneValidator(this.ipPhoneNumber);
            if (isNumb) {
                this.pAccount.UserCheck(this.ipPhoneNumber).then(function (r) {
                    if (r.message == "Is Member") {
                        localStorage.phoneNumber = r.data.phone;
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__password_input_password_input__["a" /* PasswordInputPage */], { phoneNumber: r.data.phone });
                    }
                    else {
                        //console.log("CODE : ",r.data.code);
                        //alert("OTP CODE : " + r.data.code);
                        localStorage.phoneNumber = _this.ipPhoneNumber;
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__validate_otp_validate_otp__["a" /* ValidateOtpPage */], r.data);
                    }
                });
            }
            else {
                this.phoneValidate = true;
                //this.openPopAlertMessage("Check Your Phone Number");
            }
        }
        else {
            this.phoneValidate = true;
            //this.openPopAlertMessage("Check Your Phone Number");
        }
    };
    PhoneInputPage.prototype.openPopAlertMessage = function (msg) {
        var set_Popup = {
            title: "Pop-up Request",
            text_message: msg,
            btn_cancel: "",
            btn_confirm: "Done",
            isCancel: false
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            // console.log(popup);    
        });
        popModal.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('myselect'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5_ionic_select_searchable__["SelectSearchableComponent"])
    ], PhoneInputPage.prototype, "selectComponent", void 0);
    PhoneInputPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-phone-input',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/phone-input/phone-input.html"*/'<!--\n  Generated template for the PhoneInputPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header >\n\n  <ion-navbar>\n    <ion-title></ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n \n  <div class="panel-top">\n      <h4>Enter your phone number</h4>  \n\n      <!-- <ion-item>\n          <select-searchable #myselect\n            item-content \n            [(ngModel)]="ipCountryCode"\n            itemValueField="code"\n            itemTextField="dial_code"\n            [items]="countries"\n            [canSearch]="true"  \n            (onChange)="sChanged($event)"\n            (onClose)="onClose($event)"> \n          </select-searchable>  \n          <ion-input type="tel" [(ngModel)]="ipPhoneNumber"></ion-input>\n      </ion-item> -->\n\n      <div class="panel-content">  \n          <ion-grid>\n              <ion-row>\n                  <ion-col col-2>\n                      <img item-left src="assets/flags/th.png" style="height: 32px;" />\n                  </ion-col>\n                <ion-col col-3>     \n                  <ion-select [(ngModel)]="ipCountryCode"> \n                        <ion-option value="TH">+66\n                        </ion-option>  \n                  </ion-select> \n\n                  <!-- <ion-item no-padding>\n                    <img src="assets/images/flag-TH.png" class="flag-icon" />\n                    <ion-label style="height: 0px; ;color: #fcb813">Dial-code</ion-label>\n                    <ion-select clase="ion-select-b"  [(ngModel)]="ipCountryCode">\n                         <ion-option *ngFor="let cc of countries" [value] ="cc.code">{{cc.dial_code}}\n                         </ion-option>  \n                      </ion-select> \n                  </ion-item>  -->\n\n                <!-- <select-searchable #myselect  full\n                      item-content \n                      [(ngModel)]="ipCountryCode"\n                      itemValueField="code"\n                      itemTextField="dial_code"\n                      [items]="countries"\n                      [canSearch]="true"  \n                      (onChange)="sChanged($event)"\n                      (onClose)="onClose($event)"> \n                </select-searchable>  -->\n\n                </ion-col>\n                <ion-col col-7>\n                  <input type="tel" [(ngModel)]="ipPhoneNumber"  maxlength="10" (keyup.enter)="nextState()"/> \n                </ion-col>\n              </ion-row> \n              <ion-row>\n                  <ion-col col-2>\n                  </ion-col>\n                <ion-col col-10>\n                    <p style="color: red;font-size: 0.8em;" *ngIf="phoneValidate">* {{message_phone_validate}}</p>\n                  </ion-col>\n              </ion-row>\n            </ion-grid>\n      </div> \n  </div>\n\n\n  <div class="panel-action">\n      <ion-icon name="ios-arrow-round-forward-outline" color="light" \n      (click)="nextState()"></ion-icon>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/phone-input/phone-input.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_3__providers_account_account__["a" /* AccountProvider */]])
    ], PhoneInputPage);
    return PhoneInputPage;
}());

//# sourceMappingURL=phone-input.js.map

/***/ }),

/***/ 227:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ValidateOtpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__validate_password_validate_password__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__create_newuser_create_newuser__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_account_account__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ValidateOtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ValidateOtpPage = /** @class */ (function () {
    function ValidateOtpPage(navCtrl, navParams, actionSheetCtrl, pAccount) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.actionSheetCtrl = actionSheetCtrl;
        this.pAccount = pAccount;
        this.phoneNumber = "";
        this.otpForValidate = "";
        this.maxtime = 60;
        this.ipOTP = {
            ip1: "",
            ip2: "",
            ip3: "",
            ip4: "",
        };
        this.stateValidate = false;
        this.timmerTimeup = false;
        this.phoneNumber = this.navParams.get("phone");
        this.otpForValidate = this.navParams.get("code");
    }
    ValidateOtpPage.prototype.ionViewDidLoad = function () {
        this.startTimer();
    };
    ValidateOtpPage.prototype.popPage = function () {
        this.navCtrl.pop();
    };
    ValidateOtpPage.prototype.clearAll = function () {
        this.ipOTP.ip1 = "";
        this.ipOTP.ip2 = "";
        this.ipOTP.ip3 = "";
        this.ipOTP.ip4 = "";
        this.stateValidate = false;
    };
    ValidateOtpPage.prototype.clearNum = function (i) {
        if (i == "2") {
            this.ipOTP.ip2 = "";
        }
        else if (i == "3") {
            this.ipOTP.ip3 = "";
        }
        else if (i == "4") {
            this.ipOTP.ip4 = "";
        }
    };
    ValidateOtpPage.prototype.keyupOtp = function (ev, i) {
        if (i == "1") {
            if (this.ipOTP.ip1 == "") {
                //console.log(ev.keyCode);
            }
            else {
                //console.log(ev.target.value);
                // this.ipOTP.ip1 = ev.target.value;
                this.otp2.setFocus();
            }
        }
        else if (i == "2") {
            if (this.ipOTP.ip2 == "") {
                if (ev.keyCode == 8) {
                    this.otp1.setFocus();
                }
            }
            else {
                this.otp3.setFocus();
            }
        }
        else if (i == "3") {
            if (this.ipOTP.ip3 == "") {
                if (ev.keyCode == 8) {
                    this.otp2.setFocus();
                }
            }
            else {
                this.otp4.setFocus();
            }
        }
        else if (i == "4") {
            //console.log(this.ipOTP.ip4);
            //console.log(ev.target.value);
            if (this.ipOTP.ip4.length > 1) {
                this.ipOTP.ip4 = ev.target.value.slice(1);
            }
            else {
                if (this.ipOTP.ip4 == "") {
                    if (ev.keyCode == 8) {
                        this.otp3.setFocus();
                    }
                }
                else {
                    this.nextState();
                }
            }
        }
    };
    ValidateOtpPage.prototype.nextState = function () {
        var _this = this;
        if (this.ipOTP.ip1 != "" && this.ipOTP.ip2 != ""
            && this.ipOTP.ip3 != "" && this.ipOTP.ip4 != "") {
            var ipLastOtp = this.ipOTP.ip1 + this.ipOTP.ip2 + this.ipOTP.ip3 + this.ipOTP.ip4;
            if (ipLastOtp == this.otpForValidate) {
                // this.navCtrl.push(CreateNewuserPage,{phoneNumber : this.phoneNumber,userid:15});
                this.pAccount.UserVerify(this.phoneNumber, ipLastOtp).then(function (r) {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__create_newuser_create_newuser__["a" /* CreateNewuserPage */], { phoneNumber: _this.phoneNumber, userid: r.data.id });
                });
            }
            else {
                this.stateValidate = true;
            }
        }
        else {
            this.stateValidate = true;
        }
    };
    ValidateOtpPage.prototype.startTimer = function () {
        var _this = this;
        setTimeout(function (x) {
            _this.maxtime -= 1;
            if (_this.maxtime > 0) {
                _this.startTimer();
            }
            if (_this.maxtime <= 0) {
                _this.timmerTimeup = true;
            }
        }, 1000);
    };
    ValidateOtpPage.prototype.openModal = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Resend to ' + this.phoneNumber,
            cssClass: 'resend-css',
            buttons: [
                {
                    text: 'SEND CODE VIA SMS',
                    role: 'destructive',
                    handler: function () {
                        _this.pAccount.UserCheck(_this.phoneNumber).then(function (r) {
                            _this.phoneNumber = r.data.phone;
                            _this.otpForValidate = r.data.code;
                            // console.log("Code : ", this.otpForValidate);
                            _this.maxtime = 5;
                            _this.timmerTimeup = false;
                            _this.startTimer();
                        });
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ValidateOtpPage.prototype.OnClick_ConfirmOTP = function () {
        //console.log(this.inputOtp);
    };
    ValidateOtpPage.prototype.OnClick_ExistsUser = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__validate_password_validate_password__["a" /* ValidatePasswordPage */]);
    };
    ValidateOtpPage.prototype.OnClick_NewMember = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__create_newuser_create_newuser__["a" /* CreateNewuserPage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('otp1'),
        __metadata("design:type", Object)
    ], ValidateOtpPage.prototype, "otp1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('otp2'),
        __metadata("design:type", Object)
    ], ValidateOtpPage.prototype, "otp2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('otp3'),
        __metadata("design:type", Object)
    ], ValidateOtpPage.prototype, "otp3", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('otp4'),
        __metadata("design:type", Object)
    ], ValidateOtpPage.prototype, "otp4", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('ffocus'),
        __metadata("design:type", Object)
    ], ValidateOtpPage.prototype, "ffocus", void 0);
    ValidateOtpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-validate-otp',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/validate-otp/validate-otp.html"*/'<!--\n  Generated template for the ValidateOtpPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title></ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n  <div class="panel-top">\n    <h4>Enter 4 ditgit code sent to you at your phone number </h4>\n    <div class="otp-panel">\n      <ion-input maxlength="1" type="number" name="otp1" #otp1 (keyup)="keyupOtp($event,\'1\')" (focus)="clearAll()" [(ngModel)]="ipOTP.ip1"></ion-input>\n      <ion-input maxlength="1" type="number" name="otp2" #otp2 (keyup)="keyupOtp($event,\'2\')" (focus)="clearNum(\'2\')" [(ngModel)]="ipOTP.ip2"></ion-input>\n      <ion-input maxlength="1" type="number" name="otp3" #otp3 (keyup)="keyupOtp($event,\'3\')" (focus)="clearNum(\'3\')" [(ngModel)]="ipOTP.ip3"></ion-input>\n      <ion-input maxlength="1" type="number" name="otp4" #otp4 (keyup)="keyupOtp($event,\'4\')" (focus)="clearNum(\'4\')" [(ngModel)]="ipOTP.ip4"></ion-input>\n      <h6 *ngIf="stateValidate">Did you enter the correct phone number ?</h6>\n    </div>\n\n  </div>\n  <div class="panel-detail">\n    <h6>Resend code\n      <span *ngIf="!timmerTimeup">in {{maxtime}}s</span>\n      <span *ngIf="timmerTimeup">\n        <button ion-button (click)="openModal()">Resend</button>\n      </span>\n    </h6>\n    <h6 (click)="popPage()" class="cBlue">Edit my phone number</h6>\n  </div>\n\n  <div class="panel-action">\n      <ion-icon name="ios-arrow-round-forward-outline" color="light" \n      (click)="nextState()"></ion-icon>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/validate-otp/validate-otp.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"], __WEBPACK_IMPORTED_MODULE_4__providers_account_account__["a" /* AccountProvider */]])
    ], ValidateOtpPage);
    return ValidateOtpPage;
}());

//# sourceMappingURL=validate-otp.js.map

/***/ }),

/***/ 228:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TermConditionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__term_term__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__privacy_privacy__ = __webpack_require__(116);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TermConditionPage = /** @class */ (function () {
    function TermConditionPage(navCtrl, events, navParams) {
        this.navCtrl = navCtrl;
        this.events = events;
        this.navParams = navParams;
        this.phoneNumber = "";
        this.userid = "";
        this.phoneNumber = this.navParams.get("phoneNumber");
        this.userid = this.navParams.get("userid");
    }
    TermConditionPage.prototype.gotoHomePage = function () {
        localStorage.userId = this.userid;
        localStorage.phoneNumber = this.phoneNumber;
        localStorage.memberType = "0";
        this.events.publish('username:changed', localStorage.userName);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    TermConditionPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad TermConditionPage');
    };
    TermConditionPage.prototype.viewTerm = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__term_term__["a" /* TermPage */]);
    };
    TermConditionPage.prototype.viewPrivacy = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__privacy_privacy__["a" /* PrivacyPage */]);
    };
    TermConditionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-term-condition',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/term-condition/term-condition.html"*/'<!--\n  Generated template for the TermConditionPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title></ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n  <div class="panel-top">\n    <h6 class="term-con">Tab arrow below to agree with Limo’s term of use and acknowledge that you have read Privacy Policy </h6>\n  </div>\n\n  <div class="panel-detail">\n      <h6>To learn more about <span class="cBlue" (click)="viewTerm()"> Term of Use </span><br/> and <span class="cBlue" (click)="viewPrivacy()">Privacy policy</span></h6> \n    </div>\n \n\n  <div class="panel-action">\n      <ion-icon name="ios-arrow-round-forward-outline" color="light" \n      (click)="gotoHomePage()"></ion-icon>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/term-condition/term-condition.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], TermConditionPage);
    return TermConditionPage;
}());

//# sourceMappingURL=term-condition.js.map

/***/ }),

/***/ 229:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordInputPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_account_account__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modal_popup_modal_popup__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__my_location_my_location__ = __webpack_require__(117);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { HomePage } from '../home/home';

// import { HomeDriverPage } from '../home-driver/home-driver';

var PasswordInputPage = /** @class */ (function () {
    function PasswordInputPage(navCtrl, alertCtrl, events, navParams, modalCtrl, pAccount) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.pAccount = pAccount;
        this.ipPassword = "";
        this.phoneNumber = "";
        this.phoneNumber = navParams.get('phoneNumber');
    }
    PasswordInputPage.prototype.ionViewDidLoad = function () {
    };
    PasswordInputPage.prototype.sendResetPassword = function () {
        var _this = this;
        var set_Popup = {
            title: "Alert Confirm",
            text_message: "คุณต้องการ Reset Password ? ",
            btn_cancel: "Cancel",
            btn_confirm: "OK",
            isCancel: true
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            // console.log(popup);  
            if (popup) {
                _this.sendReset();
            }
        });
        popModal.present();
    };
    PasswordInputPage.prototype.sendReset = function () {
        var _this = this;
        this.pAccount.UserResetPassword(this.phoneNumber).then(function (r) {
            if (r.status == 200) {
                _this.openPopAlertSendResetPassword(r.message);
            }
            else if (r.status == 500) {
                _this.openPopAlertSendResetPassword(r.message);
            }
            else {
                _this.openPopAlertSendResetPassword("เกิดข้อผิดพลาด กรุณาทำรายการใหม่ภายหลัง");
            }
        });
    };
    PasswordInputPage.prototype.openPopAlertSendResetPassword = function (msg) {
        var set_Popup = {
            title: "Pop-up Request",
            text_message: msg,
            btn_cancel: "",
            btn_confirm: "Done",
            isCancel: false
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            // console.log(popup);    
        });
        popModal.present();
    };
    PasswordInputPage.prototype.nextState = function () {
        var _this = this;
        if (this.ipPassword != "") {
            this.pAccount.UserLogin(this.phoneNumber, this.ipPassword).then(function (r) {
                if (r.status == 200 && r.message == "Success") {
                    //console.log(r.data); 
                    localStorage.userId = r.data.id;
                    localStorage.phoneNumber = r.data.phone;
                    localStorage.Password = _this.ipPassword;
                    localStorage.memberType = r.data.type;
                    localStorage.userName = r.data.name;
                    localStorage.userImage = r.data.image;
                    _this.events.publish('username:changed', r.data.name);
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__my_location_my_location__["a" /* MyLocationPage */]);
                    // if(r.data.type == 0)
                    // {
                    //   this.navCtrl.setRoot(HomePage); 
                    // }
                    // else if(r.data.type == 1)
                    // {
                    //   this.navCtrl.setRoot(HomePage); 
                    // }
                    // else if(r.data.type == 2)
                    // {
                    //   this.navCtrl.setRoot(HomeDriverPage); 
                    // } 
                }
                else {
                    if (r.status == 500 && r.message == "User is logged") {
                        localStorage.userId = r.data.id;
                        _this.logout();
                    }
                    else {
                        _this.openPopAlertMessage(r.message, "0");
                    }
                }
            });
        }
    };
    PasswordInputPage.prototype.openPopAlertMessage = function (msg, set) {
        var _this = this;
        var set_Popup = {
            title: "Pop-up Request",
            text_message: msg,
            btn_cancel: "",
            btn_confirm: "Done",
            isCancel: false
        };
        if (set == "1") {
            set_Popup.title = "User is logged";
            set_Popup.btn_confirm = "Re-login";
        }
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            // console.log(popup);   
            if (set == "1") {
                _this.relogin();
            }
        });
        popModal.present();
    };
    PasswordInputPage.prototype.logout = function () {
        var _this = this;
        this.pAccount.UserLogOut().then(function (r) {
            localStorage.clear();
            //this.openPopAlertMessage("You have logged in at another device, please re-login again","1");  
            _this.relogin();
        });
    };
    PasswordInputPage.prototype.relogin = function () {
        this.nextState();
    };
    PasswordInputPage.prototype.AlertContrl = function (msg) {
        var alert = this.alertCtrl.create({
            title: 'Alert Message',
            message: msg,
            buttons: [
                {
                    text: 'Done',
                    role: 'cancel',
                    handler: function () {
                    }
                }
            ]
        });
        alert.present();
    };
    PasswordInputPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-password-input',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/password-input/password-input.html"*/'<!--\n  Generated template for the PasswordInputPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title></ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content >\n\n    <div class="panel-top">\n        <h4>Enter your password</h4> \n        <div class="panel-content">\n            <br/>\n            <input type="password" [(ngModel)]="ipPassword" (keyup.enter)="nextState()"/>\n            <br/>\n            <br/>\n            <br/>\n            <br/>\n            <br/>\n            <br/>\n            <br/>\n            <h5 style="color: #fff" (click)="sendResetPassword()">RESET PASSWORD</h5> \n        </div> \n    </div>\n   \n\n    <div class="panel-action">\n            <ion-icon name="ios-arrow-round-forward-outline" color="light" \n            (click)="nextState()"></ion-icon>\n        </div>\n</ion-content>\n'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/password-input/password-input.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_account_account__["a" /* AccountProvider */]])
    ], PasswordInputPage);
    return PasswordInputPage;
}());

//# sourceMappingURL=password-input.js.map

/***/ }),

/***/ 230:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeDriverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_driver_driver__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_audio__ = __webpack_require__(231);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__modal_popup_modal_popup__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_call_number__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_account_account__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_page_api_page_api__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_order_order__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var HomeDriverPage = /** @class */ (function () {
    function HomeDriverPage(modalCtrl, pDriver, pOrder, pAccount, pget, loadingCtrl, nativeAudio, callNumber) {
        this.modalCtrl = modalCtrl;
        this.pDriver = pDriver;
        this.pOrder = pOrder;
        this.pAccount = pAccount;
        this.pget = pget;
        this.loadingCtrl = loadingCtrl;
        this.nativeAudio = nativeAudio;
        this.callNumber = callNumber;
        this.btnState = "Go Everywhere?";
        this.gmarkers = [];
        this.directionsService = new google.maps.DirectionsService;
        this.directionsDisplay = new google.maps.DirectionsRenderer;
        this.MemberType = '2';
        this.alert_driver = "";
        this.pActive = false;
        this.mActive = false;
        this.flag_show = false;
        this.selectItem = {
            id: "",
            user_id: "",
            user_name: "",
            phone: "",
            start_location: "",
            start_location_lat: "",
            start_location_long: "",
            destination: "",
            destination_lat: "",
            destination_long: "",
            fare: "",
            note: ""
        };
        this.runJob = false;
        this.showBox = false;
        this.driverName = " Driver 02";
        this.ContactName = "";
        this.ts = 90000;
        this.tds = 10000;
        this.view_Book = false;
        this.btnPickup = false;
        this.btnSuccess = true;
        this.countOmise = 0;
        this.list_user_find = [];
    }
    HomeDriverPage.prototype.ionViewDidLoad = function () {
        this.getSlug();
        this.MemberType = localStorage.memberType;
        this.driverName = localStorage.userName;
        this.gmap = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["a" /* GoogleMaps */].create('map_canvas3', {});
        this.initMap();
        this.addCurrentLoc();
        this.startTiming(this.ts);
        if (!this.pActive) {
            this.startTimingDriveFind();
        }
        this.nativeAudio.preloadSimple('1', 'assets/sounds/notify.mp3').then(function () {
            console.log('Playing');
        });
    };
    HomeDriverPage.prototype.ionViewWillLeave = function () {
        clearInterval(this.timerInterval);
        clearInterval(this.timerIntervalD);
        if (!this.map) {
            this.map.remove();
        }
        console.log('leave view');
    };
    HomeDriverPage.prototype.removeMarkers = function () {
        for (var i = 0; i < this.gmarkers.length; i++) {
            this.gmarkers[i].setMap(null);
        }
        this.gmarkers = [];
    };
    HomeDriverPage.prototype.removeMarkersD = function () {
        for (var i = 0; i < this.gmarkers.length; i++) {
            console.log(this.gmarkers[i].title);
            if (this.gmarkers[i].title == "Driver") {
                this.gmarkers[i].setMap(null);
            }
        }
    };
    HomeDriverPage.prototype.addCurrentLoc = function () {
        this.removeMarkers();
        this.addMarker(localStorage.currentLat, localStorage.currentLng, localStorage.userName, localStorage.currentPlaceB);
        // this.resetMapBounds();
    };
    HomeDriverPage.prototype.play = function () {
        this.nativeAudio.play('1').then(function () {
            console.log('Playing');
        });
    };
    HomeDriverPage.prototype.GetCurrentLoc = function () {
        var _this = this;
        this.removeMarkersD();
        this.gmap.getMyLocation()
            .then(function (location) {
            // console.log(this.gmarkers);  
            console.log(JSON.stringify(location, null, 2));
            localStorage.currentLat = location.latLng.lat;
            localStorage.currentLng = location.latLng.lng;
            var clocation = {
                lat: location.latLng.lat,
                lng: location.latLng.lng
            };
            _this.pAccount.UserLoginApp().then(function (r) {
                // console.log(r);  
            });
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({ 'location': clocation }, function (results, status) {
                //console.log(results); 
                localStorage.currentPlaceA = results[0].address_components[0].short_name + ' ' + results[0].address_components[1].short_name;
                localStorage.currentPlaceB = results[0].formatted_address;
                _this.addMarker(localStorage.currentLat, localStorage.currentLng, localStorage.userName, localStorage.currentPlaceB);
                _this.resetMapBounds();
            });
        }).catch(function (error) {
            //alert('Error getting location');
            console.log(error);
        });
    };
    HomeDriverPage.prototype.startTimingDriveFind = function () {
        var _this = this;
        this.pActive = true;
        this.timerIntervalD = setInterval(function () {
            if (!_this.runJob) {
                _this.DriveFind();
            }
            _this.pActive = false;
        }, this.tds);
    };
    HomeDriverPage.prototype.startTiming = function (ts) {
        var _this = this;
        clearInterval(this.timerInterval);
        this.timerInterval = setInterval(function () {
            _this.GetCurrentLoc();
        }, ts);
    };
    HomeDriverPage.prototype.chooseItem = function (item) {
        for (var i = 0; i < this.list_user_find.length; i++) {
            this.list_user_find[i].color = "cGray";
        }
        item.color = "primary";
        this.selectItem.id = item.id;
        this.selectItem.user_id = item.user_id;
        this.selectItem.user_name = item.user_name;
        this.selectItem.phone = item.phone;
        this.selectItem.start_location = item.start_location;
        this.selectItem.start_location_lat = item.start_location_lat;
        this.selectItem.start_location_long = item.start_location_long;
        this.selectItem.destination = item.destination;
        this.selectItem.destination_lat = item.destination_lat;
        this.selectItem.destination_long = item.destination_long;
        this.selectItem.fare = item.fare;
        this.selectItem.note = item.note;
        this.setPickUp();
    };
    HomeDriverPage.prototype.setPickUp = function () {
        this.removeMarkers();
        this.addMarker(this.selectItem.start_location_lat, this.selectItem.start_location_long, "Pick-up", this.selectItem.start_location);
        this.addMarker(this.selectItem.destination_lat, this.selectItem.destination_long, "Drop off", this.selectItem.destination);
        this.addMarker(localStorage.currentLat, localStorage.currentLng, localStorage.userName, localStorage.currentPlaceB);
        this.resetMapBounds();
    };
    HomeDriverPage.prototype.resetMapBounds = function () {
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0; i < this.gmarkers.length; i++) {
            bounds.extend(this.gmarkers[i].position);
        }
        this.map.fitBounds(bounds);
        if (this.map.getZoom() > 17) {
            this.map.setZoom(17);
        }
    };
    HomeDriverPage.prototype.viewBook = function () {
        this.view_Book = !this.view_Book;
    };
    HomeDriverPage.prototype.DriveFind = function () {
        var _this = this;
        this.list_user_find = [];
        this.showBox = false;
        this.mActive = false;
        this.pDriver.DriverFind().then(function (r) {
            if (r.status == 200) {
                //this.user_find = r.data; 
                //console.log(r.data);   
                if (r.data.length > 0) {
                    _this.play();
                    _this.showBox = true;
                    _this.mActive = true;
                    for (var i = 0; i < r.data.length; i++) {
                        var ccolor = "cGray";
                        if (_this.selectItem.id != "") {
                            if (_this.selectItem.id == r.data[i].id) {
                                ccolor = "primary";
                            }
                        }
                        var item = {
                            id: r.data[i].id,
                            user_id: r.data[i].user_id,
                            user_name: r.data[i].name,
                            phone: r.data[i].phone,
                            start_location: r.data[i].start_location,
                            start_location_lat: r.data[i].start_location_lat,
                            start_location_long: r.data[i].start_location_long,
                            destination: r.data[i].destination,
                            destination_lat: r.data[i].destination_lat,
                            destination_long: r.data[i].destination_long,
                            km: '10KM',
                            fare: r.data[i].fare,
                            note: r.data[i].note,
                            color: ccolor,
                            expressway: r.data[i].expressway
                        };
                        _this.list_user_find.push(item);
                        if (_this.selectItem.id != "") {
                            if (_this.selectItem.id == r.data[i].id) {
                                /*
                
                                this.removeMarkers();
                
                                this.addMarker(item.start_location_lat,
                                  item.start_location_long,
                                  "Pick-up",
                                  item.start_location);
                            
                                
                                this.addMarker(item.destination_lat,
                                  item.destination_long,
                                    "Drop off",
                                    item.destination);
                            
                                this.addMarker(localStorage.currentLat,
                                  localStorage.currentLng,
                                  localStorage.userName, localStorage.currentPlaceB);
                 
                                this.resetMapBounds();
                
                               */
                            }
                        }
                    }
                }
            }
        });
    };
    HomeDriverPage.prototype.addMarker = function (lat, lng, setT, st_location) {
        var infowindow = new google.maps.InfoWindow();
        var marker;
        var setText = "";
        var img_url = "";
        var title = "";
        if (setT == "Pick-up") {
            setText = "Pick-up";
            title = "Pick-up";
            img_url = "assets/images/pickup.png";
        }
        else if (setT == "Drop off") {
            setText = "Drop off";
            title = "Drop off";
            img_url = "assets/images/drop_off.png";
        }
        else {
            setText = setT;
            title = "Driver";
            img_url = "assets/images/taxi.png";
        }
        var latLng = new google.maps.LatLng(lat, lng);
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            map: this.map,
            disableDefaultUI: false,
            title: title,
            //draggable:true,
            //animation: google.maps.Animation.DROP,
            icon: {
                url: img_url,
                size: {
                    width: 48,
                    height: 48
                }
            }
        });
        // https://maps.google.com/maps?daddr=<lat>,<long>&amp;ll=
        // / infowindow.setContent("<p>"+ setText +"</p><p>" + st_location +"</p>" + "<a href='geo:" + lat + "," + lng + "?z=11'>Open Google maps</a>");
        // ios   maps://?q=LAT,LNG
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent("<div style='padding:8px;'><p>" + setText + "</p><p>" + st_location + "</p>" + "<a href='http://maps.google.com/maps?q=" + lat + "," + lng + "'>Open Google maps</a></div>");
                // infowindow.setContent("<div style='padding:8px;'><p>"+ setText +"</p><p>" + st_location +"</p>" + "<a href='maps:" + lat + "," + lng + "?q=" + lat + "," + lng + "'>Open Google maps</a></div>");
                infowindow.open(this.map, marker);
            };
        })(marker, setT));
        this.gmarkers.push(marker);
        //this.map.setZoom(11);
        //this.map.setCenter(latLng); 
        // show map, open infoBox 
        // google.maps.event.addListenerOnce(this.map, 'tilesloaded', function() {
        //   infowindow.setContent("<p>"+ setText +"</p><p>" + st_location +"</p>" + "");
        //   infowindow.open(this.map, marker);
        // });
        // let clocation = {
        //   lat: lat,
        //   lng: lng
        // };
        // let geocoder = new google.maps.Geocoder;
        // geocoder.geocode({ 'location': clocation }, (results, status) => { 
        //     let contentText =  results[1].formatted_address;
        // });
    };
    HomeDriverPage.prototype.GoDetail = function () {
    };
    HomeDriverPage.prototype.getSlug = function () {
        var _this = this;
        this.pget.PageApiGet("alert_driver").then(function (r) {
            if (r.status == 200 && r.message == "Success") {
                //console.log(r.data.content);
                _this.alert_driver = r.data.content;
            }
        });
    };
    HomeDriverPage.prototype.mailto = function (email) {
        window.open("mailto:" + email, '_system');
    };
    HomeDriverPage.prototype.telToII = function (numb) {
        window.open("tel:" + numb, '_system');
    };
    HomeDriverPage.prototype.telTo = function (numb) {
        this.callNumber.callNumber(numb, true);
    };
    HomeDriverPage.prototype.smsTo = function (numb) {
        window.open("sms:" + numb + '?body=Wellcome from Limo!', '_system');
    };
    HomeDriverPage.prototype.calling = function () {
        //console.log(this.number); 
        this.telTo(this.selectItem.phone);
    };
    HomeDriverPage.prototype.popMessage = function () {
        //console.log(this.number);  
        var set_Popup = {
            title: "Pop-up Message",
            text_message: this.selectItem.note,
            btn_cancel: "",
            btn_confirm: "Done",
            isCancel: false
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            // console.log(popup);   
        });
        popModal.present();
    };
    HomeDriverPage.prototype.clickPickup = function () {
        var _this = this;
        var set_Popup = {
            title: "Confirm Pickup",
            text_message: "รับผู้โดยสาร...",
            btn_cancel: "Cancel",
            btn_confirm: "Confirm",
            isCancel: true
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            // console.log(popup);  
            if (popup) {
                _this.btnPickup = false;
                _this.btnSuccess = true;
                _this.SendPickup();
            }
        });
        popModal.present();
    };
    HomeDriverPage.prototype.SendPickup = function () {
        var dId = localStorage.userId;
        if (this.selectItem.id != "") {
            this.pDriver.DriverPickup(dId, this.selectItem.id).then(function (r) {
                if (r.status == 200) {
                }
            });
        }
    };
    HomeDriverPage.prototype.NextSuccess = function () {
        var _this = this;
        var dId = localStorage.userId;
        if (this.selectItem.id != "") {
            this.pDriver.DriverSuccess(dId, this.selectItem.id).then(function (r) {
                if (r.status == 200) {
                    _this.ts = 90000;
                    _this.startTiming(_this.ts);
                    _this.runJob = false;
                    _this.clean();
                    _this.addCurrentLoc();
                }
            });
        }
    };
    HomeDriverPage.prototype.clickSuccess = function () {
        var _this = this;
        var set_Popup = {
            title: "Confirm SUCCESS",
            text_message: "Do you want to Next SUCCESS? ",
            btn_cancel: "Cancel",
            btn_confirm: "Confirm",
            isCancel: true
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            // console.log(popup);  
            if (popup) {
                _this.NextSuccess();
            }
        });
        popModal.present();
    };
    HomeDriverPage.prototype.initMap = function () {
        if (localStorage.currentLat != undefined) {
            // 
        }
        else {
            localStorage.currentLat = "13.752782";
            localStorage.currentLng = "100.522993";
        }
        var latLng = new google.maps.LatLng(localStorage.currentLat, localStorage.currentLng);
        this.map = new google.maps.Map(this.mapElement.nativeElement, {
            zoom: 17,
            mapTypeControl: false,
            disableDefaultUI: true,
            center: latLng
        });
        this.directionsDisplay.setMap(this.map);
    };
    HomeDriverPage.prototype.clean = function () {
        //this.directionsDisplay.setDirections({routes: []});
        this.directionsDisplay.setMap(null);
        this.initMap();
    };
    HomeDriverPage.prototype.confirmJob = function () {
        var _this = this;
        var set_Popup = {
            title: "Alert Confirm",
            text_message: this.alert_driver + "",
            btn_cancel: "No",
            btn_confirm: "Yes",
            isCancel: true
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            if (popup) {
                _this.NextConfirm();
            }
        });
        popModal.present();
    };
    HomeDriverPage.prototype.NextConfirm = function () {
        var _this = this;
        var dId = localStorage.userId;
        if (this.selectItem.id != "") {
            this.pDriver.DriverConfirm(dId, this.selectItem.id).then(function (r) {
                if (r.status == 200) {
                    // Popup message  follow order waitting payment credit 
                    _this.presentLoadingCustom(r.message);
                    //this.presentLoadingCustom("กรุณารอลูกค้าทำการชำระเงิน"); 
                }
                else if (r.status == 201) {
                    // Popup message  follow order payment Cash  
                    _this.popMessageOnJobb(r.message);
                }
                else if (r.status == 500) {
                    // post-- order/cancel  
                    //this.popMessageCancelJobb(r.message);
                    _this.popMessageCancelJobb("ลูกค้าชำระเงินไม่สำเร็จ");
                }
            });
        }
    };
    HomeDriverPage.prototype.presentLoadingCustom = function (str) {
        var _this = this;
        //circles,dots
        var loading = this.loadingCtrl.create({
            spinner: 'circles',
            content: str,
        });
        loading.onDidDismiss(function () {
            console.log('Dismissed loading');
            clearInterval(_this.timerIntervalS);
        });
        loading.present();
        clearInterval(this.timerIntervalS);
        this.timerIntervalS = setInterval(function () {
            //loading.dismiss();
            _this.pOrder.OrderGetIds(_this.selectItem.user_id, _this.selectItem.id).then(function (r) {
                if (r.status == 200) {
                    var dt_order = r.data[0];
                    //let dt_user = r.data['user'];
                    //let dt_driver = r.data['driver'];
                    var dt_payment = r.data['payment'];
                    if (dt_order.status == "3") {
                        if (dt_payment[0].card_number != "Cash") {
                            //0=waiting,1=success,2=error,3=pending	
                            if (dt_payment[0].status_payment == "0") {
                                //waiting
                            }
                            else if (dt_payment[0].status_payment == "1") {
                                //success
                                _this.countOmise = 0;
                                loading.dismiss();
                                _this.popMessageOnJobb("ลูกค้าชำระเงินสำเร็จแล้ว");
                            }
                            else if (dt_payment[0].status_payment == "2") {
                                //error
                                _this.countOmise = 0;
                                loading.dismiss();
                                _this.popMessageCancelJobb("ลูกค้าชำระเงินไม่สำเร็จ");
                            }
                            else if (dt_payment[0].status_payment == "3") {
                                _this.countOmise++;
                                if (_this.countOmise > 15) {
                                    _this.countOmise = 0;
                                    loading.dismiss();
                                    _this.popMessageCancelJobb("ลูกค้าชำระเงินไม่สำเร็จ");
                                }
                                //pending
                            }
                        }
                    }
                    else if (dt_order.status == "0") {
                        _this.countOmise = 0;
                        loading.dismiss();
                        _this.popMessageCancelJobb("ลูกค้าชำระเงินไม่สำเร็จ");
                    }
                }
            });
        }, 10000);
    };
    HomeDriverPage.prototype.popMessageCancelJobb = function (str) {
        var _this = this;
        var set_Popup = {
            title: "Pop-up Message",
            text_message: str,
            btn_cancel: "",
            btn_confirm: "Done",
            isCancel: false
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            // console.log(popup);  
            _this.CancelJobb();
        });
        popModal.present();
    };
    HomeDriverPage.prototype.CancelJobb = function () {
        var _this = this;
        this.pOrder.OrderCancel(this.selectItem.user_id, this.selectItem.id).then(function (r) {
            _this.clean();
            _this.addCurrentLoc();
        });
    };
    HomeDriverPage.prototype.popMessageOnJobb = function (str) {
        var _this = this;
        var set_Popup = {
            title: "Pop-up Message",
            text_message: str,
            btn_cancel: "",
            btn_confirm: "Next",
            isCancel: false
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            // console.log(popup);  
            _this.OnJobb();
        });
        popModal.present();
    };
    HomeDriverPage.prototype.OnJobb = function () {
        this.runJob = true;
        this.btnSuccess = false;
        this.btnPickup = true;
        this.showBox = false;
        this.ts = 20000;
        this.startTiming(this.ts);
        this.clean();
        // console.log( this.selectItem.user_name); 
        this.ContactName = this.selectItem.user_name;
        this.addMarker(this.selectItem.start_location_lat, this.selectItem.start_location_long, "Pick-up", this.selectItem.start_location);
        this.addMarker(this.selectItem.destination_lat, this.selectItem.destination_long, "Drop off", this.selectItem.destination);
        this.addMarker(localStorage.currentLat, localStorage.currentLng, localStorage.userName, localStorage.currentPlaceB);
        this.map.setZoom(15);
        //this.map.setCenter(clocation);
        this.map.setCenter({ lat: this.selectItem.start_location_lat, lng: this.selectItem.start_location_long });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], HomeDriverPage.prototype, "mapElement", void 0);
    HomeDriverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home-driver',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/home-driver/home-driver.html"*/'<ion-header no-border>\n  <ion-navbar transparent> \n      <button ion-button menuToggle>\n          <ion-icon name="menu" style="color: #666"></ion-icon>\n        </button> \n  </ion-navbar>\n</ion-header>\n\n<ion-content>  \n  <div #map id="map"></div> \n  <div id="map_canvas3" style="width: 100px; height: 100px;display: none;"></div> \n  \n  <!-- <div class="panel-find-car-a" *ngIf="!flag_show">\n      <ion-card *ngIf="!flag_show">\n          <ion-card-header>\n            Your Nearby Jobs List\n            <ion-icon item-end name="ios-arrow-dropdown-outline"></ion-icon>\n          </ion-card-header>\n      </ion-card>\n  </div> -->\n  <ion-fab middle right *ngIf="showBox" style="margin-top: -110px;">\n      <button ion-fab (click)="viewBook()"><ion-icon name="ios-book-outline"></ion-icon></button>\n      </ion-fab>\n  <div class="panel-find-car" *ngIf="showBox" [hidden]="view_Book"> \n      <ion-card>\n        <ion-card-header>\n          Your Nearby Jobs List\n        </ion-card-header>\n        <div class="line stripesLoader" style="background-position:100%;background-color : orange"></div>\n        <ion-card-content>\n            <ion-scroll scrollY="true">\n                <ion-list scroll="true"> \n                  <ion-item-sliding  *ngFor="let item of list_user_find" (click)="chooseItem(item)">\n                    <ion-item text-wrap>\n                      <ion-row> \n                        <ion-col col-10 text-wrap>\n                            <img  src="assets/images/icon-origin.png"  />  \n                            <span style="font-size: 0.9em;">{{item.start_location}}  </span>  \n                        </ion-col> \n                        <ion-col col-2>\n                            <ion-icon *ngIf="item.expressway==\'1\'" name="custom-tollways" color="bBlue" style="font-size: 26px;"></ion-icon>\n                        </ion-col>   \n                      </ion-row>\n                      <ion-row> \n                          <ion-col col-10 text-wrap>\n                              <img  src="assets/images/icon-destination.png"  />\n                              <span style="font-size: 0.9em;"> {{item.destination}}</span>   \n                          </ion-col> \n                          <ion-col col-2>\n                              <p style="font-size: 0.9em;text-align: center;">THB {{item.fare}}</p>\n                          </ion-col>\n                      </ion-row>\n                      <ion-icon item-right [color]="item.color" name="ios-checkmark-circle" (click)="chooseItem(item)"></ion-icon> \n                    </ion-item> \n                </ion-item-sliding> \n                </ion-list>\n            </ion-scroll>\n      \n        </ion-card-content> \n        <button class="btn-confirm" ion-button full (click)="confirmJob()">CONFIRM JOB</button>\n      </ion-card> \n    </div>\n    <div class="panel-confim-driver" *ngIf="runJob">\n        <ion-card>\n          <ion-card-header>\n            Contact Your Passenger\n            <p style="font-size: 0.8em">{{selectItem.user_name}}</p>\n          </ion-card-header>\n          <!-- <div class="line stripesLoader" style="background-position:100%;background-color: #666666"></div> -->\n          <!-- <ion-card-content>\n            <ion-item class="txt-label">\n              <img src="assets/images/icon-origin.png" /> <p style="font-size: 0.8em">{{selectItem.start_location}}</p>\n              <div class="trip-detail">\n                </div>\n            </ion-item>\n            <ion-item class="txt-label">\n              <img src="assets/images/icon-destination.png" /> <p style="font-size: 0.8em">{{selectItem.destination}}</p>\n              <div class="trip-detail">\n                <h6>THB {{selectItem.fare}}</h6>\n              </div>\n            </ion-item>\n          </ion-card-content> -->\n          <ion-grid text-center>\n              <ion-row no-padding>\n                <ion-col (click)="calling()"> \n                  <ion-icon name="ios-call-outline" style="font-size: 44px;"></ion-icon> \n                </ion-col> \n                <ion-col *ngIf="selectItem.note !=\'\'" (click)="popMessage()">\n                  <ion-icon name="ios-chatboxes-outline" style="font-size: 44px;"></ion-icon> \n                </ion-col>\n              </ion-row>\n            </ion-grid>\n            <button *ngIf="btnPickup" class="btn-success" ion-button full (click)="clickPickup()">รับผู้โดยสาร</button> -->\n            <button *ngIf="btnSuccess" class="btn-success" ion-button full (click)="clickSuccess()">ส่งผู้โดยสาร</button>  \n          </ion-card>\n      </div>\n</ion-content>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/home-driver/home-driver.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_driver_driver__["a" /* DriverProvider */],
            __WEBPACK_IMPORTED_MODULE_9__providers_order_order__["a" /* OrderProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_account_account__["a" /* AccountProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_page_api_page_api__["a" /* PageApiProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_audio__["a" /* NativeAudio */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_call_number__["a" /* CallNumber */]])
    ], HomeDriverPage);
    return HomeDriverPage;
}());

//# sourceMappingURL=home-driver.js.map

/***/ }),

/***/ 232:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return YourTripPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_order_order__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the YourTripPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var YourTripPage = /** @class */ (function () {
    /*
  0 Cancel
  1 Success
  2 Find Driver
  3 Driving
    */
    function YourTripPage(navCtrl, pOder, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.pOder = pOder;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.historyList = [];
        this.statusOrder = ['Cancel', 'Success', 'Find Driver', 'Driving'];
    }
    YourTripPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad YourTripPage');
        this.loadHistory();
    };
    YourTripPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    YourTripPage.prototype.loadHistory = function () {
        var _this = this;
        this.pOder.OrderHistory("0", "20").then(function (r) {
            if (r.status == 200 && r.message == "Success") {
                // console.log(r.data);
                _this.historyList = r.data;
            }
        });
    };
    YourTripPage.prototype.set_statusOrder = function (i) {
        var setT = "sp_color_b";
        if (i == '1') {
            setT = "sp_color_a";
        }
        return setT;
    };
    YourTripPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-your-trip',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/your-trip/your-trip.html"*/'\n<ion-header>\n    <ion-navbar>\n        <ion-buttons left>\n            <button ion-button (click)="dismiss()" color="light">\n                <ion-icon name="ios-arrow-round-back-outline"></ion-icon>\n            </button>\n        </ion-buttons>\n        <ion-title full text-center>History</ion-title>\n    </ion-navbar> \n  </ion-header>\n\n  <ion-content>  \n    <ion-scroll scrollY="true">\n        <ion-list scroll="true"> \n            <ion-item-sliding  *ngFor="let item of historyList">\n                <ion-item text-wrap>\n                  <ion-row style="background-color: #fcb813; margin-bottom: 4px;"> \n                          <ion-col>\n                              <span style="margin-left: 4px;">order number: {{item.number}}</span>\n                          </ion-col>\n                  </ion-row> \n                  <ion-row> \n                    <ion-col col-9>\n                        <img  src="assets/images/icon-origin.png"  />  \n                        <span style="font-size: 1em;text-overflow: ellipsis;white-space: nowrap;">{{item.start_location}}  </span>  \n                    </ion-col>                             \n                    <ion-col style="text-align: right;" col-3> \n                        <p style="font-size: 0.8em">THB {{item.fare}} <br> <span> {{item.showcard}}</span> </p>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row> \n                      <ion-col col-8>\n                          <img  src="assets/images/icon-destination.png"  />\n                          <span style="font-size: 1em;text-overflow: ellipsis;white-space: nowrap;"> {{item.destination}}</span>   \n                      </ion-col> \n                      <ion-col  style="text-align: right;" col-4>\n                          <p style="font-size: 0.8em">{{item.date_show}}</p>\n                      </ion-col>\n                  </ion-row>\n                  <ion-row> \n                      <ion-col col-6> \n                         <ion-icon *ngIf="item.expressway==\'1\'" name="custom-tollways" color="bBlue" style="margin-left: 6px; font-size: 28px;"></ion-icon>\n                      </ion-col> \n                      <ion-col  style="text-align: right;" col-6>\n                             <!-- <span class="sp_color_b">\n                                  {{statusOrder[item.status]}}\n                             </span>  -->\n                             <span class="{{set_statusOrder(item.status)}}">\n                              {{item.showstatus}}\n                         </span> \n                      </ion-col>\n                  </ion-row>\n                </ion-item> \n            </ion-item-sliding> \n            <ion-item></ion-item>\n        </ion-list>\n    </ion-scroll> \n</ion-content>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/your-trip/your-trip.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_order_order__["a" /* OrderProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], YourTripPage);
    return YourTripPage;
}());

//# sourceMappingURL=your-trip.js.map

/***/ }),

/***/ 233:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RefundPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_page_api_page_api__ = __webpack_require__(39);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the YourTripPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RefundPage = /** @class */ (function () {
    function RefundPage(navCtrl, navParams, pget, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pget = pget;
        this.viewCtrl = viewCtrl;
    }
    RefundPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        // legal, term, refund
        this.pget.PageApiGet("refund").then(function (r) {
            if (r.status == 200 && r.message == "Success") {
                _this.refund_html = r.data.content;
            }
        });
    };
    RefundPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    RefundPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-refund',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/refund/refund.html"*/'<!--\n  Generated template for the YourTripPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header> \n        <!-- <ion-navbar>\n                <ion-title>REFUND\n                </ion-title> \n        </ion-navbar>  -->\n    </ion-header>\n\n\n<ion-content>\n\n        <div class="panel-top">\n                <h2>REFUND</h2>\n             </div>\n             <div class="horizontalline"></div> \n\n    <!-- <ion-fab right top>\n        <ion-icon name="close" color="primary" style="font-size: 4em;margin-right: 20px;"\n        (click)="dismiss()"></ion-icon>\n    </ion-fab> -->\n\n     \n\n\n    <ion-scroll style="height:80%; " scrollY="true" >\n        <div class="panel-detail-b" [innerHTML]="refund_html"> \n        </div>\n    </ion-scroll>\n             \n  \n\n   <!-- <ion-fab right bottom>\n      <ion-icon name="ios-arrow-round-back-outline" color="cGray" style="font-size: 4em;margin-right: 20px;"\n      (click)="dismiss()"></ion-icon>\n  </ion-fab> -->\n\n</ion-content> \n\n  <ion-footer no-border>\n        <ion-toolbar color="light" >\n            <ion-buttons end>\n                <button ion-button (click)="dismiss()">\n                    <ion-icon name="ios-arrow-round-back-outline" color="cGray" style="font-size: 4em;margin-right: 20px;"></ion-icon>\n                </button>\n            </ion-buttons> \n        </ion-toolbar>\n      </ion-footer>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/refund/refund.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_page_api_page_api__["a" /* PageApiProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], RefundPage);
    return RefundPage;
}());

//# sourceMappingURL=refund.js.map

/***/ }),

/***/ 234:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_account_account__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__modal_popup_modal_popup__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__phone_update_phone_update__ = __webpack_require__(119);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ProfilePage = /** @class */ (function () {
    function ProfilePage(pAccount, actionSheetCtrl, transfer, camera, loadingCtrl, toastCtrl, events, modalCtrl, platform, viewCtrl) {
        this.pAccount = pAccount;
        this.actionSheetCtrl = actionSheetCtrl;
        this.transfer = transfer;
        this.camera = camera;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.events = events;
        this.modalCtrl = modalCtrl;
        this.platform = platform;
        this.viewCtrl = viewCtrl;
        this.ipFirstName = "";
        this.ipLastName = "";
        this.ipEmail = "";
        this.ipMobile = "";
        this.ipPassword = "";
        this.userName = "";
        this.image_account = "assets/images/images.jpg";
        this.fnameValidate = false;
        this.lnameValidate = false;
        this.emailValidate = false;
        this.passwordValidate = false;
        this.message_fname_validate = "";
        this.message_lname_validate = "";
        this.message_email_validate = "";
        this.message_password_validate = "";
    }
    ProfilePage.prototype.Save = function () {
        var vret = true;
        if (this.ipFirstName == "") {
            this.fnameValidate = true;
            this.message_fname_validate = "Enter your Firstname";
            vret = false;
        }
        else {
            this.fnameValidate = false;
            this.message_fname_validate = "";
        }
        if (vret) {
            if (this.ipLastName == "") {
                this.lnameValidate = true;
                this.message_lname_validate = "Enter your Lastname";
                vret = false;
            }
            else {
                this.lnameValidate = false;
                this.message_lname_validate = "";
            }
        }
        if (vret) {
            if (!this.emailValidator(this.ipEmail)) {
                this.emailValidate = true;
                this.message_email_validate = "Check your email address";
                vret = false;
            }
            else {
                this.emailValidate = false;
                this.message_email_validate = "";
            }
        }
        if (vret) {
            if (this.ipPassword != "") {
                if (!this.passwordValidator(this.ipPassword)) {
                    this.passwordValidate = true;
                    this.message_password_validate = "Enter your password, Minimum 5 charactor";
                    vret = false;
                }
                else {
                    this.passwordValidate = false;
                    this.message_password_validate = "";
                }
            }
        }
        if (vret) {
            this.goSave();
        }
    };
    ProfilePage.prototype.goSave = function () {
        var _this = this;
        localStorage.userName = this.ipFirstName + " " + this.ipLastName;
        var userid = localStorage.userId;
        if (this.ipPassword.trim() !== "") {
            this.pAccount.UserUpdate({
                id: userid,
                name: this.ipFirstName + " " + this.ipLastName,
                email: this.ipEmail,
                password: this.ipPassword
            }).then(function (r) {
                var datas = {
                    name: _this.ipFirstName + " " + _this.ipLastName,
                    email: _this.ipEmail
                };
                //this.viewCtrl.dismiss(datas);
                _this.openPopAlertUpdateComplete("แก้ไขข้อมูลเสร็จเรียบร้อยแล้ว", datas);
            });
        }
        else {
            this.pAccount.UserUpdate({
                id: userid,
                // phone: this.ipMobile, 
                name: this.ipFirstName + " " + this.ipLastName,
                email: this.ipEmail
            }).then(function (r) {
                var datas = {
                    name: _this.ipFirstName + " " + _this.ipLastName,
                    email: _this.ipEmail
                };
                //this.viewCtrl.dismiss(datas);
                _this.openPopAlertUpdateComplete("แก้ไขข้อมูลเสร็จเรียบร้อยแล้ว", datas);
            });
        }
    };
    ProfilePage.prototype.openPopAlertUpdateComplete = function (msg, datas) {
        var _this = this;
        var set_Popup = {
            title: "Pop-up Request",
            text_message: msg,
            btn_cancel: "",
            btn_confirm: "Done",
            isCancel: false
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            //console.log(popup);  
            _this.viewCtrl.dismiss(datas);
        });
        popModal.present();
    };
    ProfilePage.prototype.ionViewDidLoad = function () {
        this.loadData();
    };
    ProfilePage.prototype.loadData = function () {
        var _this = this;
        this.pAccount.UserGet().then(function (r) {
            _this.ipFirstName = r.data.name.split(' ')[0];
            _this.ipLastName = r.data.name.split(' ')[1];
            _this.ipEmail = r.data.email;
            _this.ipMobile = r.data.phone;
            _this.userName = r.data.name;
            if (r.data.image) {
                _this.image_account = r.data.image;
            }
        });
    };
    ProfilePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ProfilePage.prototype.UploadImg = function () {
        var _this = this;
        if (this.platform.is('android')) {
            var actionSheet = this.actionSheetCtrl.create({
                title: 'Select',
                buttons: [
                    {
                        text: 'PHOTO Library',
                        handler: function () {
                            _this.getPHOTOLIBRARY();
                        }
                    },
                    {
                        text: 'Take Picture',
                        handler: function () {
                            _this.getCammeraTakePicture();
                        }
                    },
                    {
                        text: 'Cancel',
                        role: 'cancel'
                    }
                ]
            });
            actionSheet.present();
        }
        else {
            var actionSheet = this.actionSheetCtrl.create({
                title: 'Select',
                buttons: [
                    {
                        text: 'PHOTO Library',
                        handler: function () {
                            _this.getPHOTOLIBRARY();
                        }
                    },
                    {
                        text: 'Cancel',
                        role: 'cancel'
                    }
                ]
            });
            actionSheet.present();
        }
        /*
         let uId = localStorage.userId;
         let profileModal = this.modalCtrl.create(UploadImagesPage , { userId: uId });
         profileModal.onDidDismiss(data => {
           if (data != null) {
             console.log(data);
             this.image_account = data.path;
             this.UpdateUserImage(data.fileName);
           }
         });
         profileModal.present();
         */
    };
    ProfilePage.prototype.getCammeraTakePicture = function () {
        /*
        const options: CameraOptions = {
          quality: 100,
          destinationType: this.camera.DestinationType.FILE_URI,
          sourceType: this.camera.PictureSourceType.CAMERA
        }
    
        */
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            allowEdit: false,
            correctOrientation: false,
            saveToPhotoAlbum: true
        };
        /*
          this.camera.getPicture(options).then((imageData) => {
            this.imageURI = imageData;
            this.base64Image = 'data:image/jpeg;base64,' + imageData;
            this.uploadFile();
          }, (err) => {
            console.log(err);
          });
      
      
         var options: CameraOptions = {
          quality: 100,
          sourceType: this.camera.PictureSourceType.CAMERA,
          saveToPhotoAlbum: false,
          correctOrientation: true
      };
      
      this.camera.getPicture(options).then(imagePath => {
          if (this.platform.is('android')){
            this.imageURI = imagePath;
            this.base64Image = 'data:image/jpeg;base64,' + imagePath;
            this.uploadFile();
          } else {
              var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
              var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
      
            this.imageURI = correctPath;
            this.base64Image = 'data:image/jpeg;base64,' + correctPath;
            this.uploadFile();
              
          }
      });
      */
        this.camera.getPicture(options)
            .then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:
            var base64Image_ = 'data:image/jpeg;base64,' + imageData;
            _this.base64Image = base64Image_;
            _this.imageURI = imageData;
            // console.log(imageData);
            // console.log(base64Image);
            _this.uploadFile();
        }, function (err) {
            // console.error('error al sacar la foto --> ' + err);
        });
    };
    ProfilePage.prototype.getPHOTOLIBRARY = function () {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.imageURI = imageData;
            _this.base64Image = 'data:image/jpeg;base64,' + imageData;
            _this.uploadFile();
        }, function (err) {
            //console.log(err);
        });
    };
    ProfilePage.prototype.uploadFile = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Uploading..."
        });
        loader.present();
        var fileTransfer = this.transfer.create();
        //console.log(this.imageURI);
        var options = {
            fileKey: 'file_upload',
            fileName: this.imageURI.substr(this.imageURI.lastIndexOf('/') + 1),
            chunkedMode: false,
            mimeType: "image/jpeg",
            headers: {}
        };
        //this.cfileName = this.imageURI.substr(this.imageURI.lastIndexOf('/') + 1); 
        //fileTransfer.upload(this.imageURI, 'http://192.168.1.105/galleryimages/upload.php', options)
        fileTransfer.upload(this.imageURI, 'https://limoapp.me/api/user/upload_image', options)
            .then(function (data) {
            // console.log(JSON.parse(data.response));   
            var obj = JSON.parse(data.response);
            //console.log(obj);
            //console.log(obj.data.filename);
            //console.log(obj.data.path);
            loader.dismiss();
            if (obj.data.path != "") {
                _this.image_account = obj.data.path;
                localStorage.userImage = obj.data.path;
                _this.UpdateUserImage(obj.data.filename);
                //  var datas = {
                //    fileName: obj.data.filename, 
                //    image: obj.data.path
                //   }
                //  this.viewCtrl.dismiss(datas);    
            }
        }, function (err) {
            //console.log(err);
            loader.dismiss();
        });
    };
    ProfilePage.prototype.UpdateUserImage = function (img) {
        var _this = this;
        var uId = localStorage.userId;
        //console.log(uId);
        //console.log(img);
        this.pAccount.UserUpdate({
            id: uId,
            image: img
        }).then(function (r) {
            _this.events.publish('username:changed', _this.userName);
        });
    };
    ProfilePage.prototype.changePhone = function () {
        var _this = this;
        if (localStorage.memberType == "0") {
            var set_Popup = {
                title: "Change Phone Number ",
                text_message: "Do you want to Change Phone Number? ",
                btn_cancel: "Cancel",
                btn_confirm: "Ok",
                isCancel: true
            };
            var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__modal_popup_modal_popup__["a" /* PopupPage */], {
                setPopup: set_Popup
            });
            popModal.onDidDismiss(function (popup) {
                if (popup) {
                    _this.OpenChangedPhone();
                }
            });
            popModal.present();
        }
    };
    ProfilePage.prototype.OpenChangedPhone = function () {
        var _this = this;
        var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__phone_update_phone_update__["a" /* PhoneUpdatePage */]);
        profileModal.onDidDismiss(function (data) {
            if (data != null) {
                localStorage.phoneNumber = data.phone;
                _this.ipMobile = data.phone;
                _this.events.publish('username:changed', _this.userName);
            }
        });
        profileModal.present();
    };
    ProfilePage.prototype.emailValidator = function (str) {
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return emailPattern.test(str);
    };
    ProfilePage.prototype.passwordValidator = function (str) {
        var regExp = /^[a-zA-Z0-9]{5,20}/;
        return regExp.test(str);
    };
    ProfilePage.prototype.fnameValidator = function (str) {
        var regExp = /^[a-zA-Z ]{1,30}$/;
        return regExp.test(str);
    };
    ProfilePage.prototype.lnameValidator = function (str) {
        var regExp = /^[a-zA-Z ]{1,30}$/;
        return regExp.test(str);
    };
    ProfilePage.prototype.phoneValidator = function (str) {
        //let regExp = /^[0-9]{10}$/; 
        var regExp = /^([0]{1})([0-9]{2})([0-9]{3})([0-9]{4})$/;
        return regExp.test(str);
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/profile/profile.html"*/'<ion-header>\n   <ion-navbar>\n    <ion-buttons left>\n      <button ion-button (click)="dismiss()" color="light">\n        <ion-icon name="ios-arrow-round-back-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n    <!-- <ion-title full text-center>EDIT ACCOUNT</ion-title> -->\n  </ion-navbar> \n</ion-header>\n\n<ion-content> \n  <div class="panel-top">\n    <h2>EDIT ACCOUNT</h2>\n    <ion-item no-padding no-margin no-lines text-center (click)="UploadImg()" color="primary"> \n      <img src="{{image_account}}" style="margin-left: 30px"/> <span item-end> +</span>\n    </ion-item>\n  </div> \n  <ion-item class="i-item">\n    <p>First Name</p>\n    <input type="text" [(ngModel)]="ipFirstName" (keyup.enter)="Save()" />\n    <p style="color: red;" *ngIf="fnameValidate">* {{message_fname_validate}}</p>\n  </ion-item>\n  <ion-item class="i-item">\n    <p>Last Name</p>\n    <input type="text" [(ngModel)]="ipLastName" (keyup.enter)="Save()" />\n    <p style="color: red;" *ngIf="lnameValidate">* {{message_lname_validate}}</p>\n  </ion-item>\n  <ion-item class="i-item">\n    <P>Email Address</P>\n    <input type="email" [(ngModel)]="ipEmail" (keyup.enter)="Save()" /> \n    <p style="color: red;" *ngIf="emailValidate">* {{message_email_validate}}</p>\n  </ion-item>\n  <ion-item class="i-item" (click)="changePhone()"> \n    <p>Mobile Phone</p> \n    <span style="margin-left: 55px">{{ipMobile}}</span>\n    <!-- <input type="tel" [(ngModel)]="ipMobile" /> -->\n  </ion-item>\n  <ion-item class="i-item">\n    <P>Password</P>\n    <input type="password" [(ngModel)]="ipPassword" (keyup.enter)="Save()" /> \n    <p style="color: red;" *ngIf="passwordValidate">* {{message_password_validate}}</p>\n  </ion-item>\n\n  <div padding class="btn-action">\n    <button ion-button full color="primary" (click)="Save()">CONFIRM</button>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/profile/profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_account_account__["a" /* AccountProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 237:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OtpUpdatePhonePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__validate_password_validate_password__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__create_newuser_create_newuser__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_account_account__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var OtpUpdatePhonePage = /** @class */ (function () {
    function OtpUpdatePhonePage(navCtrl, navParams, viewCtrl, actionSheetCtrl, pAccount) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.pAccount = pAccount;
        this.phoneNumber = "";
        this.otpForValidate = "";
        this.maxtime = 60;
        this.ipOTP = {
            ip1: "",
            ip2: "",
            ip3: "",
            ip4: "",
        };
        this.stateValidate = false;
        this.timmerTimeup = false;
        this.phoneNumber = this.navParams.get("phone");
        this.otpForValidate = this.navParams.get("code");
    }
    OtpUpdatePhonePage.prototype.ionViewDidLoad = function () {
        this.startTimer();
    };
    OtpUpdatePhonePage.prototype.popPage = function () {
        this.navCtrl.pop();
    };
    OtpUpdatePhonePage.prototype.clearAll = function () {
        this.ipOTP.ip1 = "";
        this.ipOTP.ip2 = "";
        this.ipOTP.ip3 = "";
        this.ipOTP.ip4 = "";
        this.stateValidate = false;
    };
    OtpUpdatePhonePage.prototype.keyupOtp = function (ev, i) {
        if (i == "1") {
            if (this.ipOTP.ip1 == "") {
                // console.log(ev.keyCode);
            }
            else {
                //console.log(ev.target.value);
                // this.ipOTP.ip1 = ev.target.value;
                this.otp2.setFocus();
            }
        }
        else if (i == "2") {
            if (this.ipOTP.ip2 == "") {
                if (ev.keyCode == 8) {
                    this.otp1.setFocus();
                }
            }
            else {
                this.otp3.setFocus();
            }
        }
        else if (i == "3") {
            if (this.ipOTP.ip3 == "") {
                if (ev.keyCode == 8) {
                    this.otp2.setFocus();
                }
            }
            else {
                this.otp4.setFocus();
            }
        }
        else if (i == "4") {
            //console.log(this.ipOTP.ip4);
            //console.log(ev.target.value);
            if (this.ipOTP.ip4.length > 1) {
                this.ipOTP.ip4 = ev.target.value.slice(1);
            }
            else {
                if (this.ipOTP.ip4 == "") {
                    if (ev.keyCode == 8) {
                        this.otp3.setFocus();
                    }
                }
                else {
                    this.nextState();
                }
            }
        }
    };
    OtpUpdatePhonePage.prototype.nextState = function () {
        var _this = this;
        if (this.ipOTP.ip1 != "" && this.ipOTP.ip2 != ""
            && this.ipOTP.ip3 != "" && this.ipOTP.ip4 != "") {
            var ipLastOtp = this.ipOTP.ip1 + this.ipOTP.ip2 + this.ipOTP.ip3 + this.ipOTP.ip4;
            if (ipLastOtp == this.otpForValidate) {
                var userid = localStorage.userId;
                this.pAccount.UserUpdatePhone({
                    id: userid,
                    phone: this.phoneNumber
                }).then(function (r) {
                    if (r.status == 200) {
                        var datas = {
                            phone: _this.phoneNumber
                        };
                        _this.viewCtrl.dismiss(datas);
                    }
                });
            }
            else {
                this.stateValidate = true;
            }
        }
        else {
            this.stateValidate = true;
        }
    };
    OtpUpdatePhonePage.prototype.startTimer = function () {
        var _this = this;
        setTimeout(function (x) {
            _this.maxtime -= 1;
            if (_this.maxtime > 0) {
                _this.startTimer();
            }
            if (_this.maxtime <= 0) {
                _this.timmerTimeup = true;
            }
        }, 1000);
    };
    OtpUpdatePhonePage.prototype.openModal = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Resend to ' + this.phoneNumber,
            cssClass: 'resend-css',
            buttons: [
                {
                    text: 'SEND CODE VIA SMS',
                    role: 'destructive',
                    handler: function () {
                        _this.pAccount.UserCheck(_this.phoneNumber).then(function (r) {
                            _this.phoneNumber = r.data.phone;
                            _this.otpForValidate = r.data.code;
                            // console.log("Code : ", this.otpForValidate);
                            _this.maxtime = 5;
                            _this.timmerTimeup = false;
                            _this.startTimer();
                        });
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                }
            ]
        });
        actionSheet.present();
    };
    OtpUpdatePhonePage.prototype.OnClick_ConfirmOTP = function () {
        //console.log(this.inputOtp);
    };
    OtpUpdatePhonePage.prototype.OnClick_ExistsUser = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__validate_password_validate_password__["a" /* ValidatePasswordPage */]);
    };
    OtpUpdatePhonePage.prototype.OnClick_NewMember = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__create_newuser_create_newuser__["a" /* CreateNewuserPage */]);
    };
    OtpUpdatePhonePage.prototype.phoneValidator = function (str) {
        var regExp = /^([0]{1})([0-9]{2})([0-9]{3})([0-9]{4})$/;
        //let regExp = /^[0-9]{10}$/;
        return regExp.test(str);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('otp1'),
        __metadata("design:type", Object)
    ], OtpUpdatePhonePage.prototype, "otp1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('otp2'),
        __metadata("design:type", Object)
    ], OtpUpdatePhonePage.prototype, "otp2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('otp3'),
        __metadata("design:type", Object)
    ], OtpUpdatePhonePage.prototype, "otp3", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('otp4'),
        __metadata("design:type", Object)
    ], OtpUpdatePhonePage.prototype, "otp4", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('ffocus'),
        __metadata("design:type", Object)
    ], OtpUpdatePhonePage.prototype, "ffocus", void 0);
    OtpUpdatePhonePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-otp-update-phone',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/otp-update-phone/otp-update-phone.html"*/' \n<ion-header>\n\n  <ion-navbar>\n    <ion-title></ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n  <div class="panel-top">\n    <h4>Enter 4 ditgit code sent to you at your phone number </h4>\n    <div class="otp-panel">\n      <ion-input maxlength="1" type="number" name="otp1" #otp1 (keyup)="keyupOtp($event,\'1\')" (focus)="clearAll()" [(ngModel)]="ipOTP.ip1"></ion-input>\n      <ion-input maxlength="1" type="number" name="otp2" #otp2 (keyup)="keyupOtp($event,\'2\')" (focus)="clearNum(\'2\')" [(ngModel)]="ipOTP.ip2"></ion-input>\n      <ion-input maxlength="1" type="number" name="otp3" #otp3 (keyup)="keyupOtp($event,\'3\')" (focus)="clearNum(\'3\')" [(ngModel)]="ipOTP.ip3"></ion-input>\n      <ion-input maxlength="1" type="number" name="otp4" #otp4 (keyup)="keyupOtp($event,\'4\')" (focus)="clearNum(\'4\')" [(ngModel)]="ipOTP.ip4"></ion-input>\n      <h6 *ngIf="stateValidate">Did you enter the correct phone number ?</h6>\n    </div>\n\n  </div>\n  <div class="panel-detail">\n    <h6>Resend code\n      <span *ngIf="!timmerTimeup">in {{maxtime}}s</span>\n      <span *ngIf="timmerTimeup">\n        <button ion-button (click)="openModal()">Resend</button>\n      </span>\n    </h6>\n    <h6 (click)="popPage()" class="cBlue">Edit my phone number</h6>\n  </div>\n\n  <div class="panel-action">\n      <ion-icon name="ios-arrow-round-forward-outline" color="light" \n      (click)="nextState()"></ion-icon>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/otp-update-phone/otp-update-phone.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"], __WEBPACK_IMPORTED_MODULE_4__providers_account_account__["a" /* AccountProvider */]])
    ], OtpUpdatePhonePage);
    return OtpUpdatePhonePage;
}());

//# sourceMappingURL=otp-update-phone.js.map

/***/ }),

/***/ 238:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyplacesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__authentication_authentication__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_account_account__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__modal_popup_modal_popup__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MyplacesPage = /** @class */ (function () {
    function MyplacesPage(navCtrl, pAccount, modalCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.pAccount = pAccount;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.emailAddress = "";
        this.mobilePhone = "";
        this.userName = "";
        this.image_account = "";
        this.placeList = [];
    }
    MyplacesPage.prototype.ionViewDidLoad = function () {
        //this.loadPlace();
        this.initPlaceLists();
    };
    MyplacesPage.prototype.initPlaceLists = function () {
        var _this = this;
        this.pAccount.UserListingPlace().then(function (r) {
            if (r.status == 200 && r.message == "Success") {
                _this.placeList = r.data;
            }
        });
    };
    MyplacesPage.prototype.loadPlace = function () {
        this.placeList.push({
            type: 1,
            name: "สนามบินสุวรรณภูมิ 999 หมู่ 1 Nong Prue, Amphoe Bang Phli, Chang Wat Samut Prakan 10540"
        });
        this.placeList.push({
            type: 2,
            name: "สนามบินสุวรรณภูมิ 222 Vibhavadi Rangsit Rd, Khwaeng Sanambin, Khet Don Mueang, Krung Thep Maha Nakhon 10210"
        });
        this.placeList.push({
            type: 3,
            name: "สนามบินสุวรรณภูมิ 222 Vibhavadi Rangsit Rd, Khwaeng Sanambin, Khet Don Mueang, Krung Thep Maha Nakhon 10210"
        });
    };
    MyplacesPage.prototype.OnClick_Logout = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__authentication_authentication__["a" /* AuthenticationPage */]);
    };
    MyplacesPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    MyplacesPage.prototype.removeItem = function (p) {
        var _this = this;
        var set_Popup = {
            title: "Confirm delete",
            text_message: "Do you want to delete this place? ",
            btn_cancel: "Cancel",
            btn_confirm: "Delete",
            isCancel: true
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            // console.log(popup);  
            if (popup) {
                //  console.log(p.id);  
                _this.pAccount.UserDelPlace(p.place_id).then(function (r) {
                    _this.initPlaceLists();
                });
            }
        });
        popModal.present();
    };
    MyplacesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-myplaces',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/myplaces/myplaces.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-buttons left>\n            <button ion-button (click)="dismiss()" color="light">\n                <ion-icon name="ios-arrow-round-back-outline"></ion-icon>\n            </button>\n        </ion-buttons>\n        <ion-title full text-center>MY PLACES</ion-title>\n        <ion-buttons end>\n                <button ion-button>\n                    <ion-icon name="arrow-back" color="primary"></ion-icon>\n                </button>\n            </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>     \n    <ion-list>\n        <!-- <ion-item-divider color="light" style="font-size: 1.2em"></ion-item-divider> -->\n        <ion-item-sliding *ngFor="let item of placeList" (click)="chooseItem(item)">\n                <ion-item text-wrap>\n                    <img *ngIf="item.type==\'home\'" src="assets/images/home.png" width="26"/>\n                    <img *ngIf="item.type==\'work\'" src="assets/images/work.png" width="26"/>\n                    <img *ngIf="item.type==\'other\'" src="assets/images/placeholder.png" width="26"/>\n                    <span>{{item.type}}</span>\n                    <p>{{item.name}}</p>\n                </ion-item>  \n                <ion-item-options icon-start>\n                        <button ion-button  (click)="removeItem(item)">\n                            <ion-icon name="ios-trash-outline"></ion-icon>\n                            Delete\n                        </button>\n               </ion-item-options>\n        </ion-item-sliding> \n        <ion-item> \n        </ion-item>\n    </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/myplaces/myplaces.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_3__providers_account_account__["a" /* AccountProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], MyplacesPage);
    return MyplacesPage;
}());

//# sourceMappingURL=myplaces.js.map

/***/ }),

/***/ 239:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(260);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 260:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(302);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_authentication_authentication__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_validate_otp_validate_otp__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_validate_password_validate_password__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_create_newuser_create_newuser__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_your_trip_your_trip__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_payment_payment__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_payment_add_payment_add__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_promotion_promotion__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_setting_setting__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_term_condition_term_condition__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_phone_input_phone_input__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_password_input_password_input__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_modal_resend_modal_resend__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_home_home__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_home_search_home_search__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_status_bar__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_splash_screen__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_account_account__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__providers_rest_api_rest_api__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__Declaration__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__providers_order_order__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_legal_legal__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__providers_page_api_page_api__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_term_term__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_refund_refund__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__providers_driver_driver__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__providers_country_country__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32_ionic_select_searchable__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_32_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__angular_common_http__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__ionic_native_in_app_browser__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_autocomplete_autocomplete__ = __webpack_require__(325);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__ionic_native_file__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__ionic_native_file_transfer__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__ionic_native_file_path__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__ionic_native_camera__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_profile_profile__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_modal_set_card_modal_select_payment__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_modal_cars_modal_cars__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_modal_option_modal_option__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_modal_popup_modal_popup__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_modal_place_modal_place__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_myplaces_myplaces__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_add_place_add_place__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_phone_update_phone_update__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__pages_otp_update_phone_otp_update_phone__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__pages_home_driver_home_driver__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__ionic_native_native_audio__ = __webpack_require__(231);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__ionic_native_call_number__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__pages_privacy_privacy__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__pages_add_card_add_card__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__pages_my_location_my_location__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__ionic_native_google_maps__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__pages_payment_omise_payment_omise__ = __webpack_require__(225);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


























































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_18__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_home_driver_home_driver__["a" /* HomeDriverPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_home_search_home_search__["a" /* HomeSearchPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_authentication_authentication__["a" /* AuthenticationPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_validate_otp_validate_otp__["a" /* ValidateOtpPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_validate_password_validate_password__["a" /* ValidatePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_create_newuser_create_newuser__["a" /* CreateNewuserPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_your_trip_your_trip__["a" /* YourTripPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_payment_payment__["a" /* PaymentPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_payment_add_payment_add__["a" /* PaymentAddPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_promotion_promotion__["a" /* PromotionPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_setting_setting__["a" /* SettingPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_term_condition_term_condition__["a" /* TermConditionPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_phone_input_phone_input__["a" /* PhoneInputPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_phone_update_phone_update__["a" /* PhoneUpdatePage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_modal_resend_modal_resend__["a" /* ModalResendPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_password_input_password_input__["a" /* PasswordInputPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_legal_legal__["a" /* LegalPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_term_term__["a" /* TermPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_autocomplete_autocomplete__["a" /* AutocompletePage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_modal_set_card_modal_select_payment__["a" /* SelectPayment */],
                __WEBPACK_IMPORTED_MODULE_42__pages_modal_cars_modal_cars__["a" /* SelectCars */],
                __WEBPACK_IMPORTED_MODULE_43__pages_modal_option_modal_option__["a" /* SelectOption */],
                __WEBPACK_IMPORTED_MODULE_45__pages_modal_place_modal_place__["a" /* SelectPlace */],
                __WEBPACK_IMPORTED_MODULE_46__pages_myplaces_myplaces__["a" /* MyplacesPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_modal_popup_modal_popup__["a" /* PopupPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_add_place_add_place__["a" /* AddPlacePage */],
                __WEBPACK_IMPORTED_MODULE_49__pages_otp_update_phone_otp_update_phone__["a" /* OtpUpdatePhonePage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_refund_refund__["a" /* RefundPage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_privacy_privacy__["a" /* PrivacyPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_add_card_add_card__["a" /* AddCardPage */],
                __WEBPACK_IMPORTED_MODULE_55__pages_my_location_my_location__["a" /* MyLocationPage */],
                __WEBPACK_IMPORTED_MODULE_57__pages_payment_omise_payment_omise__["a" /* PaymentOmisePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_33__angular_common_http__["a" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_32_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: []
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_18__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_home_driver_home_driver__["a" /* HomeDriverPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_home_search_home_search__["a" /* HomeSearchPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_authentication_authentication__["a" /* AuthenticationPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_validate_otp_validate_otp__["a" /* ValidateOtpPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_validate_password_validate_password__["a" /* ValidatePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_create_newuser_create_newuser__["a" /* CreateNewuserPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_your_trip_your_trip__["a" /* YourTripPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_payment_payment__["a" /* PaymentPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_payment_add_payment_add__["a" /* PaymentAddPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_promotion_promotion__["a" /* PromotionPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_setting_setting__["a" /* SettingPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_term_condition_term_condition__["a" /* TermConditionPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_phone_input_phone_input__["a" /* PhoneInputPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_phone_update_phone_update__["a" /* PhoneUpdatePage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_modal_resend_modal_resend__["a" /* ModalResendPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_password_input_password_input__["a" /* PasswordInputPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_legal_legal__["a" /* LegalPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_term_term__["a" /* TermPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_autocomplete_autocomplete__["a" /* AutocompletePage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_modal_set_card_modal_select_payment__["a" /* SelectPayment */],
                __WEBPACK_IMPORTED_MODULE_42__pages_modal_cars_modal_cars__["a" /* SelectCars */],
                __WEBPACK_IMPORTED_MODULE_43__pages_modal_option_modal_option__["a" /* SelectOption */],
                __WEBPACK_IMPORTED_MODULE_45__pages_modal_place_modal_place__["a" /* SelectPlace */],
                __WEBPACK_IMPORTED_MODULE_46__pages_myplaces_myplaces__["a" /* MyplacesPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_modal_popup_modal_popup__["a" /* PopupPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_add_place_add_place__["a" /* AddPlacePage */],
                __WEBPACK_IMPORTED_MODULE_49__pages_otp_update_phone_otp_update_phone__["a" /* OtpUpdatePhonePage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_refund_refund__["a" /* RefundPage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_privacy_privacy__["a" /* PrivacyPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_add_card_add_card__["a" /* AddCardPage */],
                __WEBPACK_IMPORTED_MODULE_55__pages_my_location_my_location__["a" /* MyLocationPage */],
                __WEBPACK_IMPORTED_MODULE_57__pages_payment_omise_payment_omise__["a" /* PaymentOmisePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_56__ionic_native_google_maps__["a" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_24__Declaration__["a" /* GlobalDeclaration */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_36__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_37__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_38__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_39__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"] },
                __WEBPACK_IMPORTED_MODULE_22__providers_account_account__["a" /* AccountProvider */],
                __WEBPACK_IMPORTED_MODULE_23__providers_rest_api_rest_api__["a" /* RestApiProvider */],
                __WEBPACK_IMPORTED_MODULE_25__providers_order_order__["a" /* OrderProvider */],
                __WEBPACK_IMPORTED_MODULE_27__providers_page_api_page_api__["a" /* PageApiProvider */],
                __WEBPACK_IMPORTED_MODULE_30__providers_driver_driver__["a" /* DriverProvider */],
                __WEBPACK_IMPORTED_MODULE_31__providers_country_country__["a" /* CountryProvider */],
                __WEBPACK_IMPORTED_MODULE_34__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_52__ionic_native_call_number__["a" /* CallNumber */],
                __WEBPACK_IMPORTED_MODULE_51__ionic_native_native_audio__["a" /* NativeAudio */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 302:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_authentication_authentication__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_your_trip_your_trip__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_payment_payment__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_account_account__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_term_term__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_refund_refund__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_profile_profile__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_modal_popup_modal_popup__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_phone_update_phone_update__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_privacy_privacy__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_myplaces_myplaces__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_my_location_my_location__ = __webpack_require__(117);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// import { HomePage } from '../pages/home/home';



//import { LegalPage } from '../pages/legal/legal';





// import { HomeDriverPage } from '../pages/home-driver/home-driver';



var MyApp = /** @class */ (function () {
    function MyApp(events, platform, statusBar, splashScreen, pAccount, alertCtrl, modalCtrl) {
        var _this = this;
        this.events = events;
        this.platform = platform;
        this.pAccount = pAccount;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.image_account = "assets/images/images.jpg";
        this.MemberType = "";
        this.UserFullName = "";
        this.Phone = "";
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need. 
            // statusBar.hide();
            splashScreen.hide();
        });
        this.initializeApp();
        this.pages = [];
        if (localStorage.memberType == "0") {
            // user
            this.pages = [
                { title: 'Limo Payment', component: __WEBPACK_IMPORTED_MODULE_6__pages_payment_payment__["a" /* PaymentPage */] },
                { title: 'History', component: __WEBPACK_IMPORTED_MODULE_5__pages_your_trip_your_trip__["a" /* YourTripPage */] },
                { title: 'My Place', component: __WEBPACK_IMPORTED_MODULE_14__pages_myplaces_myplaces__["a" /* MyplacesPage */] },
                // { title: 'Setting', component: SettingPage },
                // { title: 'Legal', component: LegalPage },
                { title: 'Term & Condition', component: __WEBPACK_IMPORTED_MODULE_8__pages_term_term__["a" /* TermPage */] },
                { title: 'Refund', component: __WEBPACK_IMPORTED_MODULE_9__pages_refund_refund__["a" /* RefundPage */] },
                { title: 'Privacy & Policy', component: __WEBPACK_IMPORTED_MODULE_13__pages_privacy_privacy__["a" /* PrivacyPage */] },
            ];
        }
        else if (localStorage.memberType == "1") {
            // banned
        }
        else if (localStorage.memberType == "2") {
            // driver
            this.pages = [
                // { title: 'Setting', component: SettingPage },
                //{ title: 'Legal', component: LegalPage },
                { title: 'Term & Condition', component: __WEBPACK_IMPORTED_MODULE_8__pages_term_term__["a" /* TermPage */] },
                // { title: 'Refund', component: RefundPage }, 
                { title: 'Privacy & Policy', component: __WEBPACK_IMPORTED_MODULE_13__pages_privacy_privacy__["a" /* PrivacyPage */] },
            ];
        }
        events.subscribe('username:changed', function (username) {
            if (username !== undefined && username !== "") {
                _this.UserFullName = username;
                _this.Phone = localStorage.phoneNumber;
                if (localStorage.userImage) {
                    _this.image_account = localStorage.userImage;
                }
                else {
                    _this.image_account = "assets/images/images.jpg";
                }
                _this.pages = [];
                if (localStorage.memberType == "0") {
                    _this.MemberType = "Member";
                    _this.pages = [
                        { title: 'Limo Payment', component: __WEBPACK_IMPORTED_MODULE_6__pages_payment_payment__["a" /* PaymentPage */] },
                        { title: 'History', component: __WEBPACK_IMPORTED_MODULE_5__pages_your_trip_your_trip__["a" /* YourTripPage */] },
                        { title: 'My Place', component: __WEBPACK_IMPORTED_MODULE_14__pages_myplaces_myplaces__["a" /* MyplacesPage */] },
                        // { title: 'Setting', component: SettingPage },
                        //{ title: 'Legal', component: LegalPage },
                        { title: 'Term & Condition', component: __WEBPACK_IMPORTED_MODULE_8__pages_term_term__["a" /* TermPage */] },
                        { title: 'Refund', component: __WEBPACK_IMPORTED_MODULE_9__pages_refund_refund__["a" /* RefundPage */] },
                        { title: 'Privacy & Policy', component: __WEBPACK_IMPORTED_MODULE_13__pages_privacy_privacy__["a" /* PrivacyPage */] },
                    ];
                }
                else if (localStorage.memberType == "1") {
                    // Banned User
                }
                else if (localStorage.memberType == "2") {
                    _this.MemberType = "Driver";
                    _this.pages = [
                        // { title: 'Setting', component: SettingPage },
                        // { title: 'Legal', component: LegalPage },
                        { title: 'Term & Condition', component: __WEBPACK_IMPORTED_MODULE_8__pages_term_term__["a" /* TermPage */] },
                        // { title: 'Refund', component: RefundPage }, 
                        { title: 'Privacy & Policy', component: __WEBPACK_IMPORTED_MODULE_13__pages_privacy_privacy__["a" /* PrivacyPage */] },
                    ];
                }
            }
        });
    }
    MyApp.prototype.ChangedPhone = function () {
        var _this = this;
        if (localStorage.memberType == "0") {
            var set_Popup = {
                title: "Change Phone Number ",
                text_message: "Do you want to Change Phone Number? ",
                btn_cancel: "Cancel",
                btn_confirm: "Ok",
                isCancel: true
            };
            var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_11__pages_modal_popup_modal_popup__["a" /* PopupPage */], {
                setPopup: set_Popup
            });
            popModal.onDidDismiss(function (popup) {
                if (popup) {
                    _this.OpenChangedPhone();
                }
            });
            popModal.present();
        }
    };
    MyApp.prototype.OpenChangedPhone = function () {
        var _this = this;
        var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_12__pages_phone_update_phone_update__["a" /* PhoneUpdatePage */]);
        profileModal.onDidDismiss(function (data) {
            if (data != null) {
                localStorage.phoneNumber = data.phone;
                _this.Phone = data.phone;
            }
        });
        profileModal.present();
    };
    MyApp.prototype.GotoPofile = function () {
        var _this = this;
        var uId = localStorage.userId;
        var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__pages_profile_profile__["a" /* ProfilePage */], { userId: uId });
        profileModal.onDidDismiss(function (data) {
            if (data != null) {
                _this.UserFullName = data.name;
            }
        });
        profileModal.present();
    };
    MyApp.prototype.initializeApp = function () {
        var userId = localStorage.userId;
        if (userId) {
            this.relogin();
        }
        else {
            localStorage.clear();
            //this.nav.setRoot(AuthenticationPage);
            this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_authentication_authentication__["a" /* AuthenticationPage */];
        }
    };
    MyApp.prototype.relogin = function () {
        var _this = this;
        this.pAccount.UserLogOut().then(function (r) {
            if (r) {
                if (r.status == 200) {
                    _this.nextlogin();
                }
                else {
                    localStorage.clear();
                    //this.nav.setRoot(AuthenticationPage);
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_authentication_authentication__["a" /* AuthenticationPage */];
                }
            }
            else {
                localStorage.clear();
                //this.nav.setRoot(AuthenticationPage);
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_authentication_authentication__["a" /* AuthenticationPage */];
            }
        });
    };
    MyApp.prototype.nextlogin = function () {
        var _this = this;
        var _phoneNumber = localStorage.phoneNumber;
        var _Password = localStorage.Password;
        this.pAccount.UserLogin(_phoneNumber, _Password).then(function (r) {
            console.log(r);
            if (r) {
                if (r.status == 200) {
                    _this.initUserProfile();
                }
                else {
                    localStorage.clear();
                    //this.nav.setRoot(AuthenticationPage);
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_authentication_authentication__["a" /* AuthenticationPage */];
                }
            }
            else {
                localStorage.clear();
                //this.nav.setRoot(AuthenticationPage);
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_authentication_authentication__["a" /* AuthenticationPage */];
            }
        });
    };
    MyApp.prototype.initUserProfile = function () {
        var _this = this;
        this.pAccount.UserGet().then(function (r) {
            if (r) {
                if (r.status == 200) {
                    _this.UserFullName = r.data.name;
                    _this.Phone = r.data.phone;
                    if (r.data.image != "") {
                        _this.image_account = r.data.image;
                    }
                    else {
                        _this.image_account = "assets/images/images.jpg";
                    }
                    // "assets/images/images.jpg"; 
                    if (r.data.type == "0") {
                        _this.MemberType = "Member";
                        //this.rootPage = HomePage;
                    }
                    else if (r.data.type == "1") {
                        _this.MemberType = "Banned";
                        //this.rootPage = HomePage;  
                    }
                    else if (r.data.type == "2") {
                        _this.MemberType = "Driver";
                        //this.rootPage = HomeDriverPage;
                    }
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_15__pages_my_location_my_location__["a" /* MyLocationPage */];
                }
                else {
                    localStorage.clear();
                    //this.nav.setRoot(AuthenticationPage);
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_authentication_authentication__["a" /* AuthenticationPage */];
                }
            }
            else {
                localStorage.clear();
                //this.nav.setRoot(AuthenticationPage);
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_authentication_authentication__["a" /* AuthenticationPage */];
            }
        });
    };
    MyApp.prototype.OnClick_Logout = function () {
        var _this = this;
        this.pAccount.UserLogOut().then(function (r) {
            var lat = localStorage.currentLat;
            var lng = localStorage.currentLng;
            localStorage.clear();
            localStorage.currentLat = lat;
            localStorage.currentLng = lng;
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_authentication_authentication__["a" /* AuthenticationPage */]);
        });
    };
    MyApp.prototype.openPopLoguut = function () {
        var _this = this;
        var set_Popup = {
            title: "Confirm logout",
            text_message: "Do you want to logout? ",
            btn_cancel: "Cancel",
            btn_confirm: "Confirm",
            isCancel: true
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_11__pages_modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            // console.log(popup);  
            if (popup) {
                _this.OnClick_Logout();
            }
        });
        popModal.present();
    };
    MyApp.prototype.openPage = function (page) {
        var profileModal = this.modalCtrl.create(page.component);
        profileModal.present();
        //this.nav.setRoot(page.component);
    };
    MyApp.prototype.AlertContrlLogout = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm logout',
            message: 'Do you want to logout?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Confirm',
                    handler: function () {
                        _this.OnClick_Logout();
                    }
                }
            ]
        });
        alert.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/app/app.html"*/'\n<ion-header> \n</ion-header>\n\n<ion-menu [content]="content" type="overlay" [swipeEnabled]="false">\n  <!-- <ion-header class="menu-header">\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n  -->\n\n  <ion-content class="side-bg">\n    <div class="user-profile-panel">\n      <div class="profile">\n        <ion-grid>\n          <ion-row>\n            <ion-col col-4 class="img-panel">\n                <img src="{{image_account}}" style="width: 70px;border-radius: 20%;"/>\n            </ion-col>\n            <ion-col col-8 class="name-panel">\n              <p>{{UserFullName}}</p>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n      <div class="user-extra">\n        <ion-grid style="height: 32px;">\n          <ion-row>\n            <ion-col col-7 style="border-right: 1px solid;">\n              <!-- <div>\n                Your Current Point 999\n              </div> --> \n              <div style="text-align: left" (click)="ChangedPhone()"> \n                  <ion-icon item-left  name="ios-arrow-back" \n                  style="margin-right: 20px;color : #fff;font-size: 16px;"></ion-icon>\n                {{Phone}}\n            </div> \n              <!-- <ion-item class="item-bg-navbar" no-lines center> \n                  <ion-label style="color: #fff">{{Phone}}</ion-label>  \n              </ion-item> -->\n            </ion-col>\n            <ion-col col-5> \n              <div style="text-align: right" (click)="GotoPofile()">\n                {{MemberType}} \n                <ion-icon item-right  name="ios-arrow-forward" \n                  style="margin-left: 20px;color : #fff;font-size: 16px;"></ion-icon>\n              </div> \n              <!-- <ion-item class="item-bg-navbar" no-lines right> \n                  <ion-label style="color: #fff">{{MemberType}}</ion-label>  \n              </ion-item> -->\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n\n    </div>\n\n    <ion-list no-lines>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n      <button menuClose ion-item (click)="openPopLoguut()">\n        Logout\n      </button>\n    </ion-list>\n  </ion-content>\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav id="nav" [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_7__providers_account_account__["a" /* AccountProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 313:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PromotionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PromotionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PromotionPage = /** @class */ (function () {
    function PromotionPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PromotionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PromotionPage');
    };
    PromotionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-promotion',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/promotion/promotion.html"*/'<!--\n  Generated template for the PromotionPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n      <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n    <ion-title>promotion</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/promotion/promotion.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], PromotionPage);
    return PromotionPage;
}());

//# sourceMappingURL=promotion.js.map

/***/ }),

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__authentication_authentication__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_account_account__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SettingPage = /** @class */ (function () {
    function SettingPage(navCtrl, pAccount, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.pAccount = pAccount;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.emailAddress = "";
        this.mobilePhone = "";
        this.userName = "";
        this.image_account = "";
        this.favoritePlace = [];
    }
    SettingPage.prototype.ionViewDidLoad = function () {
        this.loadData();
        this.loadFavoritePlace();
    };
    SettingPage.prototype.loadFavoritePlace = function () {
        this.favoritePlace.push({
            label_a: "สนามบินสุวรรณภูมิ",
            label_b: "999 หมู่ 1 Nong Prue, Amphoe Bang Phli, Chang Wat Samut Prakan 10540"
        });
        this.favoritePlace.push({
            label_a: "สนามบินดอนเมือง",
            label_b: "222 Vibhavadi Rangsit Rd, Khwaeng Sanambin, Khet Don Mueang, Krung Thep Maha Nakhon 10210"
        });
    };
    SettingPage.prototype.loadData = function () {
        var _this = this;
        this.pAccount.UserGet().then(function (r) {
            _this.emailAddress = r.data.email;
            _this.mobilePhone = r.data.phone;
            _this.userName = r.data.name;
            if (r.data.image) {
                _this.image_account = r.data.image;
            }
            else {
                _this.image_account = "assets/images/images.jpg";
            }
        });
    };
    SettingPage.prototype.OnClick_Logout = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__authentication_authentication__["a" /* AuthenticationPage */]);
    };
    SettingPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    SettingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-setting',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/setting/setting.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-buttons left>\n            <button ion-button (click)="dismiss()" color="light">\n                <ion-icon name="ios-arrow-round-back-outline"></ion-icon>\n            </button>\n        </ion-buttons>\n        <ion-title full text-center>SETTINGS</ion-title>\n        <ion-buttons end>\n                <button ion-button>\n                    <ion-icon name="arrow-back" color="primary"></ion-icon>\n                </button>\n            </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n    <div class="profile">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-4 class="img-panel">\n                    <img src="{{image_account}}" style="width: 65px;border-radius: 10%;" />\n                </ion-col>\n                <ion-col col-8>\n                    <br>\n                    <p>{{userName}}</p> \n                    <p>{{mobilePhone}}</p>\n                    <p>{{emailAddress}}</p> \n                </ion-col> \n            </ion-row>\n        </ion-grid>\n    </div>\n    <br/>  \n \n    <ion-list>\n        <ion-item-divider color="light" style="font-size: 1.2em">Favorite Place</ion-item-divider>\n        <ion-item-sliding *ngFor="let item of favoritePlace" (click)="chooseItem(item)">\n            <ion-item text-wrap>\n                <img src="assets/images/placeholder.png" width="20" />\n                {{item.label_a}}\n                <p>{{item.label_b}}</p>\n            </ion-item>\n        </ion-item-sliding>\n        <ion-item> \n        </ion-item>\n    </ion-list>\n\n</ion-content>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/setting/setting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_3__providers_account_account__["a" /* AccountProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], SettingPage);
    return SettingPage;
}());

//# sourceMappingURL=setting.js.map

/***/ }),

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalResendPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ModalResendPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ModalResendPage = /** @class */ (function () {
    function ModalResendPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ModalResendPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ModalResendPage');
    };
    ModalResendPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-modal-resend',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/modal-resend/modal-resend.html"*/'<!--\n  Generated template for the ModalResendPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/modal-resend/modal-resend.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ModalResendPage);
    return ModalResendPage;
}());

//# sourceMappingURL=modal-resend.js.map

/***/ }),

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LegalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_page_api_page_api__ = __webpack_require__(39);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the YourTripPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LegalPage = /** @class */ (function () {
    function LegalPage(navCtrl, navParams, pget, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pget = pget;
        this.viewCtrl = viewCtrl;
    }
    LegalPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad YourTripPage');
        // legal, term, refund
        this.pget.PageApiGet("legal").then(function (r) {
            if (r.status == 200 && r.message == "Success") {
                _this.legal_html = r.data.content;
            }
        });
    };
    LegalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    LegalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-legal',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/legal/legal.html"*/'<!--\n  Generated template for the YourTripPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <!-- <ion-navbar>\n       <ion-title>LEGAL\n       </ion-title> \n    </ion-navbar> -->\n</ion-header>\n\n\n<ion-content>\n\n\n        <div class="panel-top">\n                <h2>LEGAL</h2>\n             </div>\n             <div class="horizontalline"></div> \n\n    <!-- <ion-fab right top>\n        <ion-icon name="close" color="primary" style="font-size: 4em;margin-right: 20px;"\n        (click)="dismiss()"></ion-icon>\n    </ion-fab> -->\n \n\n\n    <ion-scroll style="height:80%; " scrollY="true" > \n            <div class="panel-detail-b" [innerHTML]="legal_html"> \n                </div> \n            </ion-scroll>\n\n   <!-- <ion-fab right bottom>\n        <ion-icon name="ios-arrow-round-back-outline" color="cGray" style="font-size: 4em;margin-right: 20px;"\n        (click)="dismiss()"></ion-icon>\n    </ion-fab>  -->\n   \n</ion-content>  \n\n<ion-footer no-border>\n        <ion-toolbar color="light" >\n            <ion-buttons end>\n                <button ion-button (click)="dismiss()">\n                    <ion-icon name="ios-arrow-round-back-outline" color="cGray" style="font-size: 4em;margin-right: 20px;"></ion-icon>\n                </button>\n            </ion-buttons> \n        </ion-toolbar>\n      </ion-footer>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/legal/legal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_page_api_page_api__["a" /* PageApiProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], LegalPage);
    return LegalPage;
}());

//# sourceMappingURL=legal.js.map

/***/ }),

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CountryProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__rest_api_rest_api__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CountryProvider = /** @class */ (function () {
    function CountryProvider(http) {
        this.http = http;
    }
    CountryProvider.prototype.getUserId = function () {
        return localStorage.userId;
    };
    CountryProvider.prototype.CountryList = function () {
        return this.http.PostRequest("/country/listing", {});
    };
    CountryProvider.prototype.CountryGet = function (gId) {
        var obj = {
            "id": gId,
        };
        return this.http.PostRequest("/country/get", obj);
    };
    CountryProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__rest_api_rest_api__["a" /* RestApiProvider */]])
    ], CountryProvider);
    return CountryProvider;
}());

//# sourceMappingURL=country.js.map

/***/ }),

/***/ 325:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutocompletePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AutocompletePage = /** @class */ (function () {
    function AutocompletePage(viewCtrl, zone) {
        this.viewCtrl = viewCtrl;
        this.zone = zone;
        this.latitude = 0;
        this.longitude = 0;
        this.service = new google.maps.places.AutocompleteService();
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        };
    }
    AutocompletePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AutocompletePage.prototype.chooseItem = function (item) {
        this.geo = item;
        this.geoCode(this.geo); //convert Address to lat and long
    };
    AutocompletePage.prototype.updateSearch = function () {
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        // other types available in the API: 'establishment', 'regions', and 'cities'
        var me = this;
        this.service.getPlacePredictions({
            input: this.autocomplete.query,
            componentRestrictions: {
                country: 'TH'
            }, types: ['geocode'], language: 'th-TH'
        }, function (predictions, status) {
            me.autocompleteItems = [];
            me.zone.run(function () {
                if (predictions != null) {
                    predictions.forEach(function (prediction) {
                        me.autocompleteItems.push({
                            label_a: prediction.structured_formatting.main_text,
                            label_b: prediction.description
                        });
                    });
                }
            });
        });
    };
    //convert Address string to lat and long
    AutocompletePage.prototype.geoCode = function (address) {
        var _this = this;
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address.label_b }, function (results, status) {
            _this.latitude = results[0].geometry.location.lat();
            _this.longitude = results[0].geometry.location.lng();
            var sLocation = {
                lat: results[0].geometry.location.lat(),
                lng: results[0].geometry.location.lng()
            };
            var datas = {
                select: sLocation,
                label_a: address.label_a,
                label_b: address.label_b
            };
            _this.viewCtrl.dismiss(datas);
            //alert("lat: " + this.latitude + ", long: " + this.longitude);
        });
    };
    AutocompletePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/autocomplete/autocomplete.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-buttons left>\n\n            <button ion-button (click)="dismiss()" color="light">\n\n                <ion-icon name="ios-arrow-round-back-outline"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n        <ion-title full text-center>Go every where ?</ion-title>\n\n    </ion-navbar>\n\n    <ion-toolbar> \n\n        <ion-searchbar [(ngModel)]="autocomplete.query" [showCancelButton]="true" (ionInput)="updateSearch()"\n\n            (ionCancel)="dismiss()"></ion-searchbar>\n\n    </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <!-- <ion-list>\n\n        <ion-item *ngFor="let item of autocompleteItems" tappable (click)="chooseItem(item)">\n\n             <ion-icon item-start name="ios-pin-outline"></ion-icon> \n\n            <p>{{item.label_a}}</p>\n\n            <h3>{{item.label_b}}</h3>\n\n        </ion-item>\n\n    </ion-list>\n\n   -->\n\n\n\n    <ion-list>\n\n        <ion-item-divider color="light">My Location</ion-item-divider>\n\n        <ion-item-sliding *ngFor="let item of autocompleteItems" (click)="chooseItem(item)">\n\n          <ion-item text-wrap>\n\n              <img src="assets/images/placeholder.png" width="20"/>\n\n              {{item.label_a}}\n\n              <p>{{item.label_b}}</p>\n\n          </ion-item> \n\n        </ion-item-sliding>  \n\n    </ion-list>\n\n\n\n</ion-content>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/autocomplete/autocomplete.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]])
    ], AutocompletePage);
    return AutocompletePage;
}());

//# sourceMappingURL=autocomplete.js.map

/***/ }),

/***/ 37:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_search_home_search__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_order_order__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_driver_driver__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__modal_set_card_modal_select_payment__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modal_option_modal_option__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modal_cars_modal_cars__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_account_account__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__modal_place_modal_place__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__modal_popup_modal_popup__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_call_number__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__payment_payment__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__term_term__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_google_maps__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__payment_omise_payment_omise__ = __webpack_require__(225);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var HomePage = /** @class */ (function () {
    function HomePage(modalCtrl, navCtrl, pDriver, loadingCtrl, toastCtrl, pAccount, callNumber, pOrder) {
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.pDriver = pDriver;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.pAccount = pAccount;
        this.callNumber = callNumber;
        this.pOrder = pOrder;
        this.btnState = "Go Everywhere?";
        this.gmarkers = [];
        this.bounds = [];
        this.start = '';
        this.end = '';
        this.directionsService = new google.maps.DirectionsService;
        this.directionsDisplay = new google.maps.DirectionsRenderer;
        this.MemberType = '0';
        this.searchLocal = false;
        this.searchState = false;
        this.bookState = false;
        this.Payment = "Payment";
        this.pActive = false;
        this.runJob = false;
        this.locationRequest = {
            ipStart: "",
            ipStartDetail: "-",
            ipEnd: "",
            ipEndDetail: "-",
            ipDuration: "",
            ipDistance: "",
            ipDuration_v: "",
            ipDistance_v: ""
        };
        this.dataReq = {
            start_location_lat: "",
            start_location_long: "",
            start_location: "",
            destination_lat: "",
            destination_long: "",
            destination: "",
            car_type: "",
            id_payment: "",
            fare: "",
            note: "",
            expressway: ""
        };
        this.OrderCalList = {
            name: "",
            icon: "",
            fare: 99
        };
        this.locationLabel = {};
        this.userLocation = {};
        this.currentSelectCar = 1;
        this.book_car_hidden = false;
        this.icon_name_v = "ios-arrow-dropdown-outline";
        this.showSetC = true;
        this.showDriver = false;
        this.showVote = false;
        this.Set_Tollway = "Tollway";
        this.tollWay = true;
        this.icon_tollway = "";
        this.icon_tollway_color = "";
        this.icon_text_color = "";
        this.data_driver = {
            ratings: 5,
            driverId: "",
            driverName: "",
            driverImg: "",
            driverPhone: "",
            CarReg: ""
        };
        this.VoteN = 0;
        this.VoteText = "0";
        this.textNote = "";
        this.dTimes = "5";
        this.isomise = false;
        this.hide_view_Vote = true;
        this.setFirst = true;
        this.view_Driver = false;
        //this.SetCurrentLocation();
    }
    HomePage.prototype.viewTerm = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__term_term__["a" /* TermPage */]);
    };
    HomePage.prototype.ionViewWillLeave = function () {
        this.runJob = false;
        clearInterval(this.timerInterval);
        console.log('leave view');
    };
    HomePage.prototype.getIconName = function (index) {
        if (index < this.data_driver.ratings) {
            return "star";
        }
        else {
            return "ios-star-outline";
        }
    };
    HomePage.prototype.getColor = function (index) {
        if (index < this.data_driver.ratings) {
            return '#ffa500';
        }
    };
    HomePage.prototype.getIconVote = function (index) {
        if (index < this.VoteN) {
            return "star";
        }
        else {
            return "ios-star-outline";
        }
    };
    HomePage.prototype.setColor = function (index) {
        if (index == 0) {
            this.VoteN = 1;
            this.VoteText = "1";
        }
        else if (index == 1) {
            this.VoteN = 2;
            this.VoteText = "2";
        }
        else if (index == 2) {
            this.VoteN = 3;
            this.VoteText = "3";
        }
        else if (index == 3) {
            this.VoteN = 4;
            this.VoteText = "4";
        }
        else if (index == 4) {
            this.VoteN = 5;
            this.VoteText = "5";
        }
    };
    HomePage.prototype.ionViewDidLoad = function () {
        this.gmap = __WEBPACK_IMPORTED_MODULE_14__ionic_native_google_maps__["a" /* GoogleMaps */].create('map_canvas2', {});
        // this.SetCurrentLocation();
        this.initMap();
        this.addCurrentLoc();
        this.MemberType = localStorage.memberType;
    };
    HomePage.prototype.initMap = function () {
        if (localStorage.currentLat != undefined) {
            // 
        }
        else {
            localStorage.currentLat = "13.752782";
            localStorage.currentLng = "100.522993";
        }
        var latLng = new google.maps.LatLng(localStorage.currentLat, localStorage.currentLng);
        this.map = new google.maps.Map(this.mapElement.nativeElement, {
            zoom: 17,
            mapTypeControl: false,
            disableDefaultUI: true,
            center: latLng
        });
        this.directionsDisplay.setMap(this.map);
    };
    HomePage.prototype.addCurrentLoc = function () {
        this.removeMarkers();
        var clocation = {
            lat: localStorage.currentLat,
            lng: localStorage.currentLng,
            place_a: localStorage.currentPlaceA,
            place_b: localStorage.currentPlaceB
        };
        console.log(clocation);
        this.currentlocation = clocation.place_a;
        this.currentlocation_b = clocation.place_b;
        this.locationRequest.ipStart = clocation.lat + "," + clocation.lng;
        this.addMarker(clocation.lat, clocation.lng, clocation.place_b);
        this.userLocation = clocation;
    };
    HomePage.prototype.addMarker = function (lat, lng, st_location) {
        var infowindow = new google.maps.InfoWindow();
        var marker;
        var that = this;
        var setText = "";
        var img_url = "";
        setText = "Your location";
        img_url = "assets/images/pickup.png";
        var latLng = new google.maps.LatLng(lat, lng);
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            map: this.map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            icon: {
                url: img_url,
                size: {
                    width: 48,
                    height: 48
                }
            }
        });
        this.gmarkers.push(marker);
        this.map.setZoom(17);
        this.map.setCenter(latLng);
        google.maps.event.addListener(marker, 'dragend', function (evt) {
            that.getPosition(marker.getPosition());
            var glat = evt.latLng.lat().toFixed(8);
            var glong = evt.latLng.lng().toFixed(8);
            localStorage.currentLat = glat;
            localStorage.currentLng = glong;
        });
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent("<div style='padding:8px;'><p>Move Your location   </p><a href='http://maps.google.com/maps?q=" + localStorage.currentLat + "," + localStorage.currentLng + "'>Open Google maps</a></div>");
                // infowindow.setContent("<div style='padding:8px;'><p>Move Your location   </p><a href='maps:" + localStorage.currentLat + "," + localStorage.currentLng + "?q=" + localStorage.currentLat + "," + localStorage.currentLng + "'>Open Google maps</a></div>"); 
                infowindow.open(this.map, marker);
            };
        })(marker, setText));
    };
    HomePage.prototype.getPosition = function (pos) {
        var _this = this;
        var that = this;
        var geocoder = new google.maps.Geocoder;
        geocoder.geocode({ latLng: pos }, function (results, status) {
            //console.log(status);
            // console.log(results);
            var content = 'Your location : ' + results[0].formatted_address;
            _this.currentlocation = results[0].address_components[0].long_name + ' ' + results[0].address_components[1].long_name;
            _this.currentlocation_b = results[0].formatted_address;
            localStorage.currentPlaceA = results[0].address_components[0].long_name + ' ' + results[0].address_components[1].long_name;
            localStorage.currentPlaceB = results[0].formatted_address;
            // that.presentToast(content);
        });
    };
    HomePage.prototype.getMapCenter = function () {
        return this.map.getCenter();
    };
    HomePage.prototype.SetCurrentLocation = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            spinner: 'circles',
            cssClass: 'transparent'
        });
        loader.present();
        //this.gmap.clear();
        this.gmap.getMyLocation()
            .then(function (location) {
            // console.log(JSON.stringify(location, null ,2));
            localStorage.currentLat = location.latLng.lat;
            localStorage.currentLng = location.latLng.lng;
            // let clocation = {
            //   lat: location.latLng.lat,
            //   lng: location.latLng.lng
            // };
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({ 'location': location.latLng }, function (results, status) {
                //console.log(results); 
                localStorage.currentPlaceA = results[0].address_components[0].short_name + ' ' + results[0].address_components[1].short_name;
                localStorage.currentPlaceB = results[0].formatted_address;
            });
            _this.pAccount.UserLoginApp().then(function (r) {
                //  console.log(r);  
            });
            _this.addCurrentLoc();
            loader.dismiss();
        }).catch(function (error) {
            //alert('Error getting location');
            console.log(error);
            loader.dismiss();
        });
    };
    HomePage.prototype.geocodePosition = function (clocation) {
        alert(clocation);
    };
    HomePage.prototype.actionbtn = function () {
        var _this = this;
        if (this.showVote) {
            this.showVote = false;
            this.hide_view_Vote = true;
        }
        if (!this.searchState) {
            this.showSetC = false;
            this.directionsDisplay.setDirections({ routes: [] });
            this.directionsDisplay.setMap(null);
            this.initMap();
            this.tollWay = true;
            this.Set_Tollway = "Tollway";
            this.icon_tollway = "custom-tollways";
            this.icon_tollway_color = "bBlue";
            this.icon_text_color = "sp_color_a";
            var clocation = {
                lat: localStorage.currentLat,
                lng: localStorage.currentLng
            };
            var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__home_search_home_search__["a" /* HomeSearchPage */], {
                location_a: this.currentlocation,
                location_b: this.currentlocation_b,
                userlocation: clocation
            });
            profileModal.onDidDismiss(function (data) {
                if (data != null) {
                    _this.locationLabel = data.label;
                    _this.locationRequest.ipStart = data.select.origin.lat + "," + data.select.origin.lng;
                    _this.locationRequest.ipEnd = data.select.desti.lat + "," + data.select.desti.lng;
                    _this.dataReq.start_location = data.label.origin_a;
                    _this.dataReq.destination = data.label.destination_a;
                    _this.dataReq.expressway = "1";
                    _this.openSearchState();
                    _this.calculateAndDisplayRoute();
                }
                else {
                    _this.clearSearchState();
                    _this.clean();
                }
            });
            profileModal.present().then(function () { });
        }
        else {
            this.clearSearchState();
            this.clean();
        }
    };
    HomePage.prototype.clean = function () {
        this.showSetC = true;
        this.dataReq.start_location_lat = "";
        this.dataReq.start_location_long = "";
        this.dataReq.start_location = "";
        this.dataReq.destination_lat = "";
        this.dataReq.destination_long = "";
        this.dataReq.destination = "";
        this.dataReq.car_type = "";
        this.dataReq.id_payment = "";
        this.dataReq.fare = "";
        this.dataReq.note = "";
        this.dataReq.expressway = "1";
        this.directionsDisplay.setDirections({ routes: [] });
        this.directionsDisplay.setMap(null);
        this.initMap();
        this.addCurrentLoc();
        // let directionsDisplay = new google.maps.DirectionsRenderer;
        // directionsDisplay.set('directions', null);   
    };
    HomePage.prototype.openSearchState = function () {
        this.searchState = true;
    };
    HomePage.prototype.clearSearchState = function () {
        this.searchState = false;
    };
    HomePage.prototype.hidden_book_car = function () {
        this.book_car_hidden = !this.book_car_hidden;
        if (this.book_car_hidden) {
            this.icon_name_v = "ios-arrow-dropup-outline";
        }
        else {
            this.icon_name_v = "ios-arrow-dropdown-outline";
        }
    };
    /*
  {
    origin: LatLng | String | google.maps.Place,
    destination: LatLng | String | google.maps.Place,
    travelMode: TravelMode,
    transitOptions: TransitOptions,
    drivingOptions: DrivingOptions,
    unitSystem: UnitSystem,
    waypoints[]: DirectionsWaypoint,
    optimizeWaypoints: Boolean,
    provideRouteAlternatives: Boolean,
    avoidFerries: Boolean,
    avoidHighways: Boolean,
    avoidTolls: Boolean,
    region: String
  }
    */
    HomePage.prototype.calculateAndDisplayRoute = function () {
        //console.log(this.locationRequest)
        var _this = this;
        var loader = this.loadingCtrl.create({
            spinner: 'circles',
            cssClass: 'transparent'
        });
        loader.present();
        //console.log(this.tollWay);
        if (this.tollWay) {
            this.icon_tollway = "custom-tollways";
            this.icon_tollway_color = "bBlue";
            this.icon_text_color = "sp_color_a";
        }
        else {
            this.icon_tollway = "custom-tollways";
            this.icon_tollway_color = "dark";
            this.icon_text_color = "sp_color_b";
        }
        var setObj = {
            origin: this.locationRequest.ipStart,
            destination: this.locationRequest.ipEnd,
            travelMode: 'DRIVING',
            avoidTolls: !this.tollWay
        };
        this.directionsService.route(setObj, function (response, status) {
            if (status === 'OK') {
                //console.log(response);  
                //console.log(this.directionsDisplay);
                _this.directionsDisplay.setDirections(response);
                _this.locationRequest.ipStartDetail = response.routes[0].legs[0].start_address;
                _this.locationRequest.ipEndDetail = response.routes[0].legs[0].end_address;
                _this.locationRequest.ipDistance = response.routes[0].legs[0].distance.text;
                _this.locationRequest.ipDuration = _this.GetTextTimes((response.routes[0].legs[0].duration.value / 60));
                //response.routes[0].legs[0].duration.text;  
                _this.locationRequest.ipDistance_v = (response.routes[0].legs[0].distance.value / 1000) + "";
                _this.locationRequest.ipDuration_v = (response.routes[0].legs[0].duration.value / 60) + "";
                _this.routesLeg = response.routes[0].legs[0];
                _this.getPaymentList();
                _this.getOrderCalculate();
                _this.Driving();
                setTimeout(function (x) {
                    loader.dismiss();
                }, 1000);
            }
            else {
                // window.alert('Directions request failed due to ' + status);
            }
        });
    };
    HomePage.prototype.GetTextTimes = function (vDuration) {
        var txt = "";
        txt = parseInt(vDuration) + " - " + parseInt(vDuration + 12) + " นาที";
        return txt;
    };
    HomePage.prototype.ChangeTollway = function () {
        var _this = this;
        // Disable Tollway or Enable Tollway
        var text_set = "Enable Tollway";
        if (this.tollWay) {
            text_set = "Disable Tollway";
        }
        var set_Popup = {
            title: "Confirm Changed avoid Tollway",
            text_message: text_set,
            btn_cancel: "Cancel",
            btn_confirm: "Confirm",
            isCancel: true
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            // console.log(popup);  
            if (popup) {
                _this.SetTollway();
            }
        });
        popModal.present();
    };
    HomePage.prototype.SetTollway = function () {
        this.tollWay = !this.tollWay;
        if (this.tollWay) {
            this.Set_Tollway = "Tollway";
            this.icon_tollway = "custom-tollways";
            this.icon_tollway_color = "bBlue";
            this.icon_text_color = "sp_color_a";
            this.dataReq.expressway = "1";
        }
        else {
            this.Set_Tollway = "Tollway";
            this.icon_tollway = "custom-tollways";
            this.icon_tollway_color = "dark";
            this.icon_text_color = "sp_color_b";
            this.dataReq.expressway = "";
        }
        this.directionsDisplay.setDirections({ routes: [] });
        this.directionsDisplay.setMap(null);
        this.initMap();
        this.calculateAndDisplayRoute();
    };
    HomePage.prototype.getOrderCalculate = function () {
        var _this = this;
        this.pOrder.OrderCalculate(this.locationRequest.ipDuration_v, this.locationRequest.ipDistance_v).then(function (r) {
            //console.log(r);
            _this.OrderCalList.name = r.data[0].name;
            _this.OrderCalList.icon = r.data[0].icon;
            _this.OrderCalList.fare = r.data[0].fare;
            _this.dataReq.car_type = r.data[0].id;
            _this.dataReq.fare = r.data[0].fare;
        });
    };
    HomePage.prototype.getPaymentList = function () {
        var _this = this;
        this.pAccount.UserPaymentListing().then(function (r) {
            if (r.status == 200 && r.message == "Success") {
                // console.log(r.data);
                if (r.data.length > 0) {
                    if (r.data[0].card_number == 'Cash') {
                        _this.Payment = r.data[0].card_number;
                    }
                    else {
                        _this.Payment = r.data[0].card_number.substr(r.data[0].card_number.length - 5);
                    }
                    _this.dataReq.id_payment = r.data[0].id;
                }
            }
        });
    };
    HomePage.prototype.ChangePlace = function () {
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__modal_place_modal_place__["a" /* SelectPlace */]);
        popModal.onDidDismiss(function (selectPlace) {
            //console.log(selectPlace); 
            if (selectPlace != null) {
            }
        });
        popModal.present();
    };
    HomePage.prototype.ChangePayment = function () {
        // let profileModal = this.modalCtrl.create(PaymentPage);
        // profileModal.onDidDismiss(selectPayment => {
        //   console.log(selectPayment);
        // })
        // profileModal.present();
        var _this = this;
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__modal_set_card_modal_select_payment__["a" /* SelectPayment */], { id_payment: this.dataReq.id_payment });
        popModal.onDidDismiss(function (selectPayment) {
            // console.log(selectPayment);
            if (selectPayment != null) {
                if (selectPayment.card_number != 'Cash') {
                    _this.Payment = selectPayment.card_number.substr(selectPayment.card_number.length - 5);
                }
                else {
                    _this.Payment = selectPayment.card_number;
                }
                _this.dataReq.id_payment = selectPayment.id;
            }
        });
        popModal.present();
    };
    HomePage.prototype.ChangeCars = function () {
        var _this = this;
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__modal_cars_modal_cars__["a" /* SelectCars */], {
            Duration: this.locationRequest.ipDuration_v,
            Distance: this.locationRequest.ipDistance_v
        });
        popModal.onDidDismiss(function (selectCars) {
            //console.log(selectCars); 
            if (selectCars != null) {
                _this.OrderCalList.name = selectCars.name;
                _this.OrderCalList.icon = selectCars.icon;
                _this.OrderCalList.fare = selectCars.fare;
                _this.dataReq.car_type = selectCars.id;
                _this.dataReq.fare = selectCars.fare;
            }
        });
        popModal.present();
    };
    HomePage.prototype.ChangeOption = function () {
        var _this = this;
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__modal_option_modal_option__["a" /* SelectOption */], { text: this.dataReq.note });
        popModal.onDidDismiss(function (selectOption) {
            //console.log(selectOption);
            if (selectOption != null) {
                //alert(selectOption);
                _this.dataReq.note = selectOption;
            }
        });
        popModal.present();
    };
    HomePage.prototype.ChangePromotion = function () {
        // let profileModal = this.modalCtrl.create(PaymentPage);
        // profileModal.onDidDismiss(selectPayment => {
        //   console.log(selectPayment);
        // })
        // profileModal.present();  
    };
    HomePage.prototype.checkPaymentBook = function () {
        console.log(this.dataReq);
        if (this.dataReq.id_payment == "") {
            this.popCheckPaymentBook("Please help to Add payment method");
        }
        else {
            this.BookCar();
        }
    };
    HomePage.prototype.popCheckPaymentBook = function (msg) {
        var _this = this;
        var set_Popup = {
            title: "Pop-up Request",
            text_message: msg,
            btn_cancel: "",
            btn_confirm: "Done",
            isCancel: false
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            console.log(popup);
            _this.clearSearchState();
            _this.clean();
            _this.GotoPaymentPage();
        });
        popModal.present();
    };
    HomePage.prototype.GotoPaymentPage = function () {
        var addCardModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_12__payment_payment__["a" /* PaymentPage */]);
        addCardModal.onDidDismiss(function (data) {
            if (data != null) {
            }
        });
        addCardModal.present();
    };
    HomePage.prototype.BookCar = function () {
        var _this = this;
        this.clearSearchState();
        this.dataReq.start_location_lat = this.routesLeg.start_location.lat();
        this.dataReq.start_location_long = this.routesLeg.start_location.lng();
        //this.dataReq.start_location = this.routesLeg.start_address;
        this.dataReq.destination_lat = this.routesLeg.end_location.lat();
        this.dataReq.destination_long = this.routesLeg.end_location.lng();
        if (this.tollWay) {
            this.dataReq.expressway = "1";
        }
        else {
            this.dataReq.expressway = "0";
        }
        //this.dataReq.destination = this.routesLeg.end_address; 
        //this.dataReq.car_type = "1";
        //this.dataReq.id_payment = "1";
        //this.dataReq.fare = "99";
        //this.dataReq.note = "Limo Booking";  
        //this.dataReq.expressway ="1";
        /*
        var dataReq = {
          start_location_lat: this.routesLeg.start_location.lat(),
          start_location_long: this.routesLeg.start_location.lng(),
          start_location: this.routesLeg.start_address,
          destination_lat: this.routesLeg.end_location.lat(),
          destination_long: this.routesLeg.end_location.lng(),
          destination: this.routesLeg.end_address,
          car_type: "1", // Hard Code
          id_payment: "1", // Hard Code
          fare: "99", // Hard Code
          note: "xxxx"
        };
        */
        //console.log(this.dataReq); 
        this.pOrder.OrderBook(this.dataReq).then(function (r) {
            if (r.status == 200) {
                _this.bookOrder = r.data[0];
                //console.log(this.bookOrder);
                localStorage.setBook = _this.bookOrder;
                //this.directionsDisplay.setMap(null);  
                _this.WaitDriver();
            }
            else {
                _this.openPopAlertMessage(r.message);
            }
        });
        this.bookState = true;
    };
    HomePage.prototype.openPopAlertMessage = function (msg) {
        var set_Popup = {
            title: "Pop-up Request",
            text_message: msg,
            btn_cancel: "",
            btn_confirm: "Done",
            isCancel: false
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            console.log(popup);
        });
        popModal.present();
    };
    HomePage.prototype.openPopAlertMessageGPS = function (msg) {
        var _this = this;
        var set_Popup = {
            title: "Pop-up Request",
            text_message: msg,
            btn_cancel: "",
            btn_confirm: "Done",
            isCancel: false
        };
        var popModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__modal_popup_modal_popup__["a" /* PopupPage */], {
            setPopup: set_Popup
        });
        popModal.onDidDismiss(function (popup) {
            console.log(popup);
            _this.SetCurrentLocation();
        });
        popModal.present();
    };
    HomePage.prototype.presentToast = function (mstr) {
        var toast = this.toastCtrl.create({
            message: mstr,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    HomePage.prototype.WaitDriver = function () {
        var _this = this;
        this.timerInterval = setInterval(function () {
            console.log("runJob: " + _this.runJob);
            if (!_this.runJob) {
                _this.pOrder.OrderGet(_this.bookOrder.id).then(function (r) {
                    if (r.status == 200) {
                        console.log(r.data);
                        _this.bookOrder = r.data[0];
                        //console.log(this.bookOrder); 
                        var dt_order = r.data[0];
                        var dt_user = r.data['user'];
                        var dt_driver = r.data['driver'];
                        var dt_payment = r.data['payment'];
                        localStorage.setBook = _this.bookOrder;
                        // 0 Cancel or Fail, 1 Success, 2 Find Driver, 3 Driving && Waiting For Payment
                        if (_this.bookOrder.status == "3") {
                            //console.log(r.data);  
                            var s_pay = false;
                            if (dt_payment[0].payment_id) {
                                console.log(1);
                                if (dt_payment[0].card_number == "Cash") {
                                    s_pay = true;
                                }
                                else {
                                    if (dt_payment[0].status_payment == "0") {
                                        _this.runJob = false;
                                    }
                                    else if (dt_payment[0].status_payment == "1") {
                                        s_pay = true;
                                    }
                                    else if (dt_payment[0].status_payment == "2") {
                                        _this.runJob = false;
                                        clearInterval(_this.timerInterval);
                                        _this.CancelCar();
                                        _this.showDriver = false;
                                        _this.bookState = false;
                                        _this.directionsDisplay.setDirections({ routes: [] });
                                        _this.directionsDisplay.setMap(null);
                                        _this.showSetC = true;
                                        _this.initMap();
                                        _this.addCurrentLoc();
                                    }
                                    else if (dt_payment[0].status_payment == "3") {
                                        if (!_this.isomise) {
                                            _this.isomise = true;
                                            //console.log(2); 
                                            var url = dt_payment[0].authorize_uri;
                                            //this.launch(url);
                                            _this.runJob = true;
                                            _this.OpenOmise(url);
                                        }
                                        else {
                                            _this.isomise = false;
                                            _this.runJob = false;
                                            clearInterval(_this.timerInterval);
                                            _this.CancelCar();
                                            _this.showDriver = false;
                                            _this.bookState = false;
                                            _this.directionsDisplay.setDirections({ routes: [] });
                                            _this.directionsDisplay.setMap(null);
                                            _this.showSetC = true;
                                            _this.initMap();
                                            _this.addCurrentLoc();
                                        }
                                    }
                                }
                            }
                            if (s_pay) {
                                if (_this.setFirst) {
                                    _this.directionsDisplay.setDirections({ routes: [] });
                                    _this.directionsDisplay.setMap(null);
                                }
                                _this.addPoi(r.data);
                                if (_this.bookOrder.driver_id != "0") {
                                    _this.data_driver.driverId = r.data['driver'].id;
                                    _this.data_driver.driverName = r.data['driver'].name;
                                    if (r.data['driver'].image) {
                                        _this.data_driver.driverImg = r.data['driver'].image;
                                    }
                                    else {
                                        _this.data_driver.driverImg = "assets/images/images.jpg";
                                    }
                                    var dlat = r.data['driver'].lat + "";
                                    var dlng = r.data['driver'].long + "";
                                    var sLoc = _this.bookOrder.start_location;
                                    //console.log( sLoc + "  " + dlat +',' +dlng);
                                    _this.dTimes = "10";
                                    _this.GetPlaceGetDuration(sLoc, dlat, dlng);
                                    _this.data_driver.CarReg = r.data['driver'].car;
                                    _this.data_driver.driverPhone = r.data['driver'].phone;
                                    _this.data_driver.ratings = parseInt(r.data['driver'].rate);
                                    _this.showDriver = true;
                                    _this.bookState = false;
                                }
                            }
                        }
                        else if (_this.bookOrder.status == "1") {
                            _this.runJob = false;
                            clearInterval(_this.timerInterval);
                            _this.showDriver = false;
                            _this.bookState = false;
                            _this.directionsDisplay.setDirections({ routes: [] });
                            _this.directionsDisplay.setMap(null);
                            _this.showSetC = true;
                            _this.initMap();
                            _this.addCurrentLoc();
                            _this.ShowVote();
                        }
                        else if (_this.bookOrder.status == "0") {
                            _this.bookState = false;
                            clearInterval(_this.timerInterval);
                            _this.directionsDisplay.setDirections({ routes: [] });
                            _this.directionsDisplay.setMap(null);
                            _this.showSetC = true;
                            _this.initMap();
                            _this.addCurrentLoc();
                        }
                    }
                });
                //this.clean(); 
                //this.SetCurrentLocation();   
                //this.showDriver =true;  
            }
        }, 10000);
    };
    HomePage.prototype.GetPlaceGetDuration = function (sLoc, dLat, dLong) {
        var _this = this;
        this.pDriver.DriverLoc(dLat, dLong).then(function (r) {
            if (r) {
                console.log(r);
                //let dLoc = r.results[0].address_components[0].short_name;
                var dLoc = r.results[0].formatted_address;
                console.log(dLoc);
                _this.GetDuration(sLoc, dLoc);
            }
        });
    };
    HomePage.prototype.GetDuration = function (sLoc, dLoc) {
        var _this = this;
        console.log(sLoc + ', ' + dLoc);
        var setObj = {
            origin: sLoc,
            destination: dLoc,
            travelMode: 'DRIVING',
            avoidTolls: false
        };
        this.directionsService.route(setObj, function (response, status) {
            if (status === 'OK') {
                console.log(response);
                //this.directionsDisplay.setDirections(response);
                //this.locationRequest.ipStartDetail = response.routes[0].legs[0].start_address;
                //this.locationRequest.ipEndDetail = response.routes[0].legs[0].end_address;
                //this.locationRequest.ipDistance = response.routes[0].legs[0].distance.text;
                //this.dTimes = this.GetTextTimes((response.routes[0].legs[0].duration.value /60));
                //response.routes[0].legs[0].duration.text;  
                //this.locationRequest.ipDistance_v =(response.routes[0].legs[0].distance.value /1000) +"";
                if ((response.routes[0].legs[0].duration.value / 60) > 5) {
                    _this.dTimes = (response.routes[0].legs[0].duration.value / 60).toFixed(0) + "";
                }
                else {
                    _this.dTimes = "5";
                }
                //this.routesLeg = response.routes[0].legs[0];
            }
            else {
                // window.alert('Directions request failed due to ' + status);
                _this.dTimes = "10";
            }
        });
    };
    HomePage.prototype.ShowVote = function () {
        this.hide_view_Vote = false;
        this.showVote = true;
        this.showSetC = false;
        this.VoteN = 0;
        this.textNote = "";
    };
    HomePage.prototype.closeVote = function () {
        this.hide_view_Vote = !this.hide_view_Vote;
    };
    HomePage.prototype.SaveVote = function () {
        this.hide_view_Vote = true;
        this.showVote = false;
        this.showSetC = true;
        var rate = this.VoteText;
        var comment = this.textNote;
        this.pDriver.DriverVote(this.bookOrder.id, rate, comment).then(function (r) {
            if (r.status == 200) {
            }
        });
    };
    HomePage.prototype.OpenOmise = function (url_) {
        var _this = this;
        var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_15__payment_omise_payment_omise__["a" /* PaymentOmisePage */], { url: url_ });
        profileModal.onDidDismiss(function (data) {
            _this.runJob = false;
            _this.pOrder.OrderGet(_this.bookOrder.id).then(function (r) {
                if (r.status == 200) {
                    //console.log(r.data);
                    //this.bookOrder = r.data[0]; 
                    //console.log(this.bookOrder); 
                    //let dt_order = r.data[0];
                    //let dt_user = r.data['user'];
                    //let dt_driver = r.data['driver'];
                    var dt_payment = r.data['payment'];
                    if (dt_payment[0].status_payment != "1") {
                        _this.CancelCar();
                    }
                }
            });
        });
        profileModal.present();
    };
    /*
    WaitDriver()
    {
      this.timerInterval = setInterval(() => {
        if(!this.runJob)
        {
  
          this.pOrder.OrderGet(this.bookOrder.id).then(r => {
            if(r.status == 200)
            {
              this.bookOrder = r.data[0];
             //console.log(this.bookOrder);
             
             localStorage.setBook = this.bookOrder;
              if(this.bookOrder.status =="3")
              {
               
                console.log(r.data);
                if(this.setFirst)
                {
                  this.directionsDisplay.setDirections({routes: []});
                  this.directionsDisplay.setMap(null);
                }
  
                this.addPoi(r.data);
                
                if(this.bookOrder.driver_id!="0")
                {
                  this.data_driver.driverId =  r.data['driver'].id;
                  this.data_driver.driverName =  r.data['driver'].name;
                  this.data_driver.driverImg =  r.data['driver'].image;
                  this.data_driver.CarReg =  r.data['driver'].car;
                  this.data_driver.driverPhone =  r.data['driver'].phone;
                  this.data_driver.ratings =  parseInt(r.data['driver'].rate);
                  this.showDriver =true;
                  this.bookState = false;
                }
              }
              else if(this.bookOrder.status =="1")
              {
                this.runJob =false;
                clearInterval(this.timerInterval);
                this.showDriver =false;
                this.bookState = false;
                this.directionsDisplay.setDirections({routes: []});
                this.directionsDisplay.setMap(null);
                this.showSetC =true;
                this.initMap();
                this.addCurrentLoc();
              }
            }
          });
          
          //this.clean();
          //this.SetCurrentLocation();
          //this.showDriver =true;
        }
      }, 10000);
    }
  
    */
    HomePage.prototype.addPoi = function (dat) {
        this.removeMarkers();
        this.addLoadPoi(dat['0'].start_location_lat, dat['0'].start_location_long, "Pick-up", dat['0'].start_location);
        this.addLoadPoi(dat['0'].destination_lat, dat['0'].destination_long, "Drop off", dat['0'].destination);
        this.addLoadPoi(dat['driver'].lat, dat['driver'].long, dat['driver'].name, dat['driver'].car);
    };
    HomePage.prototype.resetMapBounds = function () {
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0; i < this.gmarkers.length; i++) {
            bounds.extend(this.gmarkers[i].position);
        }
        this.map.fitBounds(bounds);
        if (this.map.getZoom() > 18) {
            this.map.setZoom(18);
        }
    };
    HomePage.prototype.addLoadPoi = function (lat, lng, setT, st_location) {
        var infowindow = new google.maps.InfoWindow();
        var marker;
        var setText = "";
        var img_url = "";
        if (setT == "Pick-up") {
            setText = "Pick-up";
            img_url = "assets/images/pickup.png";
        }
        else if (setT == "Drop off") {
            setText = "Drop off";
            img_url = "assets/images/drop_off.png";
        }
        else {
            // Driver
            setText = setT + "";
            img_url = "assets/images/taxi.png";
        }
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            map: this.map,
            icon: {
                url: img_url,
                size: {
                    width: 48,
                    height: 48
                }
            }
        });
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent("<div style='padding:8px;'><p>" + setText + "</p><p>" + st_location + "</p>" + "<a href='http://maps.google.com/maps?q=" + lat + "," + lng + "'>Open Google maps</a></div>");
                //infowindow.setContent("<div style='padding:8px;'<p>"+ setText +"</p><p>" + st_location +"</p>" + "<a href='maps:" + lat + "," + lng + "?q=" + lat + "," + lng + "'>Open Google maps</a></div>");   
                infowindow.open(this.map, marker);
            };
        })(marker, setT));
        this.gmarkers.push(marker);
        this.bounds.push(marker.position);
        // show map, open infoBox 
        // google.maps.event.addListenerOnce(this.map, 'tilesloaded', function() {
        //   infowindow.setContent("<p>"+ setText +"</p><p>" + st_location +"</p>" + "");
        //   infowindow.open(this.map, marker);
        // });
        // let clocation = {
        //   lat: lat,
        //   lng: lng
        // };
        // let geocoder = new google.maps.Geocoder;
        // geocoder.geocode({ 'location': clocation }, (results, status) => { 
        //     let contentText =  results[1].formatted_address;
        // });
        if (setT != "Pick-up" && setT != "Drop off") {
            var latLng = new google.maps.LatLng(lat, lng);
            if (this.setFirst) {
                this.setFirst = false;
                this.resetMapBounds();
            }
        }
    };
    HomePage.prototype.viewDriver = function () {
        this.view_Driver = !this.view_Driver;
    };
    HomePage.prototype.calling = function () {
        //console.log(this.number); 
        this.telTo(this.data_driver.driverPhone);
    };
    HomePage.prototype.telTo = function (numb) {
        this.callNumber.callNumber(numb, true);
    };
    HomePage.prototype.telToII = function (numb) {
        window.open("tel:" + numb, '_system');
    };
    HomePage.prototype.removeMarkers = function () {
        for (var i = 0; i < this.gmarkers.length; i++) {
            this.gmarkers[i].setMap(null);
        }
        this.gmarkers = [];
        this.bounds = [];
    };
    HomePage.prototype.Driving = function () {
        var _this = this;
        this.pDriver.DriverDriving().then(function (r) {
            _this.removeMarkers();
            if (r.status == 200) {
                //console.log(r.data); 
                //var infowindow = new google.maps.InfoWindow(); 
                var marker, i;
                if (r.data.length > 0) {
                    for (i = 0; i < r.data.length; i++) {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(r.data[i].lat, r.data[i].long),
                            map: _this.map,
                            icon: {
                                url: "assets/images/taxi.png",
                                size: {
                                    width: 48,
                                    height: 48
                                }
                            }
                        });
                        _this.gmarkers.push(marker);
                        // google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        //   return function() {
                        //     infowindow.setContent("<p>"+ r.data[i].start_location + "-" + r.data[i].destination + "  THB " + r.data[i].fare +"</p>" +
                        //       "Note: " + r.data[i].note
                        //     );
                        //     infowindow.open(this.map, marker);
                        //   }
                        // })(marker, i));
                    }
                }
            }
        });
    };
    HomePage.prototype.CancelCar = function () {
        var _this = this;
        this.pOrder.OrderCancel(this.bookOrder.user_id, this.bookOrder.id).then(function (r) {
            _this.bookState = false;
            _this.directionsDisplay.setDirections({ routes: [] });
            _this.directionsDisplay.setMap(null);
            _this.showSetC = true;
            _this.initMap();
            _this.addCurrentLoc();
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], HomePage.prototype, "mapElement", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/home/home.html"*/'<ion-header no-border>\n  <ion-navbar transparent> \n      <button ion-button menuToggle>\n          <ion-icon name="menu" style="color: #666"></ion-icon>\n        </button> \n  </ion-navbar> \n</ion-header>\n\n<ion-content>\n \n  \n  <!-- <div class="menu-toggle-panel" [class.hide]="bookState">\n    <button ion-button menuToggle class="btn-menuToggle">\n      <ion-icon name="menu"></ion-icon>\n    </button> \n  </div> -->\n\n  <div class="where-go-panel" [class.hide]="showDriver">\n    <button *ngIf="MemberType==\'0\'" ion-button icon-start (click)="actionbtn()" [class.hide]="bookState">\n      <img src="assets/images/driver.png" />\n      <span *ngIf="!searchState">Go every where ?</span>\n      <span *ngIf="searchState">Clear route</span>\n    </button>\n\n    <!-- \n      <div class="input-to-go-panel" [class.hide]="!searchState">\n      <div class="link-line"></div>\n      <div class="input-location">\n        <div class="iconpanel">\n          <img src="assets/images/icon-origin.png" />\n        </div>\n        <div class="inputpanel">\n          <ion-label>{{locationLabel.origin}}</ion-label>\n          <p>{{locationRequest.ipStartDetail}}</p>\n        </div>\n      </div>\n\n      <div class="input-location">\n        <div class="iconpanel">\n          <img src="assets/images/icon-destination.png" />\n        </div>\n        <div class="inputpanel">\n          <ion-label>{{locationLabel.destination}}</ion-label>\n          <p>{{locationRequest.ipEndDetail}}</p>\n        </div>\n      </div>\n    </div> \n  -->\n\n  </div>\n  <div #map id="map"></div>\n  <div id="map_canvas2" style="width: 100px; height: 100px;display: none;"></div> \n\n  <div class="panel-book-car" [class.hide]="!searchState"> \n    <div> \n        <ion-icon [name]="icon_name_v" item-end (click)="hidden_book_car()"></ion-icon>  \n    </div>\n    <ion-card [hidden]="book_car_hidden">\n      <ion-card-header>\n        BOOKING\n        <small>({{locationRequest.ipDistance}})</small>\n      </ion-card-header>\n      <ion-card-content>\n        <ion-item (click)="ChangeCars()">\n          <img [src]="OrderCalList.icon" /> {{OrderCalList.name}}\n          <div class="trip-detail">\n            <p>THB {{OrderCalList.fare}}</p>\n            <h6>{{locationRequest.ipDuration}}</h6>\n          </div>\n        </ion-item>\n      </ion-card-content>  \n    </ion-card> \n   <br/>\n    <ion-card padding text-center [hidden]="book_car_hidden">\n      <ion-grid>\n          <ion-row no-padding>\n            <ion-col (click)="ChangePayment()">\n              <!-- <img src="assets/images/credit-card.png" />   -->\n              <ion-icon name="ios-card-outline" style="font-size: 30px;"></ion-icon>\n              <p>{{Payment}}</p>\n            </ion-col>\n              <ion-col (click)="ChangeTollway()">\n                <ion-icon [name]="icon_tollway" [color]="icon_tollway_color" style="font-size: 30px;"></ion-icon>\n                <br/> <span class="{{icon_text_color}}">{{Set_Tollway}}</span>\n              </ion-col> \n            <!-- <ion-col (click)="ChangePlace()">\n                <ion-icon name="ios-map-outline" style="font-size: 30px;"></ion-icon>\n                <br/> Palce\n              </ion-col> -->\n            <ion-col (click)="ChangeOption()">\n              <ion-icon name="ios-more-outline" style="font-size: 30px;"></ion-icon>\n              <br/> Option\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n        <!-- <hr/>\n        <div>\n          THB 80 - 120\n        </div> -->\n    </ion-card> \n    <br/>\n    <ion-card padding text-center [hidden]="book_car_hidden">\n        <h6>By clicking "Book" I have read</h6> \n        <h6>agree to<span class="cBlue" (click)="viewTerm()"> term & condition </span></h6>  \n    </ion-card > \n\n    <div padding class="book-action">\n      <button ion-button full color="primary" (click)="checkPaymentBook()">Book</button>\n    </div>\n  </div>\n\n  <div class="panel-find-car" [class.hide]="!bookState">\n    <ion-card>\n      <ion-card-header>\n        Finding you a nearby premium driver...\n      </ion-card-header>\n      <div class="line stripesLoader" style="background-position:100%;background-color:#ffa500"></div>\n      <ion-card-content>\n        <ion-item class="txt-label">\n          <img src="assets/images/icon-origin.png" /> <p style="font-size: 0.7em">{{locationLabel.origin_a}}</p>\n          <div class="trip-detail">\n              <ion-icon name="ios-card-outline" style="font-size: 30px;"></ion-icon> {{Payment}}\n          </div>\n        </ion-item>\n        <ion-item class="txt-label">\n          <img src="assets/images/icon-destination.png" /> <p style="font-size: 0.7em">{{locationLabel.destination_a}}</p>\n          <div class="trip-detail">\n            <h6>THB {{OrderCalList.fare}}</h6>\n          </div>\n        </ion-item>\n      </ion-card-content>\n      <button class="btn-cancel" ion-button full (click)="CancelCar()">Cancel</button>\n    </ion-card>\n  </div>\n\n   <div class="drive-with-us" *ngIf="MemberType==\'9\' && !searchState && !bookState">\n    <div>\n      <h6>Drive with Us</h6>  \n      <p>Join us to meet premium passenger Lorem ipsum dolor sit amet, consectetur</p> \n      <ion-item style="height: 30px;" no-margin text-center no-lines><h6>Try Now</h6></ion-item>\n    </div> \n  </div>  \n<ion-fab middle right *ngIf="showDriver">\n      <button ion-fab (click)="viewDriver()"><ion-icon name="ios-car-outline"></ion-icon></button>\n      </ion-fab>\n  <div class="panel-show-driving" *ngIf="showDriver" [hidden]="view_Driver"> \n    <ion-card>\n      <ion-card-header>\n          <span>รถจะมารับภายใน  {{dTimes}}  นาที</span>\n      </ion-card-header>   \n      <ion-grid text-center> \n          <ion-row no-padding style="text-align: center;">\n              <ion-col style="text-align: center;"> \n                  <img class="circle-pic" src="{{data_driver.driverImg}}" > \n              </ion-col>\n          </ion-row>\n          <ion-row no-padding>\n              <ion-col>  \n                  <p style="font-size: 1.4em">{{data_driver.driverName}}</p> \n              </ion-col>\n          </ion-row> \n          <ion-row no-padding> \n              <ion-col>   \n                    <ion-icon [name]="getIconName(num)" *ngFor="let num of [0,1,2,3,4]" [ngStyle]="{\'color\': getColor(num)}" style="font-size: 32px; padding: 4px;"></ion-icon>\n              </ion-col>\n          </ion-row>\n          <ion-row no-padding>\n              <ion-col>  \n                  <p style="font-size: 1.4em">{{data_driver.CarReg}}</p> \n              </ion-col>\n          </ion-row> \n          <ion-row no-padding>\n            <ion-col (click)="calling()"> \n              <ion-icon name="ios-call-outline" style="font-size: 44px;"></ion-icon> \n            </ion-col>  \n          </ion-row>\n        </ion-grid> \n    </ion-card>\n  </div>\n  <ion-fab middle right *ngIf="showVote">\n    <button ion-fab (click)="closeVote()"><ion-icon name="ios-car-outline"></ion-icon></button>\n    </ion-fab>\n    <div class="panel-show-driving" *ngIf="showVote" [hidden]="hide_view_Vote"> \n      <ion-card>\n        <ion-card-header>\n            <!-- <span>10 mins arrived</span> -->\n        </ion-card-header>   \n        <ion-grid text-center> \n            <ion-row no-padding style="text-align: center;">\n                <ion-col style="text-align: center;"> \n                    <img class="circle-pic" src="{{data_driver.driverImg}}" > \n                </ion-col>\n            </ion-row>\n            <ion-row no-padding>\n                <ion-col>  \n                    <p style="font-size: 1.4em">{{data_driver.driverName}}</p> \n                </ion-col>\n            </ion-row> \n            <ion-row no-padding> \n                <ion-col>   \n                      <ion-icon [name]="getIconVote(num)" *ngFor="let num of [0,1,2,3,4]" [ngStyle]="{\'color\': getColor(num)}" (click)="setColor(num)" style="font-size: 32px; padding: 4px;"></ion-icon>\n                </ion-col>\n            </ion-row>\n            <ion-row no-padding>\n                <ion-col>  \n                  <ion-textarea  style="padding: 10px; width: 100%; height: 100px;border-radius: 6px; border: 1px solid #999;" no-margin text-wrap rows="5"  [(ngModel)]="textNote"  placeholder="Comment..." full ></ion-textarea>\n                </ion-col>\n            </ion-row> \n            <ion-row no-padding>\n              <ion-col> \n                <button ion-button full color="primary" style="height: 50px;font-size: 1em;" (click)="SaveVote()">Vote</button>\n              </ion-col>  \n            </ion-row>\n          </ion-grid> \n      </ion-card>\n    </div>\n  <ion-fab right bottom *ngIf="showSetC">\n      <button ion-fab color="trnsp" (click)="SetCurrentLocation()"><ion-icon name="ios-locate-outline" color="dark"></ion-icon></button>\n  </ion-fab>\n</ion-content>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_driver_driver__["a" /* DriverProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_8__providers_account_account__["a" /* AccountProvider */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_call_number__["a" /* CallNumber */],
            __WEBPACK_IMPORTED_MODULE_3__providers_order_order__["a" /* OrderProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 38:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestApiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Declaration__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ts_md5_dist_md5__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ts_md5_dist_md5___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ts_md5_dist_md5__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RestApiProvider = /** @class */ (function () {
    function RestApiProvider(http, globalDeclaration) {
        this.http = http;
        this.globalDeclaration = globalDeclaration;
        this.apiUrl = 'https://limoapp.me/api';
        this.monthN = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
    }
    // GetRequest(): Promise<any>{
    //   var urls = "https://limoapp.me/dapter/test.php";
    //   return this.http.get(urls).toPromise()
    //   .then(this.extractData)
    //   .catch(this.handleError);
    // }
    RestApiProvider.prototype.GetRequestUrl = function (url) {
        return this.http.get(url).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    RestApiProvider.prototype.PostRequest = function (url, obj) {
        // let curr = new Date(); 
        //let toDay = new Date(curr.setDate(curr.getDate())); 
        //let cHH = toDay.getHours();
        //let sYYYYmmDDhh = toDay.getFullYear() + this.monthN[toDay.getMonth()] + ("0" + toDay.getDate().toString()).slice(-2) + ("0" + toDay.getHours().toString()).slice(-2);
        // console.log(sYYYYmmDDhh);
        //let hash =Md5.hashStr('LIM@:'+ sYYYYmmDDhh);
        var hash = __WEBPACK_IMPORTED_MODULE_5_ts_md5_dist_md5__["Md5"].hashStr('LIM@:*&007');
        // "5123e556353d10bf5f459f8e642f8d65";
        //let hash =Md5.hashStr('LIM@:2019030612');
        obj["X-Api-Key"] = hash;
        //"8b065c6ec3bc280438250a21e5e2aa63"; 
        var _objX = JSON.stringify(obj);
        //var _lUrl = this.dapterUrl;
        var _lUrl = this.apiUrl + url;
        // console.log("URL : " + _lUrl);
        // console.log("REQUEST OBJ : ",_objX);
        return this.http.post(_lUrl, _objX, this.ApiHeader()).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    RestApiProvider.prototype.ApiHeader = function () {
        var _header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
        //_header.set('Content-Type', 'application/json');
        //_header.set('Authorization', '8b065c6ec3bc280438250a21e5e2aa63');
        var _option = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
            headers: _header
        });
        return _option;
    };
    RestApiProvider.prototype.CastObjectParam = function (obj) {
        var _urlParam = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* URLSearchParams */]();
        Object.keys(obj).forEach(function (k) {
            _urlParam.append(k, obj[k]);
        });
        return _urlParam;
    };
    RestApiProvider.prototype.extractData = function (res) {
        var _res = res.json() || {};
        // console.log("Response : ",_res);
        return _res;
    };
    RestApiProvider.prototype.handleError = function (res) {
        console.log("Request Error : ", res);
        return Promise.reject(res.message || res);
    };
    RestApiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_4__Declaration__["a" /* GlobalDeclaration */]])
    ], RestApiProvider);
    return RestApiProvider;
}());

//# sourceMappingURL=rest-api.js.map

/***/ }),

/***/ 39:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PageApiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__rest_api_rest_api__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PageApiProvider = /** @class */ (function () {
    function PageApiProvider(http) {
        this.http = http;
    }
    PageApiProvider.prototype.PageApiGet = function (sslug) {
        var obj = {
            "slug": sslug,
        };
        return this.http.PostRequest("/page/get", obj);
    };
    PageApiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__rest_api_rest_api__["a" /* RestApiProvider */]])
    ], PageApiProvider);
    return PageApiProvider;
}());

//# sourceMappingURL=page-api.js.map

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__rest_api_rest_api__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OrderProvider = /** @class */ (function () {
    function OrderProvider(http) {
        this.http = http;
    }
    OrderProvider.prototype.getUserId = function () {
        return localStorage.userId;
    };
    OrderProvider.prototype.OrderBook = function (data) {
        var obj = {
            "id": this.getUserId(),
            "start_location_lat": data.start_location_lat,
            "start_location_long": data.start_location_long,
            "start_location": data.start_location,
            "destination_lat": data.destination_lat,
            "destination_long": data.destination_long,
            "destination": data.destination,
            "car_type": data.car_type,
            "id_payment": data.id_payment,
            "fare": data.fare,
            "note": data.note,
            "expressway": data.expressway,
        };
        return this.http.PostRequest("/order/book", obj);
    };
    OrderProvider.prototype.OrderCancel = function (userId, orderId) {
        var obj = {
            "id": userId,
            "order_id": orderId,
        };
        return this.http.PostRequest("/order/cancel", obj);
    };
    OrderProvider.prototype.OrderHistory = function (ofset, limit) {
        var obj = {
            "id": this.getUserId(),
            "_offset": ofset,
            "_limit": limit
        };
        return this.http.PostRequest("/order/history", obj);
    };
    OrderProvider.prototype.OrderGet = function (id_history) {
        var obj = {
            "id": this.getUserId(),
            "id_history": id_history,
        };
        return this.http.PostRequest("/order/get", obj);
    };
    OrderProvider.prototype.OrderGetIds = function (id_, id_history) {
        var obj = {
            "id": id_,
            "id_history": id_history,
        };
        return this.http.PostRequest("/order/get", obj);
    };
    OrderProvider.prototype.OrderCalculate = function (time, km) {
        var obj = {
            "time": time,
            "km": km
        };
        return this.http.PostRequest("/order/calculate", obj);
    };
    OrderProvider.prototype.DriverDriving = function () {
        return this.http.PostRequest("/driver/driving", {});
    };
    OrderProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__rest_api_rest_api__["a" /* RestApiProvider */]])
    ], OrderProvider);
    return OrderProvider;
}());

//# sourceMappingURL=order.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthenticationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_account_account__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__phone_input_phone_input__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AuthenticationPage = /** @class */ (function () {
    function AuthenticationPage(navCtrl, navParams, platform, statusBar, accountProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.platform = platform;
        this.accountProvider = accountProvider;
        this.authenViewModel = {
            phoneNumber: ""
        };
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need. 
            //statusBar.styleDefault(); 
        });
    }
    AuthenticationPage.prototype.initializeApp = function () {
    };
    AuthenticationPage.prototype.afterAuthen = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    AuthenticationPage.prototype.GotoInputPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__phone_input_phone_input__["a" /* PhoneInputPage */]);
    };
    AuthenticationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-authentication',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/authentication/authentication.html"*/'<!--\n  Generated template for the AuthenticationPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>authentication</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n\n<ion-content class="ion-content">\n\n  <div class="logo-panel">\n    <img src="assets/images/Logo.png"/>\n    <div>\n      Welcome to Premium<br/>Travel Application\n    </div>\n    \n  </div>\n \n\n  <div class="btn-panel">\n    \n      Continue with..\n      <br>\n       <!-- \n       <button ion-button color="facebook" (click)="facebookLogin()">\n        <ion-icon name="logo-facebook"></ion-icon>\n        Facebook</button> \n      -->\n      \n    <button ion-button color="light" (click)="GotoInputPage()">Register</button>\n   \n    <button ion-button color="primary" class="btn-ligth" (click)="GotoInputPage()">Or your phone number</button>\n    \n  </div>\n \n\n  <!-- <form (ngSubmit)="Authentication()">\n    <ion-list>\n      <ion-item>\n        <ion-label floating>Phone</ion-label>\n        <ion-input type="text" [(ngModel)]="authenViewModel.phoneNumber" name="phoneNumber"></ion-input>\n      </ion-item>\n \n    </ion-list>\n    <button ion-button>Login</button>\n  </form>  -->\n  <!-- <button menuClose ion-item (click)="afterAuthen()">Login</button> -->\n</ion-content>\n'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/authentication/authentication.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__providers_account_account__["a" /* AccountProvider */]])
    ], AuthenticationPage);
    return AuthenticationPage;
}());

//# sourceMappingURL=authentication.js.map

/***/ }),

/***/ 58:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TermPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_page_api_page_api__ = __webpack_require__(39);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the YourTripPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TermPage = /** @class */ (function () {
    function TermPage(navCtrl, navParams, pget, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pget = pget;
        this.viewCtrl = viewCtrl;
    }
    TermPage.prototype.ionViewDidLoad = function () {
        // legal, term, refund
        var _this = this;
        var ttype = "term";
        if (localStorage.memberType == "2") {
            ttype = "term_driver";
        }
        this.pget.PageApiGet(ttype).then(function (r) {
            if (r.status == 200 && r.message == "Success") {
                _this.term_html = r.data.content;
            }
        });
    };
    TermPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    TermPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-term',template:/*ion-inline-start:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/term/term.html"*/'<!--\n  Generated template for the YourTripPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header> \n<!-- <ion-navbar>\n        <ion-title>TERM & CONDITION\n        </ion-title> \n</ion-navbar> -->\n</ion-header>\n\n\n<ion-content>\n\n  <div class="panel-top">\n     <h2>TERM & CONDITION</h2> \n  </div> \n  <div class="horizontalline"></div> \n\n  <!-- \n      <ion-fab right top>\n        <ion-icon name="close" color="primary" style="font-size: 4em;margin-right: 20px;"\n        (click)="dismiss()"></ion-icon>\n    </ion-fab>\n   -->\n \n\n    <ion-scroll style="height:80%;" scrollY="true" >\n    <div class="panel-detail-b" [innerHTML]="term_html"> \n    </div>\n    </ion-scroll>\n     \n   <!-- <ion-fab right bottom>\n      <ion-icon name="ios-arrow-round-back-outline" color="cGray" style="font-size: 4em;margin-right: 20px;"\n      (click)="dismiss()"></ion-icon>\n  </ion-fab> -->\n  \n</ion-content> \n\n<ion-footer no-border>\n    <ion-toolbar color="light" >\n        <ion-buttons end>\n            <button ion-button (click)="dismiss()">\n                <ion-icon name="ios-arrow-round-back-outline" color="cGray" style="font-size: 4em;margin-right: 20px;"></ion-icon>\n            </button>\n        </ion-buttons> \n    </ion-toolbar>\n  </ion-footer>'/*ion-inline-end:"/Users/prayuth/ionic3/limo1.36/limo/src/pages/term/term.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_page_api_page_api__["a" /* PageApiProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], TermPage);
    return TermPage;
}());

//# sourceMappingURL=term.js.map

/***/ })

},[239]);
//# sourceMappingURL=main.js.map
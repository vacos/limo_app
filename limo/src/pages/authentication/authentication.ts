import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { HomePage } from '../home/home'; 
import { AccountProvider } from '../../providers/account/account';
import { PhoneInputPage } from '../phone-input/phone-input';  
import { StatusBar } from '@ionic-native/status-bar';

@Component({
  selector: 'page-authentication',
  templateUrl: 'authentication.html',
})
export class AuthenticationPage {

  authenViewModel = {
    phoneNumber: ""
  }

  constructor(public navCtrl: NavController, 
     public navParams: NavParams, 
     public platform: Platform,
     statusBar:StatusBar,
    public accountProvider: AccountProvider) {
      platform.ready().then(() => {
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need. 
        //statusBar.styleDefault(); 
      });
  }
  
  initializeApp() {
    
  }


  afterAuthen() {
    this.navCtrl.setRoot(HomePage);
  }

  GotoInputPage() {
    this.navCtrl.push(PhoneInputPage);
  }
}

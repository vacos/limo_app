import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ViewController } from 'ionic-angular';
import { ValidatePasswordPage } from '../validate-password/validate-password';
import { CreateNewuserPage } from '../create-newuser/create-newuser'; 
import { AccountProvider } from '../../providers/account/account';
 

@Component({
  selector: 'page-otp-update-phone',
  templateUrl: 'otp-update-phone.html',
})
export class OtpUpdatePhonePage {
  @ViewChild('otp1') otp1;
  @ViewChild('otp2') otp2;
  @ViewChild('otp3') otp3;
  @ViewChild('otp4') otp4;
  @ViewChild('ffocus') ffocus;
  
  phoneNumber = "";
  otpForValidate = "";
  maxtime: any = 60;

  ipOTP = {
    ip1: "",
    ip2: "",
    ip3: "",
    ip4: "",
  };

  stateValidate = false;
  timmerTimeup = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController,
    public actionSheetCtrl: ActionSheetController, private pAccount: AccountProvider) {
    this.phoneNumber = this.navParams.get("phone");
    this.otpForValidate = this.navParams.get("code");
  }

  ionViewDidLoad() {
    this.startTimer();
  }

  popPage() {
    this.navCtrl.pop();
  }
  clearAll() {
    this.ipOTP.ip1 = "";
    this.ipOTP.ip2 = "";
    this.ipOTP.ip3 = "";
    this.ipOTP.ip4 = "";
    this.stateValidate = false;
  }

  keyupOtp(ev,i)
  {
    if(i=="1")
    { 
      if(this.ipOTP.ip1 =="")
      { 
       // console.log(ev.keyCode);
      }
      else
      {
        //console.log(ev.target.value);
       // this.ipOTP.ip1 = ev.target.value;
        this.otp2.setFocus();
      }
    }
    else if(i=="2")
    {
      if(this.ipOTP.ip2 =="")
      { 
        if(ev.keyCode ==8){
          this.otp1.setFocus();
        }
      }
      else
      { 
        this.otp3.setFocus();
      }
    }
    else if(i=="3")
    {
      if(this.ipOTP.ip3 =="")
      { 
        if(ev.keyCode ==8){
          this.otp2.setFocus();
        }
      }
      else
      { 
        this.otp4.setFocus();
      }
    }
    else if(i=="4")
    {
      //console.log(this.ipOTP.ip4);
      //console.log(ev.target.value);

      if(this.ipOTP.ip4.length >1){
        this.ipOTP.ip4 = ev.target.value.slice(1);
      }
      else
      {
        if(this.ipOTP.ip4 =="")
        { 
          if(ev.keyCode ==8){
            this.otp3.setFocus();
          }
        }
        else
        { 
          this.nextState();
        }
      }
 
    }

  }

  nextState() {
    if (this.ipOTP.ip1 != "" && this.ipOTP.ip2 != ""
      && this.ipOTP.ip3 != "" && this.ipOTP.ip4 != "") {
      let ipLastOtp = this.ipOTP.ip1 + this.ipOTP.ip2 + this.ipOTP.ip3 + this.ipOTP.ip4;
      if (ipLastOtp == this.otpForValidate) { 

        let userid = localStorage.userId; 
        this.pAccount.UserUpdatePhone({
          id: userid, 
          phone:  this.phoneNumber }).then(r => { 
            if(r.status == 200)
            { 
              var datas = {
                phone: this.phoneNumber   
              }
              this.viewCtrl.dismiss(datas);   
            }  
        });

      } else {
        this.stateValidate = true;
      }
    } else {
      this.stateValidate = true;
    }
  }


  startTimer() {
    setTimeout(x => {
      this.maxtime -= 1;
      if (this.maxtime > 0) {
        this.startTimer();
      }
      if (this.maxtime <= 0) {
        this.timmerTimeup = true;
      }
    }, 1000);
  }
 
  openModal() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Resend to ' + this.phoneNumber,
      cssClass: 'resend-css',
      buttons: [
        {
          text: 'SEND CODE VIA SMS',
          role: 'destructive',
          handler: () => {
            this.pAccount.UserCheck(this.phoneNumber).then(r => {
              this.phoneNumber = r.data.phone;
              this.otpForValidate = r.data.code;
             // console.log("Code : ", this.otpForValidate);
              this.maxtime = 5;
              this.timmerTimeup = false;
              this.startTimer();
            });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });

    actionSheet.present();
  }

  OnClick_ConfirmOTP() {
    //console.log(this.inputOtp);
  }

  OnClick_ExistsUser() {
    this.navCtrl.push(ValidatePasswordPage);
  }

  OnClick_NewMember() {
    this.navCtrl.push(CreateNewuserPage);
  }

  phoneValidator(str) {
    
    let regExp =/^([0]{1})([0-9]{2})([0-9]{3})([0-9]{4})$/;
    //let regExp = /^[0-9]{10}$/;
    return regExp.test(str);
  }

  
}

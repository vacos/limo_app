import { Component, ViewChild, ElementRef } from '@angular/core';
import { ModalController,  LoadingController } from 'ionic-angular'; 
 
import { DriverProvider } from '../../providers/driver/driver';    
import { NativeAudio } from '@ionic-native/native-audio'; 
import { PopupPage } from '../modal-popup/modal-popup';
import { CallNumber } from '@ionic-native/call-number';
import { AccountProvider } from '../../providers/account/account';
import { GoogleMap, MyLocation, GoogleMaps, LatLng, LatLngBounds} from '@ionic-native/google-maps';
import { PageApiProvider } from '../../providers/page-api/page-api';
import { OrderProvider } from '../../providers/order/order';

declare var google;

@Component({
  selector: 'page-home-driver',
  templateUrl: 'home-driver.html'
})
export class HomeDriverPage {
  @ViewChild('map') mapElement: ElementRef;

  btnState: string = "Go Everywhere?";
  map: any;
  gmap: GoogleMap;
  markers: any; 
  gmarkers = []; 

  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;

  MemberType = '2';   
  alert_driver ="";
  user_find: any;  
  timerInterval: number;
  timerIntervalD: number; 
  timerIntervalS: number;

  pActive = false; 
  mActive =false;

  list_user_find : any;
  flag_show =false;
  selectItem ={ 
    id:"",
    user_id: "",
    user_name: "",
    phone:"",
    start_location: "",
    start_location_lat: "",
    start_location_long: "",
    destination: "", 
    destination_lat : "",
    destination_long : "",
    fare: "",
    note: ""
  };

  runJob = false;
  showBox =false;
  driverName =" Driver 02";
  ContactName ="";
  ts = 90000;
  tds = 10000;
    
  constructor(public modalCtrl: ModalController, 
    private pDriver: DriverProvider,
    private pOrder: OrderProvider,
    private pAccount: AccountProvider,
    private pget: PageApiProvider,
    public loadingCtrl : LoadingController, 
    public nativeAudio: NativeAudio,
    private callNumber: CallNumber) {
      this.list_user_find =[];
  }


  ionViewDidLoad() {  

    this.getSlug();
 
    this.MemberType = localStorage.memberType; 
    this.driverName = localStorage.userName;

    this.gmap = GoogleMaps.create('map_canvas3', {}); 
 
    this.initMap();  

    this.addCurrentLoc();  

    this.startTiming(this.ts);

    if(!this.pActive)
    {  
      this.startTimingDriveFind();
    }

    this.nativeAudio.preloadSimple('1', 'assets/sounds/notify.mp3').then(()=>{
      console.log('Playing');
    });

  }

    
  ionViewWillLeave() {
    clearInterval(this.timerInterval); 
    clearInterval(this.timerIntervalD);  
    if(!this.map){
      this.map.remove();
    }
    console.log('leave view');
  }
 

  removeMarkers(){
    for(var i=0; i<this.gmarkers.length; i++){
      this.gmarkers[i].setMap(null);
    } 
    this.gmarkers = []; 
  }

  removeMarkersD(){
    for(var i=0; i<this.gmarkers.length; i++){ 
      console.log(this.gmarkers[i].title);
      if(this.gmarkers[i].title =="Driver")
      {   
        this.gmarkers[i].setMap(null);  
      }
    }  
  }

 
  addCurrentLoc()
  {
    this.removeMarkers(); 
    this.addMarker(localStorage.currentLat, localStorage.currentLng, localStorage.userName, localStorage.currentPlaceB); 
    // this.resetMapBounds();
  }
  
  play()
  {
    this.nativeAudio.play('1').then(()=>{
      console.log('Playing')
    }); 

  }


  GetCurrentLoc() {  

    this.removeMarkersD(); 

    this.gmap.getMyLocation()

    .then((location: MyLocation) => {
 
      // console.log(this.gmarkers);  
      console.log(JSON.stringify(location, null ,2));         
      localStorage.currentLat = location.latLng.lat;  
      localStorage.currentLng = location.latLng.lng;  

       let clocation = {
         lat: location.latLng.lat,
         lng: location.latLng.lng
       };
   
       this.pAccount.UserLoginApp().then(r => {  
         // console.log(r);  
       }); 
 
       let geocoder = new google.maps.Geocoder;
       geocoder.geocode({ 'location': clocation }, (results, status) => { 
          //console.log(results); 
          localStorage.currentPlaceA =results[0].address_components[0].short_name + ' ' + results[0].address_components[1].short_name;
          localStorage.currentPlaceB =results[0].formatted_address;     
          this.addMarker(localStorage.currentLat, localStorage.currentLng, localStorage.userName, localStorage.currentPlaceB); 
          this.resetMapBounds();
       });  
 
    }).catch((error) => {
      //alert('Error getting location');
      console.log(error);
    });  
 
  }

  startTimingDriveFind()
  {
    this.pActive = true; 
    this.timerIntervalD = setInterval(() => {   
       if(!this.runJob)
       {  
         this.DriveFind();
       } 
      this.pActive = false;  
    }, this.tds); 
  }
 
  startTiming(ts)
  { 
    clearInterval(this.timerInterval); 
    this.timerInterval = setInterval(() => {   
      this.GetCurrentLoc();  
    }, ts); 
  }
 

  chooseItem(item)
  {
    for (let i = 0; i < this.list_user_find.length; i++) { 
      this.list_user_find[i].color ="cGray";
    } 
    item.color ="primary"; 

    this.selectItem.id = item.id;
    this.selectItem.user_id = item.user_id; 
    this.selectItem.user_name = item.user_name; 
    this.selectItem.phone = item.phone;
    this.selectItem.start_location = item.start_location; 
    this.selectItem.start_location_lat = item.start_location_lat; 
    this.selectItem.start_location_long = item.start_location_long; 
    this.selectItem.destination = item.destination; 
    this.selectItem.destination_lat = item.destination_lat;
    this.selectItem.destination_long = item.destination_long; 
    this.selectItem.fare = item.fare;
    this.selectItem.note = item.note;  
    this.setPickUp();   

  }


  setPickUp()
  { 
    
    this.removeMarkers();
    
    this.addMarker(this.selectItem.start_location_lat,
      this.selectItem.start_location_long,
      "Pick-up",
      this.selectItem.start_location);

    this.addMarker(this.selectItem.destination_lat,
        this.selectItem.destination_long,
        "Drop off",
        this.selectItem.destination); 

    this.addMarker(localStorage.currentLat,
      localStorage.currentLng,
      localStorage.userName, localStorage.currentPlaceB); 

    this.resetMapBounds();
  
  }


  resetMapBounds(){
    var bounds = new google.maps.LatLngBounds();
    for(var i=0;i<this.gmarkers.length;i++){
        bounds.extend(this.gmarkers[i].position);
    }
    this.map.fitBounds(bounds);  
    if(this.map.getZoom()>17){
      this.map.setZoom(17);
    }
  }

   
  view_Book =false;
  viewBook()
  {
    this.view_Book =!this.view_Book;  

  }

  DriveFind() { 
    
    this.list_user_find =[];
    this.showBox =false;
    this.mActive =false;

    this.pDriver.DriverFind().then(r => {
      if (r.status == 200) {  
        
        //this.user_find = r.data; 
        //console.log(r.data);   
 
        if(r.data.length > 0)
        {
          this.play(); 
      
          this.showBox =true; 
          this.mActive =true;

          for (let i = 0; i < r.data.length; i++) {    

            let ccolor = "cGray"; 

            if(this.selectItem.id !="")
            { 
              if(this.selectItem.id == r.data[i].id)
               {
                ccolor = "primary"; 
               }
            }

            let item = {
              id:r.data[i].id,
              user_id:r.data[i].user_id,
              user_name:r.data[i].name,
              phone:r.data[i].phone,
              start_location: r.data[i].start_location,
              start_location_lat  : r.data[i].start_location_lat,
              start_location_long  : r.data[i].start_location_long,
              destination: r.data[i].destination,
              destination_lat: r.data[i].destination_lat ,
              destination_long: r.data[i].destination_long ,
              km: '10KM',
              fare: r.data[i].fare, 
              note: r.data[i].note,
              color: ccolor ,
              expressway: r.data[i].expressway
             };

             this.list_user_find.push(item);  

             if(this.selectItem.id !="")
             { 
              if(this.selectItem.id == r.data[i].id)
               {
                
                /*

                this.removeMarkers(); 

                this.addMarker(item.start_location_lat,
                  item.start_location_long,
                  "Pick-up",
                  item.start_location);
            
                
                this.addMarker(item.destination_lat,
                  item.destination_long,
                    "Drop off",
                    item.destination); 
            
                this.addMarker(localStorage.currentLat,
                  localStorage.currentLng,
                  localStorage.userName, localStorage.currentPlaceB); 
 
                this.resetMapBounds(); 

               */

               }


             }
  
           }
 
        }
           
      }  

    }); 
  }

 
  addMarker(lat:string,lng:string, setT:string, st_location :string)
  {
    var infowindow = new google.maps.InfoWindow(); 
    var marker;
 
    let setText ="";
    let img_url ="";
    let title ="";

    if(setT == "Pick-up")
    {
      setText ="Pick-up";
      title ="Pick-up";
      img_url ="assets/images/pickup.png";
    }
    else if(setT == "Drop off")
    {
      setText ="Drop off";
      title ="Drop off";
      img_url ="assets/images/drop_off.png";
    }
    else
    {
      setText =setT;
      title ="Driver";
      img_url ="assets/images/taxi.png";
    }

    let latLng = new google.maps.LatLng(lat, lng);

    marker = new google.maps.Marker({
      position: new google.maps.LatLng(lat,lng),
      map: this.map,        
      disableDefaultUI: false,
      title: title,
      //draggable:true,
      //animation: google.maps.Animation.DROP,
      icon: {
        url: img_url,
        size: {
            width: 48,
            height: 48
         }
       }
    });

    // https://maps.google.com/maps?daddr=<lat>,<long>&amp;ll=
    // / infowindow.setContent("<p>"+ setText +"</p><p>" + st_location +"</p>" + "<a href='geo:" + lat + "," + lng + "?z=11'>Open Google maps</a>");
    // ios   maps://?q=LAT,LNG
    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        
         infowindow.setContent("<div style='padding:8px;'><p>"+ setText +"</p><p>" + st_location +"</p>" + "<a href='http://maps.google.com/maps?q=" + lat + "," + lng + "'>Open Google maps</a></div>");
        
        // infowindow.setContent("<div style='padding:8px;'><p>"+ setText +"</p><p>" + st_location +"</p>" + "<a href='maps:" + lat + "," + lng + "?q=" + lat + "," + lng + "'>Open Google maps</a></div>");
        
        infowindow.open(this.map, marker);
      }
    })(marker, setT));

    this.gmarkers.push(marker);  

    //this.map.setZoom(11);
    //this.map.setCenter(latLng); 

    // show map, open infoBox 
    // google.maps.event.addListenerOnce(this.map, 'tilesloaded', function() {
    //   infowindow.setContent("<p>"+ setText +"</p><p>" + st_location +"</p>" + "");
    //   infowindow.open(this.map, marker);
    // });

    // let clocation = {
    //   lat: lat,
    //   lng: lng
    // };

    // let geocoder = new google.maps.Geocoder;
    // geocoder.geocode({ 'location': clocation }, (results, status) => { 
    //     let contentText =  results[1].formatted_address;
    // });
 
  }

  GoDetail()
  {

  }

  getSlug()
  {
    this.pget.PageApiGet("alert_driver").then(r => {
      if(r.status == 200 && r.message == "Success"){ 
        //console.log(r.data.content);
        this.alert_driver= r.data.content;
      } 
    });
  }

  mailto(email) {
    window.open(`mailto:${email}`, '_system');
  }

  telToII(numb) {
    window.open(`tel:${numb}`, '_system');
  }

  telTo(numb) {
    this.callNumber.callNumber(numb, true);
  }

  smsTo(numb) {
    window.open(`sms:${numb}` + '?body=Wellcome from Limo!', '_system');
  }

  calling() {
    //console.log(this.number); 
    this.telTo(this.selectItem.phone);
  }
 
  popMessage() {  
    //console.log(this.number);  
    let set_Popup = { 
      title: "Pop-up Message",
      text_message : this.selectItem.note,
      btn_cancel : "",
      btn_confirm : "Done",
      isCancel : false
    }; 

    
    let popModal = this.modalCtrl.create(PopupPage,{
      setPopup: set_Popup}
      );
    popModal.onDidDismiss(popup => {
        // console.log(popup);   
    })
    popModal.present(); 

  }

  btnPickup =false;
  btnSuccess =true;

  clickPickup() { 
 
    let set_Popup = {
      title: "Confirm Pickup",
      text_message: "รับผู้โดยสาร...",
      btn_cancel: "Cancel",
      btn_confirm: "Confirm",
      isCancel: true
    };

    let popModal = this.modalCtrl.create(PopupPage, {
      setPopup: set_Popup
    }
    );
    popModal.onDidDismiss(popup => {
      // console.log(popup);  
      if (popup) {
        this.btnPickup =false;
        this.btnSuccess =true;
        this.SendPickup();
      }
    })
    popModal.present();


  }


  SendPickup() { 
    let dId = localStorage.userId;
    if (this.selectItem.id != "") {
      this.pDriver.DriverPickup(dId, this.selectItem.id).then(r => {
        if (r.status == 200) {
           
        }
      });
    }
  }

  NextSuccess()
  {
     let dId = localStorage.userId; 
     if(this.selectItem.id !="")
     { 
      this.pDriver.DriverSuccess(dId,this.selectItem.id).then(r => {   
        if(r.status == 200)
        { 
          this.ts = 90000;
          this.startTiming(this.ts);
          this.runJob = false; 
          this.clean();
          this.addCurrentLoc();
        }  
      });
    }
  }


  clickSuccess() { 
    let set_Popup = { 
      title: "Confirm SUCCESS",
      text_message : "Do you want to Next SUCCESS? ",
      btn_cancel : "Cancel",
      btn_confirm : "Confirm",
      isCancel : true
    }; 
 
    let popModal = this.modalCtrl.create(PopupPage,{
      setPopup: set_Popup}
      );
    popModal.onDidDismiss(popup => {
     // console.log(popup);  
      if(popup)
      {
        this.NextSuccess();
      }
    })
    popModal.present(); 
  }

  initMap() { 

    if(localStorage.currentLat != undefined)
    {
           // 
    }
    else
    {
      localStorage.currentLat ="13.752782";
      localStorage.currentLng ="100.522993";
    }
 
    let latLng = new google.maps.LatLng(localStorage.currentLat, localStorage.currentLng);

    this.map = new google.maps.Map(this.mapElement.nativeElement, { 
      zoom: 17,
      mapTypeControl: false,
      disableDefaultUI: true,  
      center: latLng
    });   
    this.directionsDisplay.setMap(this.map); 
 
  }

  clean() {   
    //this.directionsDisplay.setDirections({routes: []});
    this.directionsDisplay.setMap(null); 
    this.initMap();  
  }
    

  confirmJob() {
    let set_Popup = {
      title: "Alert Confirm",
      text_message: this.alert_driver + "",
      btn_cancel: "No",
      btn_confirm: "Yes",
      isCancel: true
    };

    let popModal = this.modalCtrl.create(PopupPage, {
      setPopup: set_Popup
    }
    );
    popModal.onDidDismiss(popup => {
      if (popup) {
        this.NextConfirm();
      }
    })
    popModal.present();
  }


  NextConfirm() {
    let dId = localStorage.userId;
    if (this.selectItem.id != "") {
      this.pDriver.DriverConfirm(dId, this.selectItem.id).then(r => {
        if (r.status == 200) {
          // Popup message  follow order waitting payment credit 
          this.presentLoadingCustom(r.message);
          //this.presentLoadingCustom("กรุณารอลูกค้าทำการชำระเงิน"); 
        }
        else if (r.status == 201) {
          // Popup message  follow order payment Cash  
          this.popMessageOnJobb(r.message);
        }
        else if (r.status == 500) {
          // post-- order/cancel  
          //this.popMessageCancelJobb(r.message);
          this.popMessageCancelJobb("ลูกค้าชำระเงินไม่สำเร็จ");
        }
      });
    }
  }


  countOmise= 0;

  presentLoadingCustom(str) {
    //circles,dots
    let loading = this.loadingCtrl.create({
      spinner: 'circles',
      content: str,
      //duration: 5000
    });

    loading.onDidDismiss(() => {
      console.log('Dismissed loading');
      clearInterval(this.timerIntervalS);
    });

    loading.present();

    clearInterval(this.timerIntervalS);
    this.timerIntervalS = setInterval(() => {
      
      //loading.dismiss();
      this.pOrder.OrderGetIds(this.selectItem.user_id, this.selectItem.id).then(r => {
        if (r.status == 200) {

          let dt_order = r.data[0];
          //let dt_user = r.data['user'];
          //let dt_driver = r.data['driver'];
          let dt_payment = r.data['payment'];

          if (dt_order.status == "3") {
            if (dt_payment[0].card_number != "Cash") { 
              //0=waiting,1=success,2=error,3=pending	
              if (dt_payment[0].status_payment == "0") {
                //waiting
              }
              else if (dt_payment[0].status_payment == "1") {
                //success
                this.countOmise =0;
                loading.dismiss();
                this.popMessageOnJobb("ลูกค้าชำระเงินสำเร็จแล้ว");
              }
              else if (dt_payment[0].status_payment == "2") {
                //error
                this.countOmise =0;
                loading.dismiss();
                this.popMessageCancelJobb("ลูกค้าชำระเงินไม่สำเร็จ");
              }
              else if (dt_payment[0].status_payment == "3") {
                this.countOmise++;
                if(this.countOmise > 15)
                {
                  this.countOmise =0;
                  loading.dismiss();
                  this.popMessageCancelJobb("ลูกค้าชำระเงินไม่สำเร็จ");
                } 
                //pending
              } 
            }
          }
          else  if (dt_order.status == "0") { 
            this.countOmise =0;
            loading.dismiss();
            this.popMessageCancelJobb("ลูกค้าชำระเงินไม่สำเร็จ");
          }

        }
      });

    }, 10000);
 

  }

  popMessageCancelJobb(str) {
    let set_Popup = {
      title: "Pop-up Message",
      text_message: str,
      btn_cancel: "",
      btn_confirm: "Done",
      isCancel: false
    };

    let popModal = this.modalCtrl.create(PopupPage, {
      setPopup: set_Popup
    }
    );
    popModal.onDidDismiss(popup => {
      // console.log(popup);  
      this.CancelJobb();
    })
    popModal.present();

  }

  CancelJobb() {
    this.pOrder.OrderCancel(this.selectItem.user_id, this.selectItem.id).then(r => {
      this.clean(); 
      this.addCurrentLoc();
    });
  }


  popMessageOnJobb(str) {
    let set_Popup = {
      title: "Pop-up Message",
      text_message: str,
      btn_cancel: "",
      btn_confirm: "Next",
      isCancel: false
    };

    let popModal = this.modalCtrl.create(PopupPage, {
      setPopup: set_Popup
    }
    );
    popModal.onDidDismiss(popup => {
      // console.log(popup);  
      this.OnJobb();
    })
    popModal.present();

  }

  OnJobb() {
    this.runJob = true;
    this.btnSuccess =false;
    this.btnPickup =true;
    this.showBox = false;

    this.ts = 20000;
    this.startTiming(this.ts);

    this.clean(); 
    // console.log( this.selectItem.user_name); 
    this.ContactName = this.selectItem.user_name;

    this.addMarker(this.selectItem.start_location_lat,
      this.selectItem.start_location_long,
      "Pick-up",
      this.selectItem.start_location);

    this.addMarker(this.selectItem.destination_lat,
      this.selectItem.destination_long,
      "Drop off",
      this.selectItem.destination);

    this.addMarker(localStorage.currentLat,
      localStorage.currentLng,
      localStorage.userName, localStorage.currentPlaceB);

    
      this.map.setZoom(15);
      //this.map.setCenter(clocation);
      this.map.setCenter({lat:  this.selectItem.start_location_lat, lng: this.selectItem.start_location_long});

  }
  
  /*
  NextConfirm()
  {
    let dId = localStorage.userId; 
    if(this.selectItem.id !="")
     { 
      this.pDriver.DriverConfirm(dId,this.selectItem.id).then(r => {  
        if(r.status == 200)
        { 
          this.runJob =true;
          this.showBox =false;

          this.ts =20000; 
          this.startTiming(this.ts);

          this.removeMarkers(); 
          // console.log( this.selectItem.user_name); 
          this.ContactName = this.selectItem.user_name;
    
          this.addMarker(this.selectItem.start_location_lat,
            this.selectItem.start_location_long,
            "Pick-up",
            this.selectItem.start_location);

          this.addMarker(this.selectItem.destination_lat,
              this.selectItem.destination_long,
              "Drop off",
              this.selectItem.destination); 

          this.addMarker(localStorage.currentLat,
            localStorage.currentLng,
            localStorage.userName, localStorage.currentPlaceB); 

          this.resetMapBounds();
 
         }
 
      });
     }
  }*/

  
 
}


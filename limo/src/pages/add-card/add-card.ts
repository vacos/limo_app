import { Component} from '@angular/core'; 
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ViewController } from 'ionic-angular';
  

@Component({
  selector: 'page-add-card',
  templateUrl: 'add-card.html',
})
export class AddCardPage {
 
  private url:SafeResourceUrl;
   
  constructor(private domSanitizer : DomSanitizer,public viewCtrl: ViewController) {
     
  }
   

  dismiss() { 
    this.viewCtrl.dismiss();
  }
    
}


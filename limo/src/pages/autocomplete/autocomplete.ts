import {Component, NgZone} from '@angular/core';
import {ViewController} from 'ionic-angular';

declare var google;
@Component({
  templateUrl: 'autocomplete.html'
})

export class AutocompletePage {
  autocompleteItems;
  autocomplete;

  latitude: number = 0;
  longitude: number = 0;
  geo: any

  service = new google.maps.places.AutocompleteService();

  constructor (public viewCtrl: ViewController, private zone: NgZone) {
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  chooseItem(item: any) {  
    this.geo = item;
    this.geoCode(this.geo);//convert Address to lat and long
  }

  updateSearch() {

    if (this.autocomplete.query == '') {
     this.autocompleteItems = [];
     return;
    }

    // other types available in the API: 'establishment', 'regions', and 'cities'
    let me = this;
    this.service.getPlacePredictions({
    input: this.autocomplete.query,
    componentRestrictions: {
      country: 'TH'
    }, types: ['geocode'],language: 'th-TH'
   }, (predictions, status) => {
     me.autocompleteItems = [];

   me.zone.run(() => {
     if (predictions != null) {
        predictions.forEach((prediction) => {
          me.autocompleteItems.push({
            label_a: prediction.structured_formatting.main_text,
            label_b: prediction.description
          });
        });
       }
     });
   });
  }

  //convert Address string to lat and long
  geoCode(address:any) { 

    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address.label_b }, (results, status) => {
    this.latitude = results[0].geometry.location.lat();
    this.longitude = results[0].geometry.location.lng();
    
    let sLocation = {
      lat: results[0].geometry.location.lat(),
      lng: results[0].geometry.location.lng()
    };
  
     var datas = {
       select: sLocation,
       label_a: address.label_a,
       label_b: address.label_b
     }
     this.viewCtrl.dismiss(datas); 
    //alert("lat: " + this.latitude + ", long: " + this.longitude);
   });
 }
}
import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { HomePage } from '../home/home'; 
import { TermPage } from '../term/term';
import { PrivacyPage } from '../privacy/privacy';
declare var localStorage: any;
@Component({
  selector: 'page-term-condition',
  templateUrl: 'term-condition.html',
})
export class TermConditionPage {
  phoneNumber = "";
  userid = "";

  constructor(public navCtrl: NavController, 
    public events : Events, 
    public navParams: NavParams) {
    this.phoneNumber = this.navParams.get("phoneNumber");
    this.userid = this.navParams.get("userid");
  }

  gotoHomePage(){
    localStorage.userId = this.userid;
    localStorage.phoneNumber = this.phoneNumber;
    localStorage.memberType = "0";
    this.events.publish('username:changed', localStorage.userName); 
    this.navCtrl.setRoot(HomePage);
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad TermConditionPage');
  }

  viewTerm() { 
    this.navCtrl.push(TermPage);
  }


  viewPrivacy() {
    this.navCtrl.push(PrivacyPage);
  }
 

}

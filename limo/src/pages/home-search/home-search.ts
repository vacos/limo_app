import { Component, ViewChild, NgZone } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { AddPlacePage } from '../add-place/add-place';
import { AccountProvider } from '../../providers/account/account';
import { PopupPage } from '../modal-popup/modal-popup';

declare var google;
// declare var maps;

@Component({
  selector: 'page-home-search',
  templateUrl: 'home-search.html',
})
export class HomeSearchPage {
  @ViewChild('idOrigin') idOrigin;
  @ViewChild('idDestination') idDestination;

  map: any;
  ipModel = {
    origin_a: "",
    origin_b: "",
    destination_a: "",
    destination_b: ""
  }

  setOrigin = {
    lat: "",
    lng: ""
  };
  setDestination = {
    lat: "",
    lng: ""
  };
  isDestinationScr = true;
  originItems;
  originQuery;
  destinationItems;
  destinationQuery;

  label_myLocation_a = "";
  label_myLocation_b = "";

  placeList = [];

  service = new google.maps.places.AutocompleteService();

  geo: any;
  isMyPlace = false;

  constructor(public navCtrl: NavController,
    private pAccount: AccountProvider,
    public navParams: NavParams,
    private zone: NgZone,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController) {

    console.log(navParams.get('location_a'));

    this.label_myLocation_a = navParams.get('location_a');
    this.label_myLocation_b = navParams.get('location_b');
    this.setOrigin = navParams.get('userlocation');

    this.ipModel.origin_a = this.label_myLocation_a;
    this.ipModel.origin_b = this.label_myLocation_b;

    this.originItems = [];

    this.originQuery = {
      query: ''
    };

    if (this.label_myLocation_a != "") {
      this.originQuery.query = this.label_myLocation_a;
    }

    // this.originQuery.query = this.label_myLocation_a;

    this.destinationItems = [];
    this.destinationQuery = {
      query: ''
    };

    this.loadPlaceLists();
  }

  ionViewDidEnter() {
    setTimeout(() => {
      this.idDestination.setFocus();
    }, 150);
  }

  loadPlaceLists() {
    this.placeList = [];
    this.pAccount.UserListingPlace().then(r => {
      if (r.status == 200 && r.message == "Success") {
        //console.log(r.data);
        this.placeList = r.data;
        if (r.data.length > 0) {
          this.isMyPlace = true;
        }
      }
    });
  }

  // ngAfterViewInit() {
  //   setTimeout(() => {
  //      this.idDestination.setFocus();
  //   }, 150);
  // }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  clickDestination() {
    this.destinationItems = [];
    this.originItems = [];
    this.isDestinationScr = true;
    this.loadPlaceLists();
  }

  clickOrigin() {
    this.destinationItems = [];
    this.originItems = [];
    this.isDestinationScr = false;
    this.loadPlaceLists();
  }

  DestinationSearch() {
    this.isMyPlace = false;
    this.placeList = [];
    this.isDestinationScr = true;

    if (this.destinationQuery.query == '') {
      this.destinationItems = [];
      return;
    }

    // other types available in the API: 'geocode' , 'locality' , 'political', 'establishment', 'regions', and 'cities'
    let me = this;
    this.service.getPlacePredictions({
      input: this.destinationQuery.query,
      componentRestrictions: {
        country: 'TH'
      }, types: ['establishment'], language: 'th-TH'
    }, (predictions, status) => {
      me.destinationItems = [];

      me.zone.run(() => {
        if (predictions != null) {
          predictions.forEach((prediction) => {
            me.destinationItems.push({
              label_a: prediction.structured_formatting.main_text,
              label_b: prediction.description
            });
          });
        }
      });
    });
  }

  OriginSearch() {
    this.isMyPlace = false;
    this.placeList = [];
    this.isDestinationScr = false;

    if (this.originQuery.query == '') {
      this.originItems = [];
      return;
    }

    // other types available in the API: 'establishment', 'regions', and 'cities'
    let me = this;
    this.service.getPlacePredictions({
      input: this.originQuery.query,
      componentRestrictions: {
        country: 'TH'
      },
      types: ['establishment'],
      language: 'th-TH'
    }, (predictions, status) => {
      me.originItems = [];

      me.zone.run(() => {
        if (predictions != null) {
          predictions.forEach((prediction) => {
            me.originItems.push({
              label_a: prediction.structured_formatting.main_text,
              label_b: prediction.description
            });
          });
        }
      });
    });
  }


  chooseCurrentOriginItem() {
    this.originQuery.query = this.label_myLocation_a;
    this.ipModel.origin_a = this.label_myLocation_a;
    this.ipModel.origin_b = this.label_myLocation_b;

    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': this.label_myLocation_b }, (results, status) => {

      this.setOrigin = {
        lat: results[0].geometry.location.lat(),
        lng: results[0].geometry.location.lng()
      };
      //alert("lat: " + this.setOrigin.lat + ", long: " + this.setOrigin.lng);
      //alert(this.setDestination.lat);

      if (typeof this.setDestination.lat !== "undefined" && this.setDestination.lat != "") {
        this.goBackToMap({
          origin: this.setOrigin,
          desti: this.setDestination
        });
      }
      else {
        this.idDestination.setFocus();
        this.destinationItems = [];
        this.originItems = [];
        this.isDestinationScr = true;
      }

    });

  }

  choosePlaceOriginItem(item: any) {
    this.originQuery.query = item.name;
    this.ipModel.origin_a = item.name;
    this.setOrigin = {
      lat: item.lat,
      lng: item.long
    };

    let geocoder = new google.maps.Geocoder;
    geocoder.geocode({ 'location': this.setOrigin }, (results, status) => {
      //console.log(status);
      //console.log(results); 
      //this.ipModel.origin_a = results[0].address_components[0].short_name;
      this.ipModel.origin_b = results[0].formatted_address;
    });

    if (typeof this.setDestination.lat !== "undefined" && this.setDestination.lat != "") {

      if(this.setOrigin.lat == this.setDestination.lat &&
        this.setOrigin.lng == this.setDestination.lng)
        {
             this.openPopAlertMessage("Please not choose same location");
        }
        else
        {
          this.goBackToMap({
            origin: this.setOrigin,
            desti: this.setDestination
          });
        }

    }
    else {
      this.idDestination.setFocus();
      this.destinationItems = [];
      this.originItems = [];
      this.isDestinationScr = true;
    }

  }


  choosePlaceDestinationItem(item: any) {
    this.destinationQuery.query = item.name;
    this.ipModel.destination_a = item.name;

    this.setDestination = {
      lat: item.lat,
      lng: item.long
    };

    let geocoder = new google.maps.Geocoder;
    geocoder.geocode({ 'location': this.setDestination }, (results, status) => {
      //console.log(status);
      //console.log(results); 
      //this.ipModel.destination_a = results[0].address_components[0].short_name;
      this.ipModel.destination_b = results[0].formatted_address;
    });

    if (typeof this.setOrigin.lat !== "undefined") {

      if(this.setOrigin.lat == this.setDestination.lat &&
         this.setOrigin.lng == this.setDestination.lng)
         {
              this.openPopAlertMessage("Please not choose same location");
         }
         else
         {
          this.goBackToMap({
            origin: this.setOrigin,
            desti: this.setDestination
          });
         } 

    }
    else {
      this.idOrigin.setFocus();
      this.destinationItems = [];
      this.originItems = [];
      this.isDestinationScr = false;
    }

  }

  chooseDestinationItem(item: any, set: string) {
    this.geo = item;
    this.destinationQuery.query = item.label_a;
    this.ipModel.destination_a = item.label_a;
    this.ipModel.destination_b = item.label_b;
    if (set == '1') {
      this.geoCodeDestination(this.geo);//convert Address to lat and long
    }
    else if (set == '2') {
      this.AddPlaces(this.geo);
    }
    else if (set == '3') {
      this.DelPlaces(this.geo);
    }
  }

  DelPlaces(item)
  { 
    this.pAccount.UserDelPlace( 
      item.place_id
    ).then(r => {
     // console.log(r);
      if (r.status == 200 && r.message == "Success") {
          this.dismiss();
      }
      else
      {
        this.openPopAlertMessage(r.message); 
      }
    });   
  }

  geoCodeDestination(address: any) {

    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address.label_b }, (results, status) => {

      this.setDestination = {
        lat: results[0].geometry.location.lat(),
        lng: results[0].geometry.location.lng()
      };
      //alert("lat: " + this.setDestination.lat + ", long: " + this.setDestination.lng);

      //alert(this.setOrigin.lat); 
      //console.log(this.setOrigin);

      if (typeof this.setOrigin.lat !== "undefined") {
        this.goBackToMap({
          origin: this.setOrigin,
          desti: this.setDestination
        });
      }
      else {
        this.idOrigin.setFocus();
        this.destinationItems = [];
        this.originItems = [];
        this.isDestinationScr = false;
      }
    });
  }


  chooseOriginItem(item: any, set: string) {
    this.geo = item;
    this.originQuery.query = item.label_a;
    this.ipModel.origin_a = item.label_a;
    this.ipModel.origin_b = item.label_b;
    if (set == '1') {
      this.geoCodeOrigin(this.geo);//convert Address to lat and long
    }
    else if (set == '2') {
      this.AddPlaces(this.geo);
    }
    else if (set == '3') {
      this.DelPlaces(this.geo);
    }
  }

  geoCodeOrigin(address: any) {

    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address.label_b }, (results, status) => {

      this.setOrigin = {
        lat: results[0].geometry.location.lat(),
        lng: results[0].geometry.location.lng()
      };
      //alert("lat: " + this.setOrigin.lat + ", long: " + this.setOrigin.lng);
      //alert(this.setDestination.lat);

      if (typeof this.setDestination.lat !== "undefined" && this.setDestination.lat != "") {
        this.goBackToMap({
          origin: this.setOrigin,
          desti: this.setDestination
        });
      }
      else {
        this.idDestination.setFocus();
        this.destinationItems = [];
        this.originItems = [];
        this.isDestinationScr = true;
      }

    });
  }


  AddPlaces(item) {

    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': item.label_b }, (results, status) => {

      let place = {
        lat: results[0].geometry.location.lat(),
        lng: results[0].geometry.location.lng(),
        name: item.label_a,
        address: item.label_b
      };

      let addPlacesModal = this.modalCtrl.create(AddPlacePage,
        {
          place: place
        }
      );
      addPlacesModal.onDidDismiss(data => {
        this.navCtrl.pop();
      });
      addPlacesModal.present();

    });


  }


  goBackToMap(data) {
    var datas = {
      select: data,
      label: this.ipModel
    }
    this.viewCtrl.dismiss(datas);
  }


  openPopAlertMessage(msg:string) { 
    let set_Popup = { 
      title: "Pop-up Request",
      text_message : msg,
      btn_cancel : "",
      btn_confirm : "Done",
      isCancel : false
    }; 
 
    let popModal = this.modalCtrl.create(PopupPage,{
      setPopup: set_Popup}
      );
    popModal.onDidDismiss(popup => {
      console.log(popup);    
    })
    popModal.present(); 
  }

}
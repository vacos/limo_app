import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Events, ModalController } from 'ionic-angular';
import { AccountProvider } from '../../providers/account/account';
// import { HomePage } from '../home/home';
import { PopupPage } from '../modal-popup/modal-popup';
// import { HomeDriverPage } from '../home-driver/home-driver';
import { MyLocationPage } from '../my-location/my-location';
declare var localStorage: any; 
@Component({
  selector: 'page-password-input',
  templateUrl: 'password-input.html',
})
export class PasswordInputPage {
  ipPassword = "";
  phoneNumber = "";
  constructor(public navCtrl: NavController,
    private alertCtrl: AlertController,
    public events : Events,
     public navParams: NavParams,
     public modalCtrl: ModalController,
      private pAccount: AccountProvider) {

    this.phoneNumber = navParams.get('phoneNumber');
  }

  ionViewDidLoad() { 
  }
 

  sendResetPassword() { 
    let set_Popup = { 
      title: "Alert Confirm",
      text_message : "คุณต้องการ Reset Password ? ",
      btn_cancel : "Cancel",
      btn_confirm : "OK",
      isCancel : true
    }; 

    let popModal = this.modalCtrl.create(PopupPage,{
      setPopup: set_Popup}
      );
    popModal.onDidDismiss(popup => {
     // console.log(popup);  
      if(popup)
      {
        this.sendReset();
      }
    })
    popModal.present(); 
  }



  sendReset()
  {
    this.pAccount.UserResetPassword(this.phoneNumber).then(r => {
      if(r.status == 200){
        this.openPopAlertSendResetPassword(r.message); 
      }
      else  if(r.status == 500){
        this.openPopAlertSendResetPassword(r.message); 
      }
      else
      {
        this.openPopAlertSendResetPassword("เกิดข้อผิดพลาด กรุณาทำรายการใหม่ภายหลัง"); 
      }
    });
  }


  openPopAlertSendResetPassword(msg:string) { 
    let set_Popup = { 
      title: "Pop-up Request",
      text_message : msg,
      btn_cancel : "",
      btn_confirm : "Done",
      isCancel : false
    }; 
 
    let popModal = this.modalCtrl.create(PopupPage,{
      setPopup: set_Popup}
      );
    popModal.onDidDismiss(popup => {
     // console.log(popup);    
    })
    popModal.present(); 
  }

  
  nextState() {
    if (this.ipPassword != "") {
      this.pAccount.UserLogin(this.phoneNumber, this.ipPassword).then(r => {
        if(r.status == 200 && r.message == "Success"){
          //console.log(r.data); 
          localStorage.userId = r.data.id;
          localStorage.phoneNumber = r.data.phone;
          localStorage.Password = this.ipPassword;
          localStorage.memberType = r.data.type;
          localStorage.userName = r.data.name; 
          localStorage.userImage = r.data.image; 
          this.events.publish('username:changed', r.data.name); 


          this.navCtrl.setRoot(MyLocationPage); 
          
          // if(r.data.type == 0)
          // {
          //   this.navCtrl.setRoot(HomePage); 
          // }
          // else if(r.data.type == 1)
          // {
          //   this.navCtrl.setRoot(HomePage); 
          // }
          // else if(r.data.type == 2)
          // {
          //   this.navCtrl.setRoot(HomeDriverPage); 
          // } 

        }else{ 
            if(r.status == 500 && r.message == "User is logged"){
              localStorage.userId = r.data.id;
              this.logout();
            }
            else
            { 
              this.openPopAlertMessage(r.message,"0"); 
            }
        }
      });
    }
  }


  openPopAlertMessage(msg:string,set:string) { 
    let set_Popup = { 
      title: "Pop-up Request",
      text_message : msg,
      btn_cancel : "",
      btn_confirm : "Done",
      isCancel : false
    }; 

    if(set=="1")
    {
      set_Popup.title ="User is logged";
      set_Popup.btn_confirm ="Re-login";
    }

    let popModal = this.modalCtrl.create(PopupPage,{
      setPopup: set_Popup}
      );
    popModal.onDidDismiss(popup => {
     // console.log(popup);   
      if(set=="1")
      { 
        this.relogin();
      }
    })
    popModal.present(); 
  }

  logout()
  {
    this.pAccount.UserLogOut().then(r => { 
      localStorage.clear();  
      //this.openPopAlertMessage("You have logged in at another device, please re-login again","1");  

      this.relogin();
      
    }); 
  }

  relogin()
  {
    this. nextState();  
  }

  AlertContrl(msg:string){ 
    let alert = this.alertCtrl.create({
      title: 'Alert Message',
      message: msg,
      buttons: [
        {
          text: 'Done',
          role: 'cancel',
          handler: () => { 
          }
        } 
      ]
    });
    alert.present();
  }

}

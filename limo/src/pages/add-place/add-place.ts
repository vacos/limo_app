import { Component} from '@angular/core'; 
import { ViewController,  ActionSheetController, LoadingController, ToastController, Platform,  NavParams , ModalController } from 'ionic-angular';
import { AccountProvider } from '../../providers/account/account';  
import { PopupPage } from '../modal-popup/modal-popup';

@Component({
  selector: 'page-add-place',
  templateUrl: 'add-place.html',
})
export class AddPlacePage {
   
  ipName = "";
  Address= "";    
  lat="";
  lng="";
  type="1";
   
  constructor(private pAccount: AccountProvider, 
    public actionSheetCtrl: ActionSheetController,  
    public loadingCtrl: LoadingController, 
    public toastCtrl: ToastController,   
    public navParams: NavParams ,
    public platform: Platform,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController) { 

      //console.log(navParams.get('place'));
      let data = navParams.get('place')
      this.ipName = data.name;
      this.Address= data.address;
      this.lat = data.lat;
      this.lng= data.lng;

  }

  Save(){ 

    if(this.ipName !="")
    {
      this.pAccount.UserAddPlace(
        {
          lat: this.lat,
          long: this.lng,
          type: this.type,
          name: this.ipName
        }
      ).then(r => {
       // console.log(r);
        if (r.status == 200 && r.message == "Success") {
            this.dismiss();
        }
        else
        {
          this.openPopAlertMessage(r.message); 
        }
      }); 

    }
    else
    {
      this.openPopAlertMessage("Enter your place");
    }
   
  }


  openPopAlertMessage(msg:string) { 
    let set_Popup = { 
      title: "Pop-up Request",
      text_message : msg,
      btn_cancel : "",
      btn_confirm : "Done",
      isCancel : false
    }; 
  
    let popModal = this.modalCtrl.create(PopupPage,{
      setPopup: set_Popup}
      );
    popModal.onDidDismiss(popup => {
     // console.log(popup);    
    })
    popModal.present(); 
  }

  ionViewDidLoad(){
     
  }
  
    
  dismiss() {  
    this.viewCtrl.dismiss();
  }


  segmentChanged() {
   // console.log('Segment selected', this.type);  
  }
      
 
}


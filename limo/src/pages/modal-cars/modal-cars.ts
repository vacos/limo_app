import { Component, OnInit } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { OrderProvider } from '../../providers/order/order'; 
 

@Component({
    selector: 'page-select-cars',
    templateUrl: 'modal-cars.html'
})
export class SelectCars {

    list_cars=[];
   
    constructor(public viewCtrl: ViewController, public navParams: NavParams ,
        private pOrder: OrderProvider) {  

       let durat = navParams.get('Duration');
       let dist = navParams.get('Distance');   

       this.loadList(durat,dist);

    }


    loadList(Duration,Distance)
    {
        this.pOrder.OrderCalculate(Duration,
            Distance).then(r => {
              //console.log(r);
                this.list_cars =r.data;
            });
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    chooseItem(item: any) {
       // console.log('modal > chooseItem > item > ', item);
        this.viewCtrl.dismiss(item);
    }
     
}

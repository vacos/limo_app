import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { PageApiProvider } from '../../providers/page-api/page-api';

/**
 * Generated class for the YourTripPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-privacy',
  templateUrl: 'privacy.html',
})
export class PrivacyPage {

  privacy_html: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private pget: PageApiProvider,
    public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    
     // legal, term, refund
     
     let ttype ="privacy";
    if(localStorage.memberType =="2")
    {
      ttype ="privacy_driver";
    }

    this.pget.PageApiGet(ttype).then(r => {
      if(r.status == 200 && r.message == "Success"){ 
        this.privacy_html= r.data.content;
      } 
    });

  }
 
 

  dismiss() {
    this.viewCtrl.dismiss();
  }

}

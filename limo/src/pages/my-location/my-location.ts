import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { HomePage } from '../home/home';  
 
import { HomeDriverPage } from '../home-driver/home-driver'; 
import { PopupPage } from '../modal-popup/modal-popup';
import { AccountProvider } from '../../providers/account/account';
import {GoogleMaps,GoogleMap, MyLocation} from '@ionic-native/google-maps';

declare var google;
declare var localStorage: any;

@Component({
  selector: 'page-my-location',
  templateUrl: 'my-location.html',
})
export class MyLocationPage {
 
  isGPS =false; 
  map: GoogleMap;


  constructor(public navCtrl: NavController,
    private pAccount: AccountProvider,
     public modalCtrl: ModalController) {
  }
  
 

  ionViewDidLoad() {   
    this.map = GoogleMaps.create('map_canvas1', {}); 
    
    this.GetCurrentLocation();   
  }
 
  
  GetCurrentLocation() { 
 
    //this.map.clear();

    // Get the location of you
    this.map.getMyLocation()
      .then((location: MyLocation) => {
       // console.log(JSON.stringify(location, null ,2));

        localStorage.currentLat = location.latLng.lat;  
        localStorage.currentLng = location.latLng.lng;  
 
        let geocoder = new google.maps.Geocoder;
          geocoder.geocode({ 'location': location.latLng }, (results, status) => { 
           // console.log(results); 
            localStorage.currentPlaceA =results[0].address_components[0].short_name + ' ' + results[0].address_components[1].short_name;
            localStorage.currentPlaceB =results[0].formatted_address;        
            
            this.pAccount.UserLoginApp().then(r => {  
              // console.log(r);  
             }); 
             //this.isGPS =true; 
             this.GotoHomePage(); 
            
          });  
  
      }).catch((error) => {
        //alert('Error getting location');
        this.isGPS =true; 
        console.log(error); 
      }); 
  }

  GotoHomePage() {

    if(localStorage.memberType =="0")
    {
      this.navCtrl.setRoot(HomePage);

    }
    else if(localStorage.memberType =="1")
    {
      this.navCtrl.setRoot(HomePage);
    }
    else if(localStorage.memberType =="2")
    {
      this.navCtrl.setRoot(HomeDriverPage);
    } 

  }


  openPopAlertMessage(msg:string) { 
    let set_Popup = { 
      title: "Pop-up Request",
      text_message : msg,
      btn_cancel : "",
      btn_confirm : "Done",
      isCancel : false
    }; 
 
    let popModal = this.modalCtrl.create(PopupPage,{
      setPopup: set_Popup}
      );
    popModal.onDidDismiss(popup => {
       
      this.GetCurrentLocation();
      
    })
    popModal.present(); 
  }

 
}

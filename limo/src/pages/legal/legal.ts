import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { PageApiProvider } from '../../providers/page-api/page-api';

/**
 * Generated class for the YourTripPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-legal',
  templateUrl: 'legal.html',
})
export class LegalPage {

  legal_html: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private pget: PageApiProvider,
    public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad YourTripPage');

     // legal, term, refund
    this.pget.PageApiGet("legal").then(r => {
      if(r.status == 200 && r.message == "Success"){ 
        this.legal_html= r.data.content;
      } 
    });

  }
 
 

  dismiss() {
    this.viewCtrl.dismiss();
  }

}

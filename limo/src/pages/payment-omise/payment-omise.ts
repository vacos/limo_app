import { Component, ViewChild} from '@angular/core'; 
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ViewController, LoadingController, NavParams } from 'ionic-angular';
  

@Component({
  selector: 'page-payment-omise',
  templateUrl: 'payment-omise.html',
})
export class PaymentOmisePage {


  @ViewChild('hiddenInput') hiddenInput; 
 
  private url:SafeResourceUrl;

  str_url = '';
  
   
  constructor(private domSanitizer : DomSanitizer,
    public loadingCtrl : LoadingController,
    public navParams: NavParams,
    public viewCtrl: ViewController) { 
     this.str_url = navParams.get('url'); 
     this.loadurl(this.str_url);
  }
  
  ionViewWillLeave() {
    this.hiddenInput.setFocus();
    this.hiddenInput.setBlur(); 
  }

  loadurl(url_)
  {
    let loader = this.loadingCtrl.create({
      spinner: 'dots',
      content: ""
    });
    loader.present();
    
    setTimeout(x => {  
      loader.dismiss();
      }, 1500);

    this.url =this.domSanitizer.bypassSecurityTrustResourceUrl(url_);
  }

  dismiss() {
    // let iframe1 = document.getElementById('iframe1').baseURI; 
    this.viewCtrl.dismiss();
  }

  popKeyb() {
    this.hiddenInput.setFocus();
    this.hiddenInput.setBlur(); 
  }
    
}


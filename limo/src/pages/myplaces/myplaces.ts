import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController } from 'ionic-angular'; 
import { AuthenticationPage } from '../authentication/authentication';
import { AccountProvider } from '../../providers/account/account';
import { PopupPage } from '../modal-popup/modal-popup';

/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-myplaces',
  templateUrl: 'myplaces.html',
})
export class MyplacesPage {

  emailAddress = "";
  mobilePhone = "";  
  userName = "";
  image_account ="";
  placeList = [];

  constructor(public navCtrl: NavController,private pAccount: AccountProvider, 
    public modalCtrl: ModalController, 
     public navParams: NavParams,public viewCtrl: ViewController) {
  }

  ionViewDidLoad(){ 
    //this.loadPlace();
    this.initPlaceLists();

  }

  initPlaceLists()
  {
    
    this.pAccount.UserListingPlace().then(r => {
      if (r.status == 200 && r.message == "Success") {
        this.placeList = r.data;
      }
    });

  }

  loadPlace()
  {
    this.placeList.push({
      type: 1,
      name: "สนามบินสุวรรณภูมิ 999 หมู่ 1 Nong Prue, Amphoe Bang Phli, Chang Wat Samut Prakan 10540"
    });

    this.placeList.push({
      type: 2,
      name: "สนามบินสุวรรณภูมิ 222 Vibhavadi Rangsit Rd, Khwaeng Sanambin, Khet Don Mueang, Krung Thep Maha Nakhon 10210"
    }); 

    this.placeList.push({
      type: 3,
      name: "สนามบินสุวรรณภูมิ 222 Vibhavadi Rangsit Rd, Khwaeng Sanambin, Khet Don Mueang, Krung Thep Maha Nakhon 10210"
    }); 

  }
 
 
  OnClick_Logout(){
    this.navCtrl.setRoot(AuthenticationPage);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  removeItem(p) { 
    let set_Popup = { 
      title: "Confirm delete",
      text_message : "Do you want to delete this place? ",
      btn_cancel : "Cancel",
      btn_confirm : "Delete",
      isCancel : true
    }; 
 
    let popModal = this.modalCtrl.create(PopupPage,{
      setPopup: set_Popup}
      );
    popModal.onDidDismiss(popup => {
     // console.log(popup);  
      if(popup)
      {
      //  console.log(p.id);  
        this.pAccount.UserDelPlace(p.place_id).then(r=>{
          this.initPlaceLists();
         });
      }
    })
    popModal.present(); 
  }

  
}

import { Component, OnInit } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { AccountProvider } from '../../providers/account/account';
 

@Component({
    selector: 'page-place',
    templateUrl: 'modal-place.html'
})
export class SelectPlace {
   
    placeList = [];
    
    constructor(public viewCtrl: ViewController,private pAccount: AccountProvider,) {  
        this.initPlaceLists();
    }


    initPlaceLists() {
        this.pAccount.UserListingPlace().then(r => {
          if (r.status == 200 && r.message == "Success") {
            this.placeList = r.data;
          }
        });
      }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    chooseItem(item: any) {
       // console.log('modal > chooseItem > item > ', item);
        this.viewCtrl.dismiss(item);
    }
     
}

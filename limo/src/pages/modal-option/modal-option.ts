import { Component, OnInit } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
 

@Component({
    selector: 'page-select-option',
    templateUrl: 'modal-option.html'
})
export class SelectOption {
    textNote="";
    constructor(public viewCtrl: ViewController,public navParams: NavParams) {  
        this.textNote= navParams.get('text');   
    }

    dismiss() {
        let text= this.textNote;
        this.viewCtrl.dismiss(text);
    }

    chooseItem(item: any) {
        //console.log('modal > chooseItem > item > ', item);
        this.viewCtrl.dismiss(item);
    }
     
    SaveOption()
    {
        let text= this.textNote;
        this.viewCtrl.dismiss(text);
    }
}

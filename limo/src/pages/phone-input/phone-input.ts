import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { ValidateOtpPage } from '../validate-otp/validate-otp';
import { AccountProvider } from '../../providers/account/account';
import { PasswordInputPage } from '../password-input/password-input';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { PopupPage } from '../modal-popup/modal-popup';

@Component({
  selector: 'page-phone-input',
  templateUrl: 'phone-input.html',
})
export class PhoneInputPage {
  @ViewChild('myselect') selectComponent: SelectSearchableComponent;

  ipPhoneNumber = "";
  ipCountryCode = "TH";
  cCountrys = [];
  phoneValidate =false;
  message_phone_validate ="Check Your Phone Number";

  selectedCountry = {}
  countries = [{
    name: "United States",
    dial_code: "+1",
    code: "US"
  },   {
    name: "Canada",
    dial_code: "+1",
    code: "CA"
  },  {
    name: "China",
    dial_code: "+86",
    code: "CN"
  },   {
    name: "France",
    dial_code: "+33",
    code: "FR"
  },  {
    name: "Germany",
    dial_code: "+49",
    code: "DE"
  }, {
    name: "Greece",
    dial_code: "+30",
    code: "GR"
  }, {
    name: "Indonesia",
    dial_code: "+62",
    code: "ID"
  }, {
    name: "Iraq",
    dial_code: "+964",
    code: "IQ"
  },  {
    name: "Italy",
    dial_code: "+39",
    code: "IT"
  }, {
    name: "Japan",
    dial_code: "+81",
    code: "JP"
  },   {
    name: "Malaysia",
    dial_code: "+60",
    code: "MY"
  },      {
    name: "Myanmar",
    dial_code: "+95",
    code: "MM"
  },  {
    name: "Netherlands",
    dial_code: "+31",
    code: "NL"
  },    {
    name: "Panama",
    dial_code: "+507",
    code: "PA"
  },   {
    name: "Peru",
    dial_code: "+51",
    code: "PE"
  }, {
    name: "Philippines",
    dial_code: "+63",
    code: "PH"
  },   {
    name: "Qatar",
    dial_code: "+974",
    code: "QA"
  },  {
    name: "Saudi Arabia",
    dial_code: "+966",
    code: "SA"
  }, {
    name: "Singapore",
    dial_code: "+65",
    code: "SG"
  },  {
    name: "South Africa",
    dial_code: "+27",
    code: "ZA"
  },  {
    name: "Spain",
    dial_code: "+34",
    code: "ES"
  }, {
    name: "Sri Lanka",
    dial_code: "+94",
    code: "LK"
  },   {
    name: "Switzerland",
    dial_code: "+41",
    code: "CH"
  },  {
    name: "Thailand",
    dial_code: "+66",
    code: "TH"
  },    {
    name: "United Kingdom",
    dial_code: "+44",
    code: "GB"
  },   {
    name: "Brunei Darussalam",
    dial_code: "+673",
    code: "BN"
  },  {
    name: "Hong Kong",
    dial_code: "+852",
    code: "HK"
  },   {
    name: "Korea, Republic of",
    dial_code: "+82",
    code: "KR"
  },    {
    name: "Russia",
    dial_code: "+7",
    code: "RU"
  }, {
    name: "Taiwan, Province of China",
    dial_code: "+886",
    code: "TW"
  }, {
    name: "Viet Nam",
    dial_code: "+84",
    code: "VN"
  }]


  constructor(public navCtrl: NavController, 
    public modalCtrl: ModalController,
    public navParams: NavParams, private pAccount: AccountProvider) {
  }

  sChanged(event: { component: SelectSearchableComponent, value: any}) {
    // User was selected
  }
 
  onClose() {
    // let toast = this.toastCtrl.create({
    //   message: 'Thanks for your selection',
    //   duration: 2000
    // });
    // toast.present();
  }
 
  
  phoneValidator(str) {
    
    let regExp =/^([0]{1})([0-9]{2})([0-9]{3})([0-9]{4})$/;
    //let regExp = /^[0-9]{10}$/;
    return regExp.test(str);
  }

 
  openFromCode() {
    this.selectComponent.open();
  }

  ionViewDidLoad() {

  }

  nextState() {
    if (this.ipPhoneNumber != "") { 
      let isNumb = this.phoneValidator(this.ipPhoneNumber);
      if(isNumb)
      {
        this.pAccount.UserCheck(this.ipPhoneNumber).then(r => {
          if (r.message == "Is Member") {
            localStorage.phoneNumber = r.data.phone;
            this.navCtrl.push(PasswordInputPage, { phoneNumber: r.data.phone });
          } else {
            //console.log("CODE : ",r.data.code);
            //alert("OTP CODE : " + r.data.code);
            localStorage.phoneNumber = this.ipPhoneNumber;
            this.navCtrl.push(ValidateOtpPage, r.data);
          }
        });
      }
      else
      { 
         this.phoneValidate =true;
        //this.openPopAlertMessage("Check Your Phone Number");
      }
 
    }else
    { 
       this.phoneValidate =true;
      //this.openPopAlertMessage("Check Your Phone Number");
    }

  }



  openPopAlertMessage(msg:string) { 
    let set_Popup = { 
      title: "Pop-up Request",
      text_message : msg,
      btn_cancel : "",
      btn_confirm : "Done",
      isCancel : false
    }; 
 
    let popModal = this.modalCtrl.create(PopupPage,{
      setPopup: set_Popup}
      );
    popModal.onDidDismiss(popup => {
      // console.log(popup);    
    })
    popModal.present(); 
  }

}

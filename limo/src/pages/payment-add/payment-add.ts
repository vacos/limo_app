import { Component, ViewChild} from '@angular/core'; 
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ViewController, LoadingController, NavController } from 'ionic-angular';
  

@Component({
  selector: 'page-payment-add',
  templateUrl: 'payment-add.html',
})
export class PaymentAddPage {
 

  @ViewChild('hiddenInput') hiddenInput; 
  
  private url:SafeResourceUrl;
   
  constructor(private domSanitizer : DomSanitizer,
    public navCtrl: NavController, 
    public viewCtrl: ViewController, 
    public loadingCtrl : LoadingController) {
     this.loadurl();
  }
  
  ionViewWillLeave() {
    this.hiddenInput.setFocus();
    this.hiddenInput.setBlur(); 
  }
  
  loadurl()
  {
    let loader = this.loadingCtrl.create({
      spinner: 'dots',
      content: ""
    });
    loader.present();
    
    setTimeout(x => {  
      loader.dismiss();
      }, 1500);

    this.url =this.domSanitizer.bypassSecurityTrustResourceUrl("https://limoapp.me/add_card?user_id=" + localStorage.userId);
  }

  dismiss() {
    // let iframe1 = document.getElementById('iframe1').baseURI; 
    this.viewCtrl.dismiss();
  }
    
  popKeyb() {
    this.hiddenInput.setFocus();
    this.hiddenInput.setBlur(); 
  }
}


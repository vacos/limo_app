import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, ViewController } from 'ionic-angular';
import { PaymentAddPage } from '../payment-add/payment-add';
import { AccountProvider } from '../../providers/account/account';
import { PopupPage } from '../modal-popup/modal-popup';
import { AddCardPage } from '../add-card/add-card';

/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {

  isAdd =false;
  isCash =false;
  paymentList = [ ];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController, private pAccount: AccountProvider,
    private alertCtrl: AlertController ,public viewCtrl: ViewController) {
    this.initPaymentLists(); 
  }

  initPaymentLists() {
    this.isAdd =false;
    this.paymentList =[];
    this.pAccount.UserPaymentListing().then(r => {
      if (r.status == 200 && r.message == "Success") { 
        this.paymentList = r.data;
      }
    });
  }

  clickAdd()
  {
    this.isAdd =!this.isAdd;
    if(this.isAdd)
    {
      this.isCash =true;
      for (let i = 0; i <  this.paymentList.length; i++) {
        if ( this.paymentList[i].card_number == 'Cash') {
            this.isCash =false;
            break;
        }
      }
    }
    // alert(this.isCash);
  }

  OpenAddCash()
  { 

    this.pAccount.UserPaymentAddCash().then(r => {
      if (r.data) { 
        this.initPaymentLists(); 
      }
    });
 
  }


  OpenAddCredit() {
    let profileModal = this.modalCtrl.create(PaymentAddPage , { userId: 8675309 });
    profileModal.onDidDismiss(data => {
      this.isAdd =false;
      this.initPaymentLists();
    });
    profileModal.present();
  }


  OpenAddCreditII() {
    let profileModal = this.modalCtrl.create(AddCardPage , { userId: 8675309 });
    profileModal.onDidDismiss(data => {
      this.isAdd =false;
      this.initPaymentLists();
    });
    profileModal.present();
  }
    
  removeItem_B(p) { 
    let set_Popup = { 
      title: "Confirm delete",
      text_message : "Do you want to delete this card? ",
      btn_cancel : "Cancel",
      btn_confirm : "Delete",
      isCancel : true
    }; 
 
    let popModal = this.modalCtrl.create(PopupPage,{
      setPopup: set_Popup}
      );
    popModal.onDidDismiss(popup => {
     // console.log(popup);  
      if(popup)
      {
        this.pAccount.UserPaymentDelete(p.id).then(r=>{
          this.initPaymentLists();
         });
      }
    })
    popModal.present(); 
  }


  removeItem(p) { 
    this.pAccount.UserPaymentDelete(p.id).then(r=>{
      this.initPaymentLists();
     });
  }

  dismiss() {

    if(this.isAdd)
    { 
      this.isAdd =!this.isAdd;
    }
    else
    { 
      this.viewCtrl.dismiss();
    }
  }
}

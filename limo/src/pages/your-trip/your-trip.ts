import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { OrderProvider } from '../../providers/order/order';
import { iterateListLike } from '@angular/core/src/change_detection/change_detection_util';

/**
 * Generated class for the YourTripPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-your-trip',
  templateUrl: 'your-trip.html',
})
export class YourTripPage {

  historyList =[];

  statusOrder = ['Cancel','Success','Find Driver','Driving'];

  /*
0 Cancel
1 Success
2 Find Driver
3 Driving
  */
  
  constructor(public navCtrl: NavController, 
    private pOder: OrderProvider,
    public navParams: NavParams,public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad YourTripPage');
    this.loadHistory();
  }


  dismiss() {
    this.viewCtrl.dismiss();
  }

  loadHistory()
  {
    this.pOder.OrderHistory("0","20").then(r => {
      if(r.status == 200 && r.message == "Success"){ 
       // console.log(r.data);
        this.historyList = r.data;
      } 
    });
  }


  set_statusOrder(i)
  {
      let  setT = "sp_color_b";
      if(i=='1')
      {
        setT = "sp_color_a";
      }

      return setT;

  }



}

import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home'; 
/**
 * Generated class for the ValidatePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-validate-password',
  templateUrl: 'validate-password.html',
})
export class ValidatePasswordPage {
  inputPassword = "";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
   // console.log('ionViewDidLoad ValidatePasswordPage');
  }


  OnClick_Login(){
    this.navCtrl.setRoot(HomePage);
  }

  OnClick_ForgotPassword(){
    alert("Unavailable now.");
  }
}


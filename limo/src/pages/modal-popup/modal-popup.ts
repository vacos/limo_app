import { Component} from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
 

@Component({
    selector: 'page-popup',
    templateUrl: 'modal-popup.html'
})
export class PopupPage {

    setPopup = { 
        title: "Pop-up Request",
        text_message : "",
        btn_cancel : "Cancel",
        btn_confirm : "Confirm",
        isCancel: true
      }; 

     
    constructor(public viewCtrl: ViewController, public navParams: NavParams) { 
        //console.log( navParams.get('setPopup'));
        this.setPopup= navParams.get('setPopup');   
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    chooseItem(val) {
       // console.log('choose btn > ', val);
        this.viewCtrl.dismiss(val);
    }
     
    SaveOption()
    {
        this.viewCtrl.dismiss();
    }
}

import { Component, ViewChild, ElementRef } from '@angular/core';
import { ModalController,  LoadingController, ToastController, NavController } from 'ionic-angular';
import { HomeSearchPage } from '../home-search/home-search'; 
import { OrderProvider } from '../../providers/order/order'; 
import { DriverProvider } from '../../providers/driver/driver';  
import { SelectPayment } from '../modal-set-card/modal-select-payment';
import { SelectOption } from '../modal-option/modal-option';
import { SelectCars } from '../modal-cars/modal-cars';
import { AccountProvider } from '../../providers/account/account';
import { SelectPlace } from '../modal-place/modal-place';
import { PopupPage } from '../modal-popup/modal-popup'; 
import { CallNumber } from '@ionic-native/call-number';
import { PaymentPage } from '../payment/payment'; 
import { TermPage } from '../term/term';
import { GoogleMap, MyLocation, GoogleMaps, LatLng} from '@ionic-native/google-maps';
import { PaymentOmisePage } from '../payment-omise/payment-omise';

declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('map') mapElement: ElementRef;
 
  btnState: string = "Go Everywhere?"; 
  gmap: GoogleMap;
  map: any;
  markers: any;
  gmarkers = [];
  bounds: LatLng[] = [];
  start = '';
  end = '';
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;

  MemberType = '0';
  searchLocal: boolean = false;
  searchState: boolean = false;
  bookState: boolean = false;
  currentlocation: string;
  destinationlocation: string;
  currentlocation_b: string;
  destinationlocation_b: string;
  long: any; //longitude
  lat: any; //latitude
  Payment ="Payment";

  timerInterval: number; 
  pActive = false;
  runJob = false;

  locationRequest = {
    ipStart: "",
    ipStartDetail: "-",
    ipEnd: "",
    ipEndDetail: "-",
    ipDuration: "", 
    ipDistance: "",
    ipDuration_v: "",
    ipDistance_v: ""
  }


  dataReq = {
    start_location_lat: "",
    start_location_long: "",
    start_location: "",
    destination_lat: "",
    destination_long: "",
    destination: "",
    car_type: "", 
    id_payment: "",  
    fare: "",  
    note: "",
    expressway: ""
  };


  OrderCalList = {
    name: "",
    icon: "",
    fare: 99
  }

  locationLabel = {};
  address: any;
  userLocation = {};
  
  currentSelectCar = 1;
  searchCarList: carlisting;
  routesLeg: any;
  book_car_hidden: boolean = false;
  icon_name_v = "ios-arrow-dropdown-outline";

  showSetC =true;
  showDriver =false;
  showVote =false;
  Set_Tollway ="Tollway";
  tollWay =true; 
  icon_tollway="";
  icon_tollway_color="";
  icon_text_color ="";

  data_driver ={
    ratings : 5,
    driverId : "",
    driverName :"",
    driverImg :"",
    driverPhone :"",
    CarReg :""
  }

  constructor(public modalCtrl: ModalController, 
    public navCtrl: NavController, 
    private pDriver: DriverProvider,
    public loadingCtrl : LoadingController,
    private toastCtrl: ToastController, 
    private pAccount: AccountProvider,
    private callNumber: CallNumber,  
    private pOrder: OrderProvider) { 
      //this.SetCurrentLocation();
  }

  viewTerm() { 
    this.navCtrl.push(TermPage);
  }
     
  ionViewWillLeave() {
    this.runJob =false;
    clearInterval(this.timerInterval); 
    console.log('leave view');
  }
 
  getIconName(index: number) {
    if(index < this.data_driver.ratings)
    {
      return "star";
    }
    else{
      return "ios-star-outline";
    }
  }

  getColor(index: number) { 
    if(index < this.data_driver.ratings)
    {
       return '#ffa500';
    }
  }


  VoteN = 0;
  VoteText = "0";
  textNote ="";

  getIconVote(index: number) {
    if(index < this.VoteN)
    {
      return "star";
    }
    else{
      return "ios-star-outline";
    }
  }

  setColor(index: number)
  {
    if(index ==0)
    {
      this.VoteN = 1;
      this.VoteText = "1";
    }
    else if(index ==1)
    {
      this.VoteN = 2;
      this.VoteText = "2";
    }
    else if(index ==2)
    {
      this.VoteN = 3;
      this.VoteText = "3";
    }
    else if(index ==3)
    {
      this.VoteN = 4;
      this.VoteText = "4";
    }
    else if(index ==4)
    {
      this.VoteN = 5;
      this.VoteText = "5";
    }

  }


 
  ionViewDidLoad() {  

    this.gmap = GoogleMaps.create('map_canvas2', {}); 

    // this.SetCurrentLocation();
 
    this.initMap();
   
    this.addCurrentLoc();

    this.MemberType = localStorage.memberType;  
  }
 
  

  initMap() {  
  
    if(localStorage.currentLat != undefined)
    {
           // 
    }
    else
    {
      localStorage.currentLat ="13.752782";
      localStorage.currentLng ="100.522993";
    }
 
    let latLng = new google.maps.LatLng(localStorage.currentLat, localStorage.currentLng);

    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 17,
      mapTypeControl: false,
      disableDefaultUI: true,  
      center: latLng
    });   
    this.directionsDisplay.setMap(this.map); 
 
  }


  addCurrentLoc()
  {
    this.removeMarkers();
       
    let clocation = {
      lat: localStorage.currentLat,
      lng: localStorage.currentLng,
      place_a: localStorage.currentPlaceA,
      place_b: localStorage.currentPlaceB 
    };

    console.log(clocation);
 
    this.currentlocation = clocation.place_a;
    this.currentlocation_b = clocation.place_b;

    this.locationRequest.ipStart = clocation.lat + "," + clocation.lng; 
    this.addMarker( clocation.lat,clocation.lng, clocation.place_b);
    this.userLocation = clocation;
  }


  addMarker(lat:string,lng:string, st_location :string)
  {
    var infowindow = new google.maps.InfoWindow(); 
    var marker;

    let that =this;
 
    let setText ="";
    let img_url =""; 
    setText ="Your location";
    img_url ="assets/images/pickup.png";
    let latLng = new google.maps.LatLng(lat, lng);

    marker = new google.maps.Marker({
      position: new google.maps.LatLng(lat,lng),
      map: this.map,
      draggable:true,
      animation: google.maps.Animation.DROP,  
      icon: {
        url: img_url,
        size: {
            width: 48,
            height: 48
         }
       }
    }); 
    this.gmarkers.push(marker); 
    this.map.setZoom(17);
    this.map.setCenter(latLng); 
 
    google.maps.event.addListener(marker, 'dragend', function(evt){
    
      that.getPosition(marker.getPosition());
  
      let glat  = evt.latLng.lat().toFixed(8);
      let glong  = evt.latLng.lng().toFixed(8); 

      localStorage.currentLat = glat;
      localStorage.currentLng = glong; 
           
    });


    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() { 
        infowindow.setContent("<div style='padding:8px;'><p>Move Your location   </p><a href='http://maps.google.com/maps?q=" + localStorage.currentLat + "," + localStorage.currentLng + "'>Open Google maps</a></div>"); 
        // infowindow.setContent("<div style='padding:8px;'><p>Move Your location   </p><a href='maps:" + localStorage.currentLat + "," + localStorage.currentLng + "?q=" + localStorage.currentLat + "," + localStorage.currentLng + "'>Open Google maps</a></div>"); 
        infowindow.open(this.map, marker); 
      }
    })(marker, setText));

  
  }

 
  
  getPosition(pos) {
 
    let that =this;

    let geocoder = new google.maps.Geocoder;
    geocoder.geocode({ latLng: pos }, (results, status) => {
      //console.log(status);
     // console.log(results);
      let content = 'Your location : ' + results[0].formatted_address;
      this.currentlocation = results[0].address_components[0].long_name + ' ' + results[0].address_components[1].long_name;
      this.currentlocation_b = results[0].formatted_address;
      localStorage.currentPlaceA =results[0].address_components[0].long_name + ' ' + results[0].address_components[1].long_name;
      localStorage.currentPlaceB =results[0].formatted_address;  

     // that.presentToast(content);

    }); 
 
  }

  getMapCenter(){
    return this.map.getCenter()
  }
    
   

  SetCurrentLocation() { 

    let loader = this.loadingCtrl.create({  
      spinner: 'circles',
      cssClass: 'transparent'
    });
    loader.present();

    //this.gmap.clear();
    this.gmap.getMyLocation()
    .then((location: MyLocation) => {
     // console.log(JSON.stringify(location, null ,2));

      localStorage.currentLat = location.latLng.lat;  
      localStorage.currentLng = location.latLng.lng;  

      // let clocation = {
      //   lat: location.latLng.lat,
      //   lng: location.latLng.lng
      // };

      let geocoder = new google.maps.Geocoder;
        geocoder.geocode({ 'location': location.latLng }, (results, status) => { 
          //console.log(results); 
          localStorage.currentPlaceA =results[0].address_components[0].short_name + ' ' + results[0].address_components[1].short_name;
          localStorage.currentPlaceB =results[0].formatted_address;         
        }); 

        this.pAccount.UserLoginApp().then(r => {  
        //  console.log(r);  
        }); 

        this.addCurrentLoc();

        loader.dismiss();

    }).catch((error) => {
      //alert('Error getting location');
      console.log(error);
      loader.dismiss();
    }); 
  }
 
  geocodePosition(clocation)
  {
    alert(clocation); 
  }
 

  actionbtn()
  {

    if (this.showVote) {
      this.showVote =false;
      this.hide_view_Vote =true; 
    }

    if (!this.searchState) {

      this.showSetC =false;

      this.directionsDisplay.setDirections({routes: []});
      this.directionsDisplay.setMap(null); 
      this.initMap(); 

      this.tollWay =true; 
      this.Set_Tollway ="Tollway";
      this.icon_tollway="custom-tollways";
      this.icon_tollway_color="bBlue";
      this.icon_text_color ="sp_color_a";

      let clocation = {
        lat: localStorage.currentLat,
        lng: localStorage.currentLng
      };

      let profileModal = this.modalCtrl.create(HomeSearchPage,
        {
          location_a: this.currentlocation,
          location_b: this.currentlocation_b,
          userlocation: clocation
        }
      );
      profileModal.onDidDismiss(data => {
        if (data != null) {
          this.locationLabel = data.label;
          this.locationRequest.ipStart = data.select.origin.lat + "," + data.select.origin.lng;
          this.locationRequest.ipEnd = data.select.desti.lat + "," + data.select.desti.lng;
 
          this.dataReq.start_location = data.label.origin_a; 
          this.dataReq.destination = data.label.destination_a; 
          this.dataReq.expressway ="1";
 
          this.openSearchState(); 
          this.calculateAndDisplayRoute();

        } else {
          this.clearSearchState();
          this.clean(); 
        }
      });
      profileModal.present().then(() => { });
    } else { 
      this.clearSearchState(); 
      this.clean(); 
    } 
  }


  clean() {   
    this.showSetC =true;
    this.dataReq.start_location_lat= "";
    this.dataReq.start_location_long= "";
    this.dataReq.start_location= "";
    this.dataReq.destination_lat= "";
    this.dataReq.destination_long= "";
    this.dataReq.destination= "";
    this.dataReq.car_type= ""; 
    this.dataReq.id_payment= "";  
    this.dataReq.fare= "";  
    this.dataReq.note= "";  
    this.dataReq.expressway="1";
   
    this.directionsDisplay.setDirections({routes: []});
    this.directionsDisplay.setMap(null); 
    this.initMap();    
    this.addCurrentLoc(); 
    // let directionsDisplay = new google.maps.DirectionsRenderer;
    // directionsDisplay.set('directions', null);   
  }

  openSearchState() {
    this.searchState = true;
  }

  clearSearchState() {
    this.searchState = false;   
  }

 
  hidden_book_car() {
    this.book_car_hidden = !this.book_car_hidden;
    if (this.book_car_hidden) {
      this.icon_name_v = "ios-arrow-dropup-outline";
    }
    else {
      this.icon_name_v = "ios-arrow-dropdown-outline";
    }
  }


  /*
{
  origin: LatLng | String | google.maps.Place,
  destination: LatLng | String | google.maps.Place,
  travelMode: TravelMode,
  transitOptions: TransitOptions,
  drivingOptions: DrivingOptions,
  unitSystem: UnitSystem,
  waypoints[]: DirectionsWaypoint,
  optimizeWaypoints: Boolean,
  provideRouteAlternatives: Boolean,
  avoidFerries: Boolean,
  avoidHighways: Boolean,
  avoidTolls: Boolean,
  region: String
}
  */

  calculateAndDisplayRoute() {
    //console.log(this.locationRequest)


    let loader = this.loadingCtrl.create({  
      spinner: 'circles',
      cssClass: 'transparent'
    });
    loader.present();

    //console.log(this.tollWay);

    if(this.tollWay)
    {
      this.icon_tollway="custom-tollways";
      this.icon_tollway_color="bBlue";
      this.icon_text_color ="sp_color_a";
    }
    else
    {
      this.icon_tollway="custom-tollways";
      this.icon_tollway_color="dark";
      this.icon_text_color ="sp_color_b";
    }

    let setObj = {
      origin: this.locationRequest.ipStart,
      destination: this.locationRequest.ipEnd,
      travelMode: 'DRIVING',
      avoidTolls: !this.tollWay
    }
 
    this.directionsService.route(setObj, (response, status) => {
      if (status === 'OK') {
        
        //console.log(response);  
        //console.log(this.directionsDisplay);

        this.directionsDisplay.setDirections(response);
        this.locationRequest.ipStartDetail = response.routes[0].legs[0].start_address;
        this.locationRequest.ipEndDetail = response.routes[0].legs[0].end_address;
        this.locationRequest.ipDistance = response.routes[0].legs[0].distance.text;
        this.locationRequest.ipDuration = this.GetTextTimes((response.routes[0].legs[0].duration.value /60));
        //response.routes[0].legs[0].duration.text;  
        this.locationRequest.ipDistance_v =(response.routes[0].legs[0].distance.value /1000) +"";
        this.locationRequest.ipDuration_v = (response.routes[0].legs[0].duration.value /60) + ""; 
        this.routesLeg = response.routes[0].legs[0];
 
        this.getPaymentList();
        this.getOrderCalculate();
        this.Driving();

        setTimeout(x => {  
        loader.dismiss();
        }, 1000);
      } else {
        // window.alert('Directions request failed due to ' + status);
      }
    });
  }

  GetTextTimes(vDuration)
  {
    let txt = "";


    txt = parseInt(vDuration) + " - " + parseInt(vDuration + 12)  + " นาที";


    return txt;

  }

  ChangeTollway()
  {

    // Disable Tollway or Enable Tollway
    let text_set = "Enable Tollway";
    if(this.tollWay)
    {
      text_set = "Disable Tollway";
    }

    let set_Popup = { 
      title: "Confirm Changed avoid Tollway",
      text_message : text_set,
      btn_cancel : "Cancel",
      btn_confirm : "Confirm",
      isCancel : true
    }; 

    
    let popModal = this.modalCtrl.create(PopupPage,{
      setPopup: set_Popup}
      );
    popModal.onDidDismiss(popup => {
     // console.log(popup);  
      if(popup)
      {
        this.SetTollway();
      }
    })
    popModal.present(); 
  }

  SetTollway()
  {
    this.tollWay =!this.tollWay;
    if(this.tollWay)
    {
      this.Set_Tollway ="Tollway";
      this.icon_tollway="custom-tollways";
      this.icon_tollway_color="bBlue";
      this.icon_text_color ="sp_color_a";
      this.dataReq.expressway ="1";
    }
    else{
      this.Set_Tollway ="Tollway";
      this.icon_tollway="custom-tollways";
      this.icon_tollway_color="dark";
      this.icon_text_color ="sp_color_b";
      this.dataReq.expressway ="";
    }
    this.directionsDisplay.setDirections({routes: []});
    this.directionsDisplay.setMap(null); 
    this.initMap();  
    this.calculateAndDisplayRoute();
  }

  getOrderCalculate()
  {
    this.pOrder.OrderCalculate(this.locationRequest.ipDuration_v,
      this.locationRequest.ipDistance_v).then(r => {
        //console.log(r);
        this.OrderCalList.name = r.data[0].name;
        this.OrderCalList.icon = r.data[0].icon;
        this.OrderCalList.fare = r.data[0].fare;
        this.dataReq.car_type = r.data[0].id;
        this.dataReq.fare = r.data[0].fare; 
      });
  }

  getPaymentList()
  {
    this.pAccount.UserPaymentListing().then(r => {
      if (r.status == 200 && r.message == "Success") { 
       // console.log(r.data);
       if(r.data.length >0)
       {
        if(r.data[0].card_number == 'Cash')
        {
         this.Payment =r.data[0].card_number;
        }
        else{
         this.Payment =  r.data[0].card_number.substr(r.data[0].card_number.length - 5);
        } 
 
        this.dataReq.id_payment = r.data[0].id; 
       }
       
      }
    });
  }


  ChangePlace()
  {
      let popModal = this.modalCtrl.create(SelectPlace
      );
      popModal.onDidDismiss(selectPlace => {
        //console.log(selectPlace); 
        if (selectPlace != null) {  

        } 
      })
      popModal.present();
  }

  ChangePayment() {
    // let profileModal = this.modalCtrl.create(PaymentPage);
    // profileModal.onDidDismiss(selectPayment => {
    //   console.log(selectPayment);
    // })
    // profileModal.present();
    
    let popModal = this.modalCtrl.create(SelectPayment,
      {id_payment: this.dataReq.id_payment});

    popModal.onDidDismiss(selectPayment => {
     // console.log(selectPayment);
      if (selectPayment != null) { 

        if(selectPayment.card_number != 'Cash')
        {
          this.Payment =  selectPayment.card_number.substr(selectPayment.card_number.length - 5);
        }
        else{
          this.Payment =  selectPayment.card_number;
        }
      
        this.dataReq.id_payment = selectPayment.id; 
      } 
    })
    popModal.present();

  }


  ChangeCars() { 
    let popModal = this.modalCtrl.create(SelectCars,
          {
            Duration: this.locationRequest.ipDuration_v, 
            Distance: this.locationRequest.ipDistance_v
          }
      );
    popModal.onDidDismiss(selectCars => {
      //console.log(selectCars); 
      if (selectCars != null) { 
        this.OrderCalList.name = selectCars.name;
        this.OrderCalList.icon = selectCars.icon;
        this.OrderCalList.fare = selectCars.fare; 
        this.dataReq.car_type = selectCars.id; 
        this.dataReq.fare = selectCars.fare; 
      } 
    })
    popModal.present();

  }


  ChangeOption() { 
    let popModal = this.modalCtrl.create(SelectOption, {text: this.dataReq.note});
    popModal.onDidDismiss(selectOption => {
      //console.log(selectOption);
      if (selectOption != null) {  
      //alert(selectOption);
      this.dataReq.note = selectOption;
      }
    })
    popModal.present();

  }


  ChangePromotion() {
    // let profileModal = this.modalCtrl.create(PaymentPage);
    // profileModal.onDidDismiss(selectPayment => {
    //   console.log(selectPayment);
    // })
    // profileModal.present();  
  }


  bookOrder: any;

  checkPaymentBook()
  {

    console.log(this.dataReq); 
    if(this.dataReq.id_payment == "")
    { 
      this.popCheckPaymentBook("Please help to Add payment method");
    }
    else
    {
      this.BookCar();
    }

  }

  popCheckPaymentBook(msg:string) { 
    let set_Popup = { 
      title: "Pop-up Request",
      text_message : msg,
      btn_cancel : "",
      btn_confirm : "Done",
      isCancel : false
    }; 

    
    let popModal = this.modalCtrl.create(PopupPage,{
      setPopup: set_Popup}
      );
    popModal.onDidDismiss(popup => {
      console.log(popup);  
      this.clearSearchState(); 
      this.clean();
      this.GotoPaymentPage();
    })
    popModal.present(); 
  }

  GotoPaymentPage()
  { 
    let addCardModal = this.modalCtrl.create(PaymentPage);
    addCardModal.onDidDismiss(data => {
      if (data != null) {
        
      }
    });
    addCardModal.present();
  }


  dTimes ="5";

  BookCar() 
  {
    this.clearSearchState(); 
    this.dataReq.start_location_lat = this.routesLeg.start_location.lat();
    this.dataReq.start_location_long = this.routesLeg.start_location.lng();
    //this.dataReq.start_location = this.routesLeg.start_address;
    this.dataReq.destination_lat = this.routesLeg.end_location.lat();
    this.dataReq.destination_long = this.routesLeg.end_location.lng();
    
    if(this.tollWay)
    {
      this.dataReq.expressway ="1";
    }
    else{
      this.dataReq.expressway ="0";
    }

    //this.dataReq.destination = this.routesLeg.end_address; 
     //this.dataReq.car_type = "1";
     //this.dataReq.id_payment = "1";
     //this.dataReq.fare = "99";
     //this.dataReq.note = "Limo Booking";  
     //this.dataReq.expressway ="1";

    /*
    var dataReq = {
      start_location_lat: this.routesLeg.start_location.lat(),
      start_location_long: this.routesLeg.start_location.lng(),
      start_location: this.routesLeg.start_address,
      destination_lat: this.routesLeg.end_location.lat(),
      destination_long: this.routesLeg.end_location.lng(),
      destination: this.routesLeg.end_address,
      car_type: "1", // Hard Code
      id_payment: "1", // Hard Code
      fare: "99", // Hard Code
      note: "xxxx"
    };
    */  

    //console.log(this.dataReq); 
    this.pOrder.OrderBook(this.dataReq).then(r => {
      if(r.status == 200)
      {
        this.bookOrder = r.data[0];
        //console.log(this.bookOrder);
        localStorage.setBook = this.bookOrder; 
        //this.directionsDisplay.setMap(null);  
        this.WaitDriver();
      }
      else
      {
        this.openPopAlertMessage(r.message);
      } 

    });

    this.bookState = true;
  }


  openPopAlertMessage(msg:string) { 
    let set_Popup = { 
      title: "Pop-up Request",
      text_message : msg,
      btn_cancel : "",
      btn_confirm : "Done",
      isCancel : false
    }; 

    
    let popModal = this.modalCtrl.create(PopupPage,{
      setPopup: set_Popup}
      );
    popModal.onDidDismiss(popup => {
      console.log(popup);  
      
    })
    popModal.present(); 
  }


  openPopAlertMessageGPS(msg:string) { 
    let set_Popup = { 
      title: "Pop-up Request",
      text_message : msg,
      btn_cancel : "",
      btn_confirm : "Done",
      isCancel : false
    }; 

    
    let popModal = this.modalCtrl.create(PopupPage,{
      setPopup: set_Popup}
      );
    popModal.onDidDismiss(popup => {
      console.log(popup); 
      this.SetCurrentLocation(); 
      
    })
    popModal.present(); 
  }


  presentToast(mstr :string) {
    let toast = this.toastCtrl.create({
      message: mstr,
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  isomise = false;

  WaitDriver()
  {
    this.timerInterval = setInterval(() => {    

      console.log("runJob: " +this.runJob);  
      if(!this.runJob)
      { 
         
        this.pOrder.OrderGet(this.bookOrder.id).then(r => {
          if(r.status == 200)
          { 
            console.log(r.data);
            this.bookOrder = r.data[0]; 
           //console.log(this.bookOrder); 

           let dt_order = r.data[0];
           let dt_user = r.data['user'];
           let dt_driver = r.data['driver'];
           let dt_payment = r.data['payment'];
 
            localStorage.setBook = this.bookOrder; 
           // 0 Cancel or Fail, 1 Success, 2 Find Driver, 3 Driving && Waiting For Payment
            if(this.bookOrder.status =="3")
            { 
              //console.log(r.data);  
              let s_pay = false;
 
              if(dt_payment[0].payment_id)
              { 
                console.log(1);  
                if(dt_payment[0].card_number =="Cash")
                {
                  s_pay = true; 
                }
                else
                {
                  if(dt_payment[0].status_payment =="0")
                  {
                    this.runJob =false; 
                  }
                  else if(dt_payment[0].status_payment =="1")
                  {
                    s_pay = true;  
                  }  
                  else if(dt_payment[0].status_payment =="2")
                  {   
                      this.runJob =false;
                      clearInterval(this.timerInterval);  
                      this.CancelCar(); 
                      this.showDriver =false;  
                      this.bookState = false;
                      this.directionsDisplay.setDirections({routes: []});
                      this.directionsDisplay.setMap(null); 
                      this.showSetC =true;
                      this.initMap();    
                      this.addCurrentLoc();  
                  } 
                  else if(dt_payment[0].status_payment =="3")
                  {
                    if(!this.isomise)
                    {
                      this.isomise=true;
                      //console.log(2); 
                      let url = dt_payment[0].authorize_uri;
                      //this.launch(url);
                      this.runJob =true;
                      this.OpenOmise(url); 
                    }
                    else
                    { 
                      this.isomise=false;
                      this.runJob =false;
                      clearInterval(this.timerInterval);  
                      this.CancelCar(); 
                      this.showDriver =false;  
                      this.bookState = false;
                      this.directionsDisplay.setDirections({routes: []});
                      this.directionsDisplay.setMap(null); 
                      this.showSetC =true;
                      this.initMap();    
                      this.addCurrentLoc();  
                    }
                  } 
                }
              } 
               
              if(s_pay)
              {
                if(this.setFirst)
                {
                  this.directionsDisplay.setDirections({routes: []});
                  this.directionsDisplay.setMap(null); 
                }
  
                this.addPoi(r.data);  
  
                if(this.bookOrder.driver_id !="0")
                {
                  this.data_driver.driverId =  r.data['driver'].id;
                  this.data_driver.driverName =  r.data['driver'].name;
  
                  if(r.data['driver'].image)
                  {
                    this.data_driver.driverImg =  r.data['driver'].image; 
                  }
                  else{
                    this.data_driver.driverImg = "assets/images/images.jpg";
                  }


                  let dlat = r.data['driver'].lat +"";
                  let dlng = r.data['driver'].long +""; 
                  let sLoc = this.bookOrder.start_location;

                  //console.log( sLoc + "  " + dlat +',' +dlng);

 
                  this.dTimes ="10";

                  this.GetPlaceGetDuration(sLoc,dlat,dlng);
                  
                  this.data_driver.CarReg =  r.data['driver'].car;
                  this.data_driver.driverPhone =  r.data['driver'].phone; 
                  this.data_driver.ratings =  parseInt(r.data['driver'].rate);
                  this.showDriver =true;  
                  this.bookState = false;
                }
              }
 
            }
            else if(this.bookOrder.status =="1")
            {
              this.runJob =false;
              clearInterval(this.timerInterval); 
              this.showDriver =false;  
              this.bookState = false;
              this.directionsDisplay.setDirections({routes: []});
              this.directionsDisplay.setMap(null); 
              this.showSetC =true;
              this.initMap();    
              this.addCurrentLoc(); 
              this.ShowVote();
            }
            else if(this.bookOrder.status =="0")
            {
              this.bookState = false; 
              clearInterval(this.timerInterval); 
              this.directionsDisplay.setDirections({routes: []});
              this.directionsDisplay.setMap(null); 
              this.showSetC =true;
              this.initMap();    
              this.addCurrentLoc();  
            }

          }  
        });
        
        //this.clean(); 
        //this.SetCurrentLocation();   
        //this.showDriver =true;  
      } 
    }, 10000); 
  }
 

  GetPlaceGetDuration(sLoc: string, dLat: string, dLong: string)
  {
     
    this.pDriver.DriverLoc(dLat,dLong).then(r => {
      if(r)
      { 
        console.log(r);  
        //let dLoc = r.results[0].address_components[0].short_name;
        let dLoc = r.results[0].formatted_address;
        console.log(dLoc);
        this.GetDuration(sLoc,dLoc);
      } 
    });
  
  }

  GetDuration(sLoc: string, dLoc: string)
  {
    console.log(sLoc +', ' + dLoc);

    let setObj = {
      origin: sLoc,
      destination: dLoc,
      travelMode: 'DRIVING',
      avoidTolls: false
    }

    this.directionsService.route(setObj, (response, status) => {
      if (status === 'OK') {
        
        console.log(response);   
        //this.directionsDisplay.setDirections(response);
        //this.locationRequest.ipStartDetail = response.routes[0].legs[0].start_address;
        //this.locationRequest.ipEndDetail = response.routes[0].legs[0].end_address;
        //this.locationRequest.ipDistance = response.routes[0].legs[0].distance.text;
        //this.dTimes = this.GetTextTimes((response.routes[0].legs[0].duration.value /60));
        //response.routes[0].legs[0].duration.text;  
        //this.locationRequest.ipDistance_v =(response.routes[0].legs[0].distance.value /1000) +"";
        if((response.routes[0].legs[0].duration.value /60)>5)
        { 
          this.dTimes = (response.routes[0].legs[0].duration.value /60).toFixed(0) + ""; 
        }
        else{ 
          this.dTimes = "5"; 
        }
        //this.routesLeg = response.routes[0].legs[0];
   
      } else {
        // window.alert('Directions request failed due to ' + status);
        this.dTimes = "10";
      }
    });
  }

  ShowVote()
  { 
    this.hide_view_Vote =false; 
    this.showVote =true;
    this.showSetC =false;
    this.VoteN = 0;
    this.textNote ="";
  }

  hide_view_Vote =true;

  closeVote()
  {
    this.hide_view_Vote =!this.hide_view_Vote;   
  }

  SaveVote()
  {
    this.hide_view_Vote =true; 
    this.showVote =false; 
    this.showSetC =true;

    let rate = this.VoteText;
    let comment = this.textNote;

    this.pDriver.DriverVote(this.bookOrder.id, rate, comment).then(r => { 
      if (r.status == 200) {  
      }
    });

  }

  setFirst=true;

  OpenOmise(url_) {
    let profileModal = this.modalCtrl.create(PaymentOmisePage , { url: url_ });
    profileModal.onDidDismiss(data => { 
      this.runJob =false;

      this.pOrder.OrderGet(this.bookOrder.id).then(r => {
        if(r.status == 200)
        { 
           //console.log(r.data);
           //this.bookOrder = r.data[0]; 
           //console.log(this.bookOrder); 

           //let dt_order = r.data[0];
           //let dt_user = r.data['user'];
           //let dt_driver = r.data['driver'];
           let dt_payment = r.data['payment'];

           if(dt_payment[0].status_payment !="1")
           {    
               this.CancelCar();      
           } 

        }
      });

    });
    profileModal.present();
  }

  
  /*
  WaitDriver()
  {
    this.timerInterval = setInterval(() => {    
      if(!this.runJob)
      { 

        this.pOrder.OrderGet(this.bookOrder.id).then(r => {
          if(r.status == 200)
          { 
            this.bookOrder = r.data[0]; 
           //console.log(this.bookOrder); 
           
           localStorage.setBook = this.bookOrder; 
            if(this.bookOrder.status =="3")
            {
             
              console.log(r.data); 
              if(this.setFirst)
              {
                this.directionsDisplay.setDirections({routes: []});
                this.directionsDisplay.setMap(null); 
              }

              this.addPoi(r.data);  
              
              if(this.bookOrder.driver_id!="0")
              {
                this.data_driver.driverId =  r.data['driver'].id;
                this.data_driver.driverName =  r.data['driver'].name;
                this.data_driver.driverImg =  r.data['driver'].image; 
                this.data_driver.CarReg =  r.data['driver'].car;
                this.data_driver.driverPhone =  r.data['driver'].phone; 
                this.data_driver.ratings =  parseInt(r.data['driver'].rate);
                this.showDriver =true;  
                this.bookState = false;
              } 
            }
            else if(this.bookOrder.status =="1")
            {
              this.runJob =false;
              clearInterval(this.timerInterval); 
              this.showDriver =false;  
              this.bookState = false;
              this.directionsDisplay.setDirections({routes: []});
              this.directionsDisplay.setMap(null); 
              this.showSetC =true;
              this.initMap();    
              this.addCurrentLoc(); 
            }
          }
        });
        
        //this.clean(); 
        //this.SetCurrentLocation();   
        //this.showDriver =true;  
      } 
    }, 10000); 
  }

  */
 
 
  addPoi(dat:any){

    this.removeMarkers();
   
    this.addLoadPoi( dat['0'].start_location_lat,
    dat['0'].start_location_long,
      "Pick-up",
      dat['0'].start_location);


    this.addLoadPoi(dat['0'].destination_lat,
    dat['0'].destination_long,
        "Drop off",
        dat['0'].destination); 

    this.addLoadPoi(dat['driver'].lat,
        dat['driver'].long,
        dat['driver'].name,
        dat['driver'].car); 
 
  }


  resetMapBounds(){
    var bounds = new google.maps.LatLngBounds();
    for(var i=0;i<this.gmarkers.length;i++){
        bounds.extend(this.gmarkers[i].position);
    }
    this.map.fitBounds(bounds);  
    if(this.map.getZoom()>18){
      this.map.setZoom(18);
    }
  }


  addLoadPoi(lat:string,lng:string, setT:string, st_location :string)
  {
    var infowindow = new google.maps.InfoWindow(); 
    var marker;
 
    let setText ="";
    let img_url ="";
    if(setT == "Pick-up")
    {
      setText ="Pick-up";
      img_url ="assets/images/pickup.png";
    }
    else if(setT == "Drop off")
    {
      setText ="Drop off";
      img_url ="assets/images/drop_off.png";
    }
    else 
    {
      // Driver
      setText =setT +"";
      img_url ="assets/images/taxi.png";
    }

    marker = new google.maps.Marker({
      position: new google.maps.LatLng(lat,lng),
      map: this.map,
      icon: {
        url: img_url,
        size: {
            width: 48,
            height: 48
         }
       }
    });

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent("<div style='padding:8px;'><p>"+ setText +"</p><p>" + st_location +"</p>" + "<a href='http://maps.google.com/maps?q=" + lat + "," + lng + "'>Open Google maps</a></div>");   
        //infowindow.setContent("<div style='padding:8px;'<p>"+ setText +"</p><p>" + st_location +"</p>" + "<a href='maps:" + lat + "," + lng + "?q=" + lat + "," + lng + "'>Open Google maps</a></div>");   
        infowindow.open(this.map, marker);
      }
    })(marker, setT));


    this.gmarkers.push(marker); 
    this.bounds.push(marker.position);

    // show map, open infoBox 
    // google.maps.event.addListenerOnce(this.map, 'tilesloaded', function() {
    //   infowindow.setContent("<p>"+ setText +"</p><p>" + st_location +"</p>" + "");
    //   infowindow.open(this.map, marker);
    // });

    // let clocation = {
    //   lat: lat,
    //   lng: lng
    // };

    // let geocoder = new google.maps.Geocoder;
    // geocoder.geocode({ 'location': clocation }, (results, status) => { 
    //     let contentText =  results[1].formatted_address;
    // });
 

    if(setT != "Pick-up" && setT != "Drop off")
    {
      let latLng = new google.maps.LatLng(lat, lng); 
      if(this.setFirst)
      { 
        this.setFirst = false;  
        this.resetMapBounds();
      }

    }
 
  }


  view_Driver =false;
  viewDriver()
  {
    this.view_Driver =!this.view_Driver;  

  }

  calling() {
    //console.log(this.number); 
    this.telTo(this.data_driver.driverPhone);
  }
 
  telTo(numb) {
    this.callNumber.callNumber(numb, true);
  }

  telToII(numb) {
    window.open(`tel:${numb}`, '_system');
  }

  removeMarkers(){
    for(var i=0; i<this.gmarkers.length; i++){
      this.gmarkers[i].setMap(null);
    }
   this.gmarkers = [];
   this.bounds =[];
 }
 
  Driving()
  {
    this.pDriver.DriverDriving().then(r => {

      this.removeMarkers();
       
      if (r.status == 200) {  
         
        //console.log(r.data); 
        //var infowindow = new google.maps.InfoWindow(); 
        var marker, i;
        
        if(r.data.length > 0)
        {
          
          for (i = 0; i < r.data.length; i++) {   
            
            marker = new google.maps.Marker({
              position: new google.maps.LatLng(r.data[i].lat,r.data[i].long),
              map: this.map,
              icon: {
                url: "assets/images/taxi.png",
                size: {
                    width: 48,
                    height: 48
                 }
               }
            });

            
            this.gmarkers.push(marker);
  
            // google.maps.event.addListener(marker, 'click', (function(marker, i) {
            //   return function() {
            //     infowindow.setContent("<p>"+ r.data[i].start_location + "-" + r.data[i].destination + "  THB " + r.data[i].fare +"</p>" +
            //       "Note: " + r.data[i].note
            //     );
            //     infowindow.open(this.map, marker);
            //   }
            // })(marker, i));
  
           }
        } 
      }  
    }); 
  }

  CancelCar() {
    this.pOrder.OrderCancel(this.bookOrder.user_id, this.bookOrder.id).then(r => {
      this.bookState = false;
      this.directionsDisplay.setDirections({routes: []});
      this.directionsDisplay.setMap(null); 
      this.showSetC =true;
      this.initMap();    
      this.addCurrentLoc(); 
    });
  }
}

interface carlisting {
  fare: number;
  icon: string;
  name: string;
}

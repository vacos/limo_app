import { Component} from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { AccountProvider } from '../../providers/account/account';
 

@Component({
    selector: 'page-select-payment',
    templateUrl: 'modal-select-payment.html'
})
export class SelectPayment {
   
    paymentList = [];
    id_payment ="";
    constructor(public viewCtrl: ViewController,
        public navParams: NavParams ,
        private pAccount: AccountProvider,) {  
        this.id_payment = navParams.get('id_payment');
        this.initPaymentLists();
    }


    initPaymentLists() {
        this.pAccount.UserPaymentListing().then(r => {
          if (r.status == 200 && r.message == "Success") {
            this.paymentList = r.data;
          }
        });
      }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    chooseItem(item: any) {
        //console.log('modal > chooseItem > item > ', item);
        this.viewCtrl.dismiss(item);
    }
     
}

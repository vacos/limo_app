import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular'; 
import { AuthenticationPage } from '../authentication/authentication';
import { AccountProvider } from '../../providers/account/account';

/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {

  emailAddress = "";
  mobilePhone = "";  
  userName = "";
  image_account ="";
  favoritePlace = [];

  constructor(public navCtrl: NavController,private pAccount: AccountProvider, 
     public navParams: NavParams,public viewCtrl: ViewController) {
  }

  ionViewDidLoad(){
    this.loadData();
    this.loadFavoritePlace();
  }

  loadFavoritePlace()
  {
    this.favoritePlace.push({
      label_a: "สนามบินสุวรรณภูมิ",
      label_b: "999 หมู่ 1 Nong Prue, Amphoe Bang Phli, Chang Wat Samut Prakan 10540"
    });

    this.favoritePlace.push({
      label_a: "สนามบินดอนเมือง",
      label_b: "222 Vibhavadi Rangsit Rd, Khwaeng Sanambin, Khet Don Mueang, Krung Thep Maha Nakhon 10210"
    });

  }

  loadData()
  {
    this.pAccount.UserGet().then(r => { 
      this.emailAddress = r.data.email;
      this.mobilePhone = r.data.phone;  
      this.userName = r.data.name;

      if(r.data.image)
      {
        this.image_account = r.data.image;
      }
      else
      {
        this.image_account ="assets/images/images.jpg";
      }
      
    });
  }
 
  OnClick_Logout(){
    this.navCtrl.setRoot(AuthenticationPage);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}

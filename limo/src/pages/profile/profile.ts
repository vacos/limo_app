import { Component } from '@angular/core';
import { ViewController, ActionSheetController, LoadingController, ToastController, Platform, Events, ModalController } from 'ionic-angular';
import { AccountProvider } from '../../providers/account/account';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { FileTransfer, FileTransferObject, FileUploadOptions } from '@ionic-native/file-transfer';
import { PopupPage } from '../modal-popup/modal-popup';
import { PhoneUpdatePage } from '../phone-update/phone-update';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  imageURI: any;
  base64Image;
  ipFirstName = "";
  ipLastName = "";
  ipEmail = "";
  ipMobile = "";
  ipPassword = "";
  userName = "";
  image_account = "assets/images/images.jpg";

  fnameValidate = false;
  lnameValidate = false;
  emailValidate = false;
  passwordValidate = false;
  message_fname_validate = "";
  message_lname_validate = "";
  message_email_validate = "";
  message_password_validate = "";

  constructor(private pAccount: AccountProvider,
    public actionSheetCtrl: ActionSheetController,
    private transfer: FileTransfer,
    private camera: Camera,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public events: Events,
    public modalCtrl: ModalController,
    public platform: Platform,
    public viewCtrl: ViewController) {
  }

  Save() {

    let vret = true;

    if (this.ipFirstName =="") {
      this.fnameValidate = true;
      this.message_fname_validate = "Enter your Firstname";
      vret = false;
    }
    else {
      this.fnameValidate = false;
      this.message_fname_validate = "";
    }

    if (vret) {
      if (this.ipLastName =="") {
        this.lnameValidate = true;
        this.message_lname_validate = "Enter your Lastname";
        vret = false;
      }
      else {
        this.lnameValidate = false;
        this.message_lname_validate = "";
      }
    }

    if (vret) {
      if (!this.emailValidator(this.ipEmail)) {
        this.emailValidate = true;
        this.message_email_validate = "Check your email address";
        vret = false;
      }
      else {
        this.emailValidate = false;
        this.message_email_validate = "";
      }
    }

    if (vret) {

      if (this.ipPassword != "") {
        if (!this.passwordValidator(this.ipPassword)) {
          this.passwordValidate = true;
          this.message_password_validate = "Enter your password, Minimum 5 charactor";
          vret = false;
        }
        else {
          this.passwordValidate = false;
          this.message_password_validate = "";
        }
      }

    }


    if (vret) {
      this.goSave();
    }

  }

  goSave() {
    localStorage.userName = this.ipFirstName + " " + this.ipLastName;
    let userid = localStorage.userId;

    if (this.ipPassword.trim() !== "") {
      this.pAccount.UserUpdate({
        id: userid,
        name: this.ipFirstName + " " + this.ipLastName,
        email: this.ipEmail,
        password: this.ipPassword
      }).then(r => {
        var datas = {
          name: this.ipFirstName + " " + this.ipLastName,
          email: this.ipEmail
        }
        
        //this.viewCtrl.dismiss(datas);
        this.openPopAlertUpdateComplete("แก้ไขข้อมูลเสร็จเรียบร้อยแล้ว",datas);

      });
    }
    else {
      this.pAccount.UserUpdate({
        id: userid,
        // phone: this.ipMobile, 
        name: this.ipFirstName + " " + this.ipLastName,
        email: this.ipEmail
      }).then(r => {
        var datas = {
          name: this.ipFirstName + " " + this.ipLastName,
          email: this.ipEmail
        }
        
        //this.viewCtrl.dismiss(datas);
        this.openPopAlertUpdateComplete("แก้ไขข้อมูลเสร็จเรียบร้อยแล้ว",datas);

      });
    }
  }


  openPopAlertUpdateComplete(msg:string,datas: any) { 
    let set_Popup = { 
      title: "Pop-up Request",
      text_message : msg,
      btn_cancel : "",
      btn_confirm : "Done",
      isCancel : false
    }; 
 
    let popModal = this.modalCtrl.create(PopupPage,{
      setPopup: set_Popup}
      );
    popModal.onDidDismiss(popup => {
      //console.log(popup);  
      this.viewCtrl.dismiss(datas);
    })
    popModal.present(); 
  }

  
  ionViewDidLoad() {
    this.loadData();
  }

  loadData() {
    this.pAccount.UserGet().then(r => {
      this.ipFirstName = r.data.name.split(' ')[0];
      this.ipLastName = r.data.name.split(' ')[1];
      this.ipEmail = r.data.email;
      this.ipMobile = r.data.phone;
      this.userName = r.data.name;

      if (r.data.image) {
        this.image_account = r.data.image;
      }

    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  UploadImg() {

    if (this.platform.is('android')){


      let actionSheet = this.actionSheetCtrl.create({
        title: 'Select',
        buttons: [
          {
            text: 'PHOTO Library',
            handler: () => {
              this.getPHOTOLIBRARY();
            }
          },
          {
            text: 'Take Picture',
            handler: () => {
              this.getCammeraTakePicture();
            }
          },
          {
            text: 'Cancel',
            role: 'cancel'
          }
        ]
      });
      actionSheet.present();

    }
    else{


      let actionSheet = this.actionSheetCtrl.create({
        title: 'Select',
        buttons: [
          {
            text: 'PHOTO Library',
            handler: () => {
              this.getPHOTOLIBRARY();
            }
          }, 
          {
            text: 'Cancel',
            role: 'cancel'
          }
        ]
      });
      actionSheet.present();

    }



    /* 
     let uId = localStorage.userId;
     let profileModal = this.modalCtrl.create(UploadImagesPage , { userId: uId });
     profileModal.onDidDismiss(data => {
       if (data != null) {
         console.log(data);
         this.image_account = data.path; 
         this.UpdateUserImage(data.fileName);
       }
     });
     profileModal.present();
     */

  }


  getCammeraTakePicture() {
    /*
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.CAMERA
    }

    */
    

   const options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    allowEdit: false,
    correctOrientation: false,
    saveToPhotoAlbum: true
  } 

  

  /*
    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      this.uploadFile();
    }, (err) => {
      console.log(err);
    }); 


   var options: CameraOptions = {
    quality: 100,
    sourceType: this.camera.PictureSourceType.CAMERA,
    saveToPhotoAlbum: false,
    correctOrientation: true
}; 

this.camera.getPicture(options).then(imagePath => {
    if (this.platform.is('android')){
      this.imageURI = imagePath;
      this.base64Image = 'data:image/jpeg;base64,' + imagePath;
      this.uploadFile();
    } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);

      this.imageURI = correctPath;
      this.base64Image = 'data:image/jpeg;base64,' + correctPath;
      this.uploadFile();
        
    }
});
*/

 
    this.camera.getPicture(options)
    .then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      
      let base64Image_ = 'data:image/jpeg;base64,' + imageData; 
      this.base64Image = base64Image_;

      this.imageURI = imageData;
      // console.log(imageData);
      // console.log(base64Image);
      this.uploadFile();

    }, (err) => {
     // console.error('error al sacar la foto --> ' + err);
    });
  
  }



  getPHOTOLIBRARY() {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY, 
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      this.uploadFile();
    }, (err) => {
      //console.log(err);
    });

  }


  uploadFile() {
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();

    const fileTransfer: FileTransferObject = this.transfer.create();

    //console.log(this.imageURI);

    let options: FileUploadOptions = {
      fileKey: 'file_upload',
      fileName: this.imageURI.substr(this.imageURI.lastIndexOf('/') + 1),
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {}
    }

    //this.cfileName = this.imageURI.substr(this.imageURI.lastIndexOf('/') + 1); 
    //fileTransfer.upload(this.imageURI, 'http://192.168.1.105/galleryimages/upload.php', options)

    fileTransfer.upload(this.imageURI, 'https://limoapp.me/api/user/upload_image', options)
      .then((data) => {

        // console.log(JSON.parse(data.response));   

        let obj = JSON.parse(data.response);

        //console.log(obj);
        //console.log(obj.data.filename);
        //console.log(obj.data.path);

        loader.dismiss();

        if (obj.data.path != "") {
          this.image_account = obj.data.path;
          localStorage.userImage = obj.data.path;
          this.UpdateUserImage(obj.data.filename);

          //  var datas = {
          //    fileName: obj.data.filename, 
          //    image: obj.data.path
          //   }
          //  this.viewCtrl.dismiss(datas);    
        }

      }, (err) => {
        //console.log(err);
        loader.dismiss();
      });
  }



  UpdateUserImage(img: string) {
    let uId = localStorage.userId;
    //console.log(uId);
    //console.log(img);
    this.pAccount.UserUpdate({
      id: uId,
      image: img
    }).then(r => {
      this.events.publish('username:changed', this.userName);
    });
  }


  changePhone() {

    if (localStorage.memberType == "0") {
      let set_Popup = {
        title: "Change Phone Number ",
        text_message: "Do you want to Change Phone Number? ",
        btn_cancel: "Cancel",
        btn_confirm: "Ok",
        isCancel: true
      };

      let popModal = this.modalCtrl.create(PopupPage, {
        setPopup: set_Popup
      }
      );
      popModal.onDidDismiss(popup => {
        if (popup) {
          this.OpenChangedPhone();
        }
      })
      popModal.present();

    }


  }

  OpenChangedPhone() {
    let profileModal = this.modalCtrl.create(PhoneUpdatePage);
    profileModal.onDidDismiss(data => {
      if (data != null) {
        localStorage.phoneNumber = data.phone;
        this.ipMobile = data.phone;
        this.events.publish('username:changed', this.userName);
      }
    });
    profileModal.present();
  }



  emailValidator(str) {
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(str);
  }

  passwordValidator(str) {
    let regExp = /^[a-zA-Z0-9]{5,20}/;
    return regExp.test(str);
  }

  fnameValidator(str) {
    let regExp = /^[a-zA-Z ]{1,30}$/;
    return regExp.test(str);
  }

  lnameValidator(str) {
    let regExp = /^[a-zA-Z ]{1,30}$/;
    return regExp.test(str);
  }

  phoneValidator(str) {
    //let regExp = /^[0-9]{10}$/; 
    let regExp = /^([0]{1})([0-9]{2})([0-9]{3})([0-9]{4})$/;
    return regExp.test(str);
  }

}


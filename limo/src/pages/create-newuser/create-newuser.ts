import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform, Navbar } from 'ionic-angular';
import { HomePage } from '../home/home';
import { TermConditionPage } from '../term-condition/term-condition';
import { AccountProvider } from '../../providers/account/account';

@Component({
  selector: 'page-create-newuser',
  templateUrl: 'create-newuser.html',
})
export class CreateNewuserPage {

  @ViewChild(Navbar) navBar: Navbar;
  stateValidate=false;
  message_validate="";

  questionList = [
    {
      question: "What is your email address?",
      value: [{
        placeholder: "Name@example",
        type: "email",
        value: ""
      }]
    },
    {
      question: "Create your password?",
      value: [{
        placeholder: "Minimum 5 charactor",
        type: "password",
        value: ""
      },
      {
        placeholder: "Confirm password",
        type: "password",
        value: ""
      }]
    },
    {
      question: "Enter your name?",
      value: [{
        placeholder: "Firstname",
        type: "text",
        value: ""
      },
      {
        placeholder: "Lastname",
        type: "text",
        value: ""
      }]
    }
  ]
  
  currentIndex = 0;

  phoneNumber = "";
  userid = "";
  bBack = true;

  constructor(public navCtrl: NavController,
    public platform: Platform, 
    public navParams: NavParams,
    private pAccount: AccountProvider) {
    this.phoneNumber = this.navParams.get("phoneNumber");
    this.userid = this.navParams.get("userid");
  }
 
  ionViewDidLoad() {
    this.navBar.backButtonClick = (e:UIEvent)=>{
      // todo something 
      if (this.currentIndex > 0)
      {
        this.currentIndex--;
      }  
      if (this.currentIndex == 0)
      {
        this.bBack = true;
      } 

     }
     
    this.platform.registerBackButtonAction(() => {
      if (this.currentIndex > 0)
      {
        this.currentIndex--;
      }  
      if (this.currentIndex == 0)
      {
        this.bBack = true;
      } 
    }); 
  }

  dismiss()
  {
    if (this.currentIndex > 0)
    {
      this.currentIndex--;
    }  
  }

  text_color ="sp_color_b";
  ip_color="ip_color_b";
  nextState() {
   
    if (this.currentIndex < this.questionList.length-1) { 

      if (this.currentIndex == 0)
      {
        //console.log("0000000000");

        if(!this.emailValidator(this.questionList[0].value[0].value))
        {
          this.text_color ="sp_color_a";
          this.ip_color="ip_color_a";
          this.stateValidate =true;
          this.message_validate ="Check your email address"; 
        }
        else
        { 
          this.text_color ="sp_color_b";
          this.ip_color="ip_color_b";
          this.stateValidate=false; 
          this.message_validate ="";   
          this.bBack = false;
          this.currentIndex++;
        }
      }
      else if (this.currentIndex == 1)
      {
        //console.log("11111111111");
        if(!this.passwordValidator(this.questionList[1].value[0].value))
        { 
          this.text_color ="sp_color_a";
          this.ip_color="ip_color_a";
          this.stateValidate =true;
          this.message_validate ="Enter your password, Minimum 5 charactor, Password not match"; 
        }
        else
        { 
          if (this.questionList[1].value[0].value != this.questionList[1].value[1].value) { 
            
            this.text_color ="sp_color_a";
            this.ip_color="ip_color_a";
            this.stateValidate =true;
            this.message_validate ="Enter your password"; 
          } else { 
            this.text_color ="sp_color_b";
            this.ip_color="ip_color_b";
            this.stateValidate=false; 
            this.message_validate =""; 
            this.currentIndex++;
          }  
        }
      } 

    }else{ 

       //  console.log("222222222"); 

        if((this.questionList[2].value[0].value + "" !="") && (this.questionList[2].value[1].value +"" !=""))
        { 
          this.text_color ="sp_color_b";
          this.ip_color="ip_color_b";
          this.stateValidate=false; 
          this.message_validate =""; 
           
          localStorage.userName =this.questionList[2].value[0].value + " " + this.questionList[2].value[1].value;
          this.pAccount.UserUpdate({
            id: this.userid,
            _phone: this.phoneNumber,
            _facebook_id: "???",
            name: this.questionList[2].value[0].value + " " + this.questionList[2].value[1].value,
            email: this.questionList[0].value[0].value,
            _image: "",
            _uuid: "?",
            _udid: "?",
            _pushbadge: "?",
            _pushsound: "?",
            _pushalert: "?",
            device: "?",
            password: this.questionList[1].value[0].value,
          }).then(r => {
            this.navCtrl.push(TermConditionPage, { phoneNumber: r.data.phone, userid: r.data.id });
          });

        }
        else
        {  
          this.text_color ="sp_color_a";
          this.stateValidate =true;
          this.message_validate ="Enter your name"; 
        }
 
    }

  }

  nextStateff() {
    if (this.currentIndex < this.questionList.length - 1) { 
      if (this.currentIndex == 1 && (this.questionList[1].value[0].value != this.questionList[1].value[1].value)) { 
       // alert("Password miss match.");
      } else {
        this.currentIndex++;
      }
    } else {

      localStorage.userName =this.questionList[2].value[0].value + " " + this.questionList[2].value[1].value;
      
      this.pAccount.UserUpdate({
        id: this.userid,
        _phone: this.phoneNumber,
        _facebook_id: "???",
        name: this.questionList[2].value[0].value + " " + this.questionList[2].value[1].value,
        email: this.questionList[0].value[0].value,
        _image: "",
        _uuid: "?",
        _udid: "?",
        _pushbadge: "?",
        _pushsound: "?",
        _pushalert: "?",
        device: "?",
        password: this.questionList[1].value[0].value,
      }).then(r => {
        this.navCtrl.push(TermConditionPage, { phoneNumber: r.data.phone, userid: r.data.id });
      });
    }
  }

  OnClick_CreateNewUser() {
    this.navCtrl.setRoot(HomePage);
  }


  emailValidator(str) {
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(str);
  }

  passwordValidator(str) {
    let regExp = /^[a-zA-Z0-9]{5,20}/;
    return regExp.test(str);
  }

  fnameValidator(str) {
    let regExp = /^[a-zA-Z ]{1,30}$/;
    return regExp.test(str);
  }

  lnameValidator(str) {
    let regExp = /^[a-zA-Z ]{1,30}$/;
    return regExp.test(str);
  }

  phoneValidator(str) { 

    let regExp =/^([0]{1})([0-9]{2})([0-9]{3})([0-9]{4})$/;
    //let regExp = /^[0-9]{10}$/;
    return regExp.test(str);
  }



}

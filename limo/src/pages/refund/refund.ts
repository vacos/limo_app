import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { PageApiProvider } from '../../providers/page-api/page-api';

/**
 * Generated class for the YourTripPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-refund',
  templateUrl: 'refund.html',
})
export class RefundPage {

  refund_html: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private pget: PageApiProvider,
    public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    
     // legal, term, refund
    this.pget.PageApiGet("refund").then(r => {
      if(r.status == 200 && r.message == "Success"){ 
        this.refund_html= r.data.content;
      } 
    });

  }
 
 

  dismiss() {
    this.viewCtrl.dismiss();
  }

}

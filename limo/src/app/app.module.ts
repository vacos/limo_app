import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';  
 
  
import { MyApp } from './app.component';
import { AuthenticationPage } from '../pages/authentication/authentication';
import { ValidateOtpPage } from '../pages/validate-otp/validate-otp';
import { ValidatePasswordPage } from '../pages/validate-password/validate-password';
import { CreateNewuserPage } from '../pages/create-newuser/create-newuser';
import { YourTripPage } from '../pages/your-trip/your-trip';
import { PaymentPage } from '../pages/payment/payment';
import { PaymentAddPage } from '../pages/payment-add/payment-add';
import { PromotionPage } from '../pages/promotion/promotion';
import { SettingPage } from '../pages/setting/setting';
import { TermConditionPage } from '../pages/term-condition/term-condition';

import { PhoneInputPage } from '../pages/phone-input/phone-input';
import { PasswordInputPage } from '../pages/password-input/password-input';
import { ModalResendPage } from '../pages/modal-resend/modal-resend';


import { HomePage } from '../pages/home/home';
import { HomeSearchPage } from '../pages/home-search/home-search';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen'; 
import { AccountProvider } from '../providers/account/account'; 
import { RestApiProvider } from '../providers/rest-api/rest-api';

import { GlobalDeclaration } from '../Declaration';
import { OrderProvider } from '../providers/order/order';
import { LegalPage } from '../pages/legal/legal';
import { PageApiProvider } from '../providers/page-api/page-api';
import { TermPage } from '../pages/term/term';
import { RefundPage } from '../pages/refund/refund';
import { DriverProvider } from '../providers/driver/driver';
import { CountryProvider } from '../providers/country/country';
import { SelectSearchableModule } from 'ionic-select-searchable';   
import { HttpClientModule } from '@angular/common/http'; 
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AutocompletePage } from '../pages/autocomplete/autocomplete'; 
import {File} from '@ionic-native/file';
import {FileTransfer} from '@ionic-native/file-transfer';
import {FilePath} from '@ionic-native/file-path';
import {Camera} from '@ionic-native/camera';
import { ProfilePage } from '../pages/profile/profile';
import { SelectPayment } from '../pages/modal-set-card/modal-select-payment';
import { SelectCars } from '../pages/modal-cars/modal-cars';
import { SelectOption } from '../pages/modal-option/modal-option';
import { PopupPage } from '../pages/modal-popup/modal-popup';
import { SelectPlace } from '../pages/modal-place/modal-place';
import { MyplacesPage } from '../pages/myplaces/myplaces';
import { AddPlacePage } from '../pages/add-place/add-place';
import { PhoneUpdatePage } from '../pages/phone-update/phone-update';
import { OtpUpdatePhonePage } from '../pages/otp-update-phone/otp-update-phone';
import { HomeDriverPage } from '../pages/home-driver/home-driver'; 
import { NativeAudio } from '@ionic-native/native-audio';  
import { CallNumber } from '@ionic-native/call-number';
import { PrivacyPage } from '../pages/privacy/privacy';
import { AddCardPage } from '../pages/add-card/add-card';
import { MyLocationPage } from '../pages/my-location/my-location';  
import { GoogleMaps } from "@ionic-native/google-maps";
import { PaymentOmisePage } from '../pages/payment-omise/payment-omise';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    HomeDriverPage,
    HomeSearchPage,
    AuthenticationPage,
    ValidateOtpPage,
    ValidatePasswordPage,
    CreateNewuserPage,
    YourTripPage,
    PaymentPage,
    PaymentAddPage,
    PromotionPage,
    SettingPage,
    TermConditionPage,
    PhoneInputPage,
    PhoneUpdatePage,
    ModalResendPage,
    PasswordInputPage,
    LegalPage,
    TermPage,
    AutocompletePage, 
    ProfilePage,
    SelectPayment,
    SelectCars,
    SelectOption, 
    SelectPlace, 
    MyplacesPage,
    PopupPage,
    AddPlacePage,
    OtpUpdatePhonePage,
    RefundPage,
    PrivacyPage,
    AddCardPage,
    MyLocationPage,
    PaymentOmisePage
  ],
  imports: [
    BrowserModule,
    HttpModule,  
    HttpClientModule, 
    SelectSearchableModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    HomeDriverPage,
    HomeSearchPage,
    AuthenticationPage,
    ValidateOtpPage,
    ValidatePasswordPage,
    CreateNewuserPage,
    YourTripPage,
    PaymentPage,
    PaymentAddPage,
    PromotionPage,
    SettingPage,
    TermConditionPage,
    PhoneInputPage,
    PhoneUpdatePage,
    ModalResendPage,
    PasswordInputPage,
    LegalPage, 
    TermPage,
    AutocompletePage, 
    ProfilePage,
    SelectPayment,
    SelectCars,
    SelectOption,
    SelectPlace, 
    MyplacesPage,
    PopupPage,
    AddPlacePage,
    OtpUpdatePhonePage,
    RefundPage,
    PrivacyPage,
    AddCardPage,
    MyLocationPage,
    PaymentOmisePage
  ],
  providers: [ 
    GoogleMaps,
    GlobalDeclaration,
    StatusBar,
    File,
    FileTransfer,
    FilePath,
    Camera,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AccountProvider,
    RestApiProvider,
    OrderProvider,
    PageApiProvider,
    DriverProvider,
    CountryProvider,  
    InAppBrowser, 
    CallNumber,
    NativeAudio
  ]
})
export class AppModule {}



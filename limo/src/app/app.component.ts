import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ModalController, AlertController, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AuthenticationPage } from '../pages/authentication/authentication';
// import { HomePage } from '../pages/home/home';
import { YourTripPage } from '../pages/your-trip/your-trip';
import { PaymentPage } from '../pages/payment/payment';
import { AccountProvider } from '../providers/account/account';
//import { LegalPage } from '../pages/legal/legal';
import { TermPage } from '../pages/term/term';
import { RefundPage } from '../pages/refund/refund';
import { ProfilePage } from '../pages/profile/profile';
import { PopupPage } from '../pages/modal-popup/modal-popup';
import { PhoneUpdatePage } from '../pages/phone-update/phone-update';
// import { HomeDriverPage } from '../pages/home-driver/home-driver';
import { PrivacyPage } from '../pages/privacy/privacy';
import { MyplacesPage } from '../pages/myplaces/myplaces';
import { MyLocationPage } from '../pages/my-location/my-location'; 

declare var localStorage: any;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  image_account = "assets/images/images.jpg";
  pages: Array<{ title: string, component: any }>;
  MemberType = "";
  UserFullName = "";
  Phone = "";
  constructor(public events: Events,
    public platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private pAccount: AccountProvider,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController) {

      platform.ready().then(() => {
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need. 
        // statusBar.hide();
        splashScreen.hide();
      });

    this.initializeApp(); 

    this.pages = [];

    if (localStorage.memberType == "0") {
      // user
      this.pages = [
        { title: 'Limo Payment', component: PaymentPage },
        { title: 'History', component: YourTripPage },
        { title: 'My Place', component: MyplacesPage },
        // { title: 'Setting', component: SettingPage },
        // { title: 'Legal', component: LegalPage },
        { title: 'Term & Condition', component: TermPage },
        { title: 'Refund', component: RefundPage },
        { title: 'Privacy & Policy', component: PrivacyPage },
      ];
    }
    else if (localStorage.memberType == "1") {
      // banned
    }
    else if (localStorage.memberType == "2") {
      // driver
      this.pages = [
        // { title: 'Setting', component: SettingPage },
        //{ title: 'Legal', component: LegalPage },
        { title: 'Term & Condition', component: TermPage },
        // { title: 'Refund', component: RefundPage }, 
        { title: 'Privacy & Policy', component: PrivacyPage },
      ];
    }

    events.subscribe('username:changed', username => {
      if (username !== undefined && username !== "") {
        this.UserFullName = username;
        this.Phone = localStorage.phoneNumber;

        if (localStorage.userImage) {
          this.image_account = localStorage.userImage;
        }
        else {
          this.image_account = "assets/images/images.jpg";
        }

        this.pages = [];

        if (localStorage.memberType == "0") {
          this.MemberType = "Member";
          this.pages = [
            { title: 'Limo Payment', component: PaymentPage },
            { title: 'History', component: YourTripPage },
            { title: 'My Place', component: MyplacesPage },
            // { title: 'Setting', component: SettingPage },
            //{ title: 'Legal', component: LegalPage },
            { title: 'Term & Condition', component: TermPage },
            { title: 'Refund', component: RefundPage },
            { title: 'Privacy & Policy', component: PrivacyPage },
          ];
        }
        else if (localStorage.memberType == "1") {
          // Banned User
        }
        else if (localStorage.memberType == "2") {
          this.MemberType = "Driver";
          this.pages = [
            // { title: 'Setting', component: SettingPage },
            // { title: 'Legal', component: LegalPage },
            { title: 'Term & Condition', component: TermPage },
            // { title: 'Refund', component: RefundPage }, 
            { title: 'Privacy & Policy', component: PrivacyPage },
          ];
        }
      }

    });

  }




  ChangedPhone() {
    if (localStorage.memberType == "0") {
      let set_Popup = {
        title: "Change Phone Number ",
        text_message: "Do you want to Change Phone Number? ",
        btn_cancel: "Cancel",
        btn_confirm: "Ok",
        isCancel: true
      };

      let popModal = this.modalCtrl.create(PopupPage, {
        setPopup: set_Popup
      }
      );
      popModal.onDidDismiss(popup => {
        if (popup) {
          this.OpenChangedPhone();
        }
      })
      popModal.present();

    }

  }

  OpenChangedPhone() {
    let profileModal = this.modalCtrl.create(PhoneUpdatePage);
    profileModal.onDidDismiss(data => {
      if (data != null) {
        localStorage.phoneNumber = data.phone;
        this.Phone = data.phone;
      }
    });
    profileModal.present();
  }

  GotoPofile() {
    let uId = localStorage.userId;
    let profileModal = this.modalCtrl.create(ProfilePage, { userId: uId });
    profileModal.onDidDismiss(data => {
      if (data != null) {
        this.UserFullName = data.name;
      }
    });
    profileModal.present();
  }

  initializeApp() {
      
      let userId = localStorage.userId; 
      if (userId) { 
        this.relogin(); 

      } else {
        localStorage.clear();
        //this.nav.setRoot(AuthenticationPage);
        this.rootPage = AuthenticationPage;
      }
  

  }

  relogin() {
    this.pAccount.UserLogOut().then(r => {
      if (r) {
        if (r.status == 200) {
          this.nextlogin();

        }
        else {

          localStorage.clear();
          //this.nav.setRoot(AuthenticationPage);
          this.rootPage = AuthenticationPage;
        }
      }
      else {

        localStorage.clear();
        //this.nav.setRoot(AuthenticationPage);
        this.rootPage = AuthenticationPage;
      }

    });
  }

  nextlogin() {
    let _phoneNumber = localStorage.phoneNumber;
    let _Password = localStorage.Password;
    this.pAccount.UserLogin(_phoneNumber, _Password).then(r => {
      console.log(r);
      if (r) {
        if (r.status == 200) {
          this.initUserProfile();
        }
        else {

          localStorage.clear();
          //this.nav.setRoot(AuthenticationPage);
          this.rootPage = AuthenticationPage;
        }
      }
      else {

        localStorage.clear();
        //this.nav.setRoot(AuthenticationPage);
        this.rootPage = AuthenticationPage;
      }

    });
  }

  initUserProfile() {
    this.pAccount.UserGet().then(r => {

      if (r) {

        if (r.status == 200) {
          this.UserFullName = r.data.name;
          this.Phone = r.data.phone;

          if (r.data.image != "") {
            this.image_account = r.data.image;
          }
          else {
            this.image_account = "assets/images/images.jpg";
          }

          // "assets/images/images.jpg"; 

          if (r.data.type == "0") {
            this.MemberType = "Member";
            //this.rootPage = HomePage;
          }
          else if (r.data.type == "1") {
            this.MemberType = "Banned";
            //this.rootPage = HomePage;  
          }
          else if (r.data.type == "2") {
            this.MemberType = "Driver";
            //this.rootPage = HomeDriverPage;
          }

          this.rootPage = MyLocationPage;
        }
        else {

          localStorage.clear();
          //this.nav.setRoot(AuthenticationPage);
          this.rootPage = AuthenticationPage;
        }

      }
      else {
        localStorage.clear();
        //this.nav.setRoot(AuthenticationPage);
        this.rootPage = AuthenticationPage;

      }

    });
  }

  OnClick_Logout() {
    this.pAccount.UserLogOut().then(r => {

      let lat = localStorage.currentLat;
      let lng = localStorage.currentLng;

      localStorage.clear();

      localStorage.currentLat = lat;
      localStorage.currentLng = lng;

      this.nav.setRoot(AuthenticationPage);
    });
  }


  openPopLoguut() {
    let set_Popup = {
      title: "Confirm logout",
      text_message: "Do you want to logout? ",
      btn_cancel: "Cancel",
      btn_confirm: "Confirm",
      isCancel: true
    };


    let popModal = this.modalCtrl.create(PopupPage, {
      setPopup: set_Popup
    }
    );
    popModal.onDidDismiss(popup => {
      // console.log(popup);  
      if (popup) {
        this.OnClick_Logout();
      }
    })
    popModal.present();
  }


  openPage(page) {
    let profileModal = this.modalCtrl.create(page.component);
    profileModal.present();
    //this.nav.setRoot(page.component);
  }


  AlertContrlLogout() {
    let alert = this.alertCtrl.create({
      title: 'Confirm logout',
      message: 'Do you want to logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Confirm',
          handler: () => {
            this.OnClick_Logout();
          }
        }
      ]
    });
    alert.present();
  }

}

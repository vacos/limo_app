import { RestApiProvider } from '../rest-api/rest-api';
import { Injectable } from '@angular/core';
declare var localStorage: any;
@Injectable()
export class PageApiProvider {
 
  constructor(public http: RestApiProvider) { 
  }


  PageApiGet(sslug: string) {
    let obj = {
      "slug": sslug, 
    }
    return this.http.PostRequest("/page/get", obj);
  }
 

}

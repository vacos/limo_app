import { Injectable } from '@angular/core';
import { RestApiProvider } from '../rest-api/rest-api';
declare var localStorage: any; 
@Injectable()
export class AccountProvider { 
  constructor(public http: RestApiProvider) { 
  }

  getUserId(){
    return localStorage.userId;
  }

  UserLogOut() {
    let obj = {
      "id": this.getUserId()
    }
    return this.http.PostRequest("/user/logout", obj);
  }

  UserGet() {
    let obj = {
      "id": this.getUserId()
    }
    return this.http.PostRequest("/user/get", obj);
  }

  UserCheck(phone: string) {
    let obj = {
      "phone": phone,
      "_check_only": "1"
    }
    return this.http.PostRequest("/user/check", obj);
  }

  UserCheckUpdatePhone(phone: string) {
    let obj = {
      "phone": phone,
      "check_only": "1"
    }
    return this.http.PostRequest("/user/check", obj);
  }

  UserVerify(phone: string, code: string) {
    let obj = {
      "phone": phone,
      "code": code
    }
    return this.http.PostRequest("/user/verify", obj);
  }

  UserResetPassword(phone: string) {
    let obj = {
      "phone": phone
    }
    return this.http.PostRequest("/user/reset_password", obj);
  }

  UserLogin(phone: string, password: string) {
    let lat = localStorage.currentLat;
    let lng = localStorage.currentLng;
    if(lat == null)
    {
      lat ="13.7014544";
    }

    if(lng == null)
    {
      lng ="100.3879633";
    }

    let obj = {
      "phone": phone,
      "password": password,
      "lat" : lat,
      "long" : lng
    }
    return this.http.PostRequest("/user/login", obj);
  }

  UserLoginApp() {

    let lat = localStorage.currentLat;
    let lng = localStorage.currentLng;

    if(lat == null)
    {
      lat ="13.750443";
    }

    if(lng == null)
    {
      lng ="100.539913";
    }

    let obj = {
      "id": localStorage.userId, 
      "lat" : lat,
      "long" : lng
    }
    return this.http.PostRequest("/user/login_app", obj);
  }

  UserUpdate(data: any) {
    let obj = {
      "id": data.id,
      "_phone": data.phone,
      "_facebook_id": data._facebook_id,
      "name": data.name,
      "email": data.email,
      "image": data.image,
      "_uuid": data._uuid,
      "_udid": data._udid,
      "_pushbadge": data._pushbadge,
      "_pushsound": data._pushsound,
      "_pushalert": data._pushalert,
      "device": data.device,
      "password": data.password,
    }  
    return this.http.PostRequest("/user/update", obj);
  }


  UserUpdatePhone(data: any) {
    let obj = {
      "id": data.id,
      "phone": data.phone
    }  
    return this.http.PostRequest("/user/update", obj);
  }

  UserPaymentAdd(str_token: string) {
    let obj = {
      "id": this.getUserId(),
      "token": str_token, 
    }
    return this.http.PostRequest("/user/payment_add", obj);
  }

  UserPaymentAddCash() {
    let obj = {
      "id": this.getUserId(),
    }
    return this.http.PostRequest("/user/payment_add_cash", obj);
  }

   

  UserPaymentDelete(pid:string) {
    let obj = {
      "id":this.getUserId(),
      "id_payment": pid
    }
    return this.http.PostRequest("/user/payment_delete", obj);
  }

  UserPaymentListing() {
    let obj = {
      "id": this.getUserId(),
    }
    return this.http.PostRequest("/user/payment_listing", obj);
  }


  UserAddPlace(data: any) {   
    let obj = {
      "id": this.getUserId(),   
      "lat" : data.lat,
      "long" : data.long,
      "type": data.type,
      "name" : data.name
    }
    return this.http.PostRequest("/user/add_place", obj);
  }


  UserListingPlace() {   
    let obj = {
      "id": this.getUserId()  
    }
    return this.http.PostRequest("/user/listing_place", obj);
  }


  UserDelPlace(pid: string) {   
    let obj = {
      "id": this.getUserId(),
      "place_id" : pid
    }
    return this.http.PostRequest("/user/del_place", obj);
  }


}

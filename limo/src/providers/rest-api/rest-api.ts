import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, RequestOptionsArgs, Headers, URLSearchParams, Jsonp } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { GlobalDeclaration } from '../../Declaration' 
import {Md5} from 'ts-md5/dist/md5';

@Injectable() 
export class RestApiProvider { 
  apiUrl = 'https://limoapp.me/api';
  

  constructor(public http: Http,private globalDeclaration: GlobalDeclaration) {  
  }

  // GetRequest(): Promise<any>{
  //   var urls = "https://limoapp.me/dapter/test.php";
  //   return this.http.get(urls).toPromise()
  //   .then(this.extractData)
  //   .catch(this.handleError);
  // }

  GetRequestUrl(url:string): Promise<any>{ 
    return this.http.get(url).toPromise()
    .then(this.extractData)
    .catch(this.handleError);
  }

  public monthN = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
    
 
  PostRequest(url:string,obj:any): Promise<any> {  
    // let curr = new Date(); 
    //let toDay = new Date(curr.setDate(curr.getDate())); 
    //let cHH = toDay.getHours();
    //let sYYYYmmDDhh = toDay.getFullYear() + this.monthN[toDay.getMonth()] + ("0" + toDay.getDate().toString()).slice(-2) + ("0" + toDay.getHours().toString()).slice(-2);
    
    // console.log(sYYYYmmDDhh);

    //let hash =Md5.hashStr('LIM@:'+ sYYYYmmDDhh);

    let hash =Md5.hashStr('LIM@:*&007');

    // "5123e556353d10bf5f459f8e642f8d65";

     //let hash =Md5.hashStr('LIM@:2019030612');

     obj["X-Api-Key"] = hash; 
     
     //"8b065c6ec3bc280438250a21e5e2aa63"; 
     var _objX = JSON.stringify(obj); 
     //var _lUrl = this.dapterUrl;
     var _lUrl = this.apiUrl+url;
    // console.log("URL : " + _lUrl);
    // console.log("REQUEST OBJ : ",_objX);
     
    return this.http.post(_lUrl,_objX, this.ApiHeader()).toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  private ApiHeader(): RequestOptions {
    let _header: Headers = new Headers;
    //_header.set('Content-Type', 'application/json');
    //_header.set('Authorization', '8b065c6ec3bc280438250a21e5e2aa63');

    let _option: RequestOptions = new RequestOptions({
      headers: _header
    });
    return _option;
  }

  private CastObjectParam(obj: any): URLSearchParams { 
    let _urlParam = new URLSearchParams();
    Object.keys(obj).forEach(function (k) {
      _urlParam.append(k, obj[k]);
    }); 
    return _urlParam;
  }

  private extractData(res: Response) { 
    var _res = res.json() || {};
    // console.log("Response : ",_res);
    return _res;
  }
  private handleError(res: Response | any) {
    console.log("Request Error : ",res);
    return Promise.reject(res.message || res);
  }
}

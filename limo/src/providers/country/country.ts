import { RestApiProvider } from '../rest-api/rest-api';
import { Injectable } from '@angular/core';
declare var localStorage: any;
@Injectable()
export class CountryProvider {
 
  constructor(public http: RestApiProvider) { 
  }

  getUserId(){
    return localStorage.userId;
  }

  CountryList() { 
    return this.http.PostRequest("/country/listing", {});
  }

  CountryGet(gId: string) {
    var obj = {
      "id": gId, 
    }
    return this.http.PostRequest("/country/get", obj);
  }
  
}

import { RestApiProvider } from '../rest-api/rest-api';
import { Injectable } from '@angular/core';
declare var localStorage: any;
@Injectable()
export class OrderProvider {
 
  constructor(public http: RestApiProvider) { 
  }

  getUserId(){
    return localStorage.userId;
  }

  OrderBook(data: any) {
    let obj = {
      "id": this.getUserId(),
      "start_location_lat": data.start_location_lat,
      "start_location_long": data.start_location_long,
      "start_location": data.start_location,
      "destination_lat": data.destination_lat,
      "destination_long": data.destination_long,
      "destination": data.destination,
      "car_type": data.car_type,
      "id_payment": data.id_payment,
      "fare": data.fare,
      "note": data.note,
      "expressway": data.expressway,
    }
    return this.http.PostRequest("/order/book", obj);
  }

  OrderCancel(userId: string, orderId: string) {
    var obj = {
      "id": userId,
      "order_id": orderId,
    }
    return this.http.PostRequest("/order/cancel", obj);
  }

  OrderHistory(ofset: string, limit: string) {
    var obj = {
      "id": this.getUserId(),
      "_offset": ofset,
      "_limit": limit
    }
    return this.http.PostRequest("/order/history", obj);
  }

  OrderGet(id_history: string) {
    var obj = {
      "id": this.getUserId(),
      "id_history": id_history,
    }
    return this.http.PostRequest("/order/get", obj);
  }

  OrderGetIds(id_: string,id_history: string) {
    var obj = {
      "id": id_,
      "id_history": id_history,
    }
    return this.http.PostRequest("/order/get", obj);
  }
  
  OrderCalculate(time: string, km: string) {
    var obj = {
      "time": time,
      "km": km
    }
    return this.http.PostRequest("/order/calculate", obj);
  }

  DriverDriving() {
    return this.http.PostRequest("/driver/driving", {});
  }

}

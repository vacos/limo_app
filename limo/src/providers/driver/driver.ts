import { RestApiProvider } from '../rest-api/rest-api';
import { Injectable } from '@angular/core';
declare var localStorage: any;
@Injectable()
export class DriverProvider {
 
  constructor(public http: RestApiProvider) { 
  }
 

  DriverFind() {
    return this.http.PostRequest("/driver/find", {});
  }

  DriverDriving() {
    return this.http.PostRequest("/driver/driving", {});
  }

  DriverConfirm(dId: string, oId: string) { 
    var obj = {
      "driver_id": dId,
      "order_id" : oId
    } 
    return this.http.PostRequest("/driver/confirm", obj);
  }

  DriverSuccess(dId: string, oId: string) {
    var obj = {
      "driver_id": dId,
      "order_id" : oId
    } 
    return this.http.PostRequest("/driver/success", obj);
  }

  DriverVote(oId: string, rate : string, comment :string) {
    var obj = { 
      "order_id" : oId,
      "rate": rate,
      "comment": comment
    } 
    return this.http.PostRequest("/driver/vote", obj);
  }

  DriverPickup(dId: string, oId: string) {
    var obj = {
      "driver_id": dId,
      "order_id" : oId
    } 
    return this.http.PostRequest("/driver/pickup", obj);
  }

 
  // https://maps.googleapis.com/maps/api/geocode/json?latlng=13.70149792102319,100.38790051145523&result_type=route&key=AIzaSyDgxVA7U6pyie189pCEKCd-C1QxpRE9ibI
  DriverLoc(lat: string, long: string) { 
    let urls = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat +","+ long +"&result_type=route&key=AIzaSyDgxVA7U6pyie189pCEKCd-C1QxpRE9ibI"; 
    return this.http.GetRequestUrl(urls);
  }

}
